<!DOCTYPE html>
<html lang="pt-br">
	<head>
	<meta charset="utf-8"/>
	<title>Formulário da Livraria</title>
	<link rel="stylesheet" type="text/css" href="css/estilo.css"/>
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		<script type="text/javascript">
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
		</script>
	</head>
<body>
    <div>
        <select>
            <option>Escolha uma opção:</option>
            <option value="categorias">Categorias</option>
            <option value="livros">Livros</option>
            <option value="produtos">Produtos</option>
        </select>
    </div>
	<!-- ==================== CATEGORIAS ==================== -->    
	<div class="categorias box"><br/><br/>
		<div>
		<form method="post" action="inserir.php">
			<fieldset>
			<h2>Inserir categorias: </h2>
				<label><span title="Se for repetido não funcionará">Código: </span></label>
					<input type="number" name="codigo" placeholder="Insira o código da categoria"/><br/><br/>
	
				<label><span title="Se for repetido não funcionará">Nome:</span></label>
					<input type="text" name="nome" placeholder="Insira o nome da categoria"/><br/><br/>			
					<input type="submit" name="enviar"/><br/>		
	
			</fieldset>
		</form><br/><br/>
			<fieldset>
		<form method="POST" action="select.php" name="buscar">
			<h2>Buscar por categorias</h2>
					<label>Pesquisa por nome: </label><input type="text" name="pesquisar" placeholder="Pesquise por categorias"/><br/><br/>
					<input type="submit" value="Buscar" />
		</form>
		</fieldset>
	</div><br/><br/>
	</div>
	<!-- ==================== LIVROS ==================== -->  
	<div class="livros box"><br/><br/>
		<div>
		<form method="post" action="inserir2.php">
			<fieldset>
			<h2>Inserir livros: </h2>
				<label>Isbn: </label>
					<input type="number" name="isbn" placeholder="Insira o isbn do livro."/><br/><br/>
	
				<label>Título:</label>
					<input type="text" name="titulo" placeholder="Insira o título do livro."/><br/><br/>

				<label>Autor:</label>
					<input type="text" name="autor" placeholder="Insira o nome do autor do livro."/><br/><br/>
					
				<label>Páginas:</label>
					<input type="number" name="paginas" placeholder="Insira o número de páginas do livro."/><br/>	<br/>				

				<label>Preço:</label>
					<input type="number" name="preco" placeholder="Insira o preço do livro."/><br/>						
						<br/><input type="submit" name="enviar2"/><br/>			
			</fieldset>
		</form><br/><br/>
			<fieldset>
		<form method="POST" action="select2.php" name="buscar">
			<h2>Buscar livros</h2>
					<label>Pesquisa pelo livro: </label><input type="text" name="pesquisar1" placeholder="Insira o nome do livro"/><br/><br/>
					
						<input type="submit" value="Buscar" />
		</form>
			</fieldset>
	</div><br/><br/>  
	</div>
	<!-- ==================== PRODUTOS ==================== -->   
	<div class="produtos box"><br/><br/>   
		<div>
		<form method="post" action="inserir3.php">
			<fieldset>
			<h2>Inserir produtos: </h2>
				<label>Código:</label>
					<input type="number" name="codigo" placeholder="Insira o código do produto."/><br/><br/>	
			
				<label>Nome:</label>
					<input type="text" name="nome" placeholder="Insira o nome do produto."/><br/><br/>				
			
				<label>Descrição:</label>
					<textarea name="descricao" rows="6" cols="60" placeholder="Faça uma breve descrição do produto." ></textarea><br/><br/>					
				
				<label>Preço:</label>
					<input type="number" name="preco" placeholder="Insira o preço do produto."/><br/><br/>

				<label>Código da categoria:</label>
					<input type="number" name="codigo_categoria" placeholder="Insira o código da categoria em que o produto se enquadra."/><br/>
					<br/><input type="submit" name="enviar3"/><br/>		
			</fieldset>
		</form><br/><br/>		
			<fieldset>
		<form method="POST" action="select3.php" name="buscar">
			<h2>Buscar por produtos</h2>
					<label>Pesquisa pelo produto: </label><input type="text" name="pesquisar2" placeholder="Pesquise por produtos"/><br/><br/>

					<input type="submit" value="Buscar" />
		</form>
			</fieldset>
		</div><br/>
	</div>
<body>
</html>