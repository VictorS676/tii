-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 02-Nov-2018 às 03:45
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `livraria`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `codigo` smallint(6) NOT NULL,
  `nome` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`codigo`, `nome`) VALUES
(3, 'Esporte e Lazer'),
(4, 'Inform?tica'),
(5, 'O melhor'),
(6, 'O pior'),
(7, 'EletrodomÃ©sticos'),
(9, 'Roupas'),
(12, 'A pior'),
(211, 'Cozinha'),
(889, 'A mellhor'),
(12345, 'Categoria CategÃ³rica');

-- --------------------------------------------------------

--
-- Estrutura da tabela `livros`
--

CREATE TABLE `livros` (
  `isbn` varchar(13) DEFAULT NULL,
  `titulo` varchar(80) NOT NULL,
  `autor` varchar(80) NOT NULL,
  `paginas` smallint(6) NOT NULL,
  `preco` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `livros`
--

INSERT INTO `livros` (`isbn`, `titulo`, `autor`, `paginas`, `preco`) VALUES
('8575220020', 'Oracle Forms 6i - Guia de Consulta R?pida', 'Alexandre Poda', 0, 0),
('8575220012', 'Windows 2000 Server - Guia de Consulta R?pida', 'Roberto Veiga', 0, 0),
('8585184582', 'ARJ - Guia de Consulta R?pida', 'Rubens Prates,Joel Saade', 32, 11),
('8585184752', 'ASP - Guia de Consulta R?pida', 'Rubens Prates', 0, 18),
('8585184779', 'Autocad 2000 - Guia de Consulta R?pida', 'Leonardo Lemes', 126, 0),
('858518454X', 'C++ - Guia de Consulta R?pida', 'Mauro Rezende', 32, 11),
('8585184108', 'Clipper 5.2 - Guia de Consulta R?pida', 'Rubens Prates', 32, 9),
('8585184132', 'Clipper Mensagens de Erro - Guia de Consulta R?pida', 'Rubens Prates', 32, 9),
('8585184574', 'Cobol ANS 85 - Guia de Consulta R?pida', 'Hugo Santos', 32, 11),
('8585184612', 'Primeiro', 'Hugo Santos', 520, 55),
('858518499X', 'ColdFusion - Guia de Consulta R?pida', 'El?i Assis', 0, 0),
('8585184450', 'Construindo Aplicativos com o Visual Basic 4', 'Marcelo Moya Dias', 176, 25),
('8585184329', 'Corel Draw 5 - Guia de Consulta R?pida', 'Rubens Prates,Adriana Pedrassa', 32, 9),
('8585184566', 'CorelDraw 7 Fonts - Guia de Consulta R?pida', 'Horie', 0, 18),
('8585184531', 'CorelDraw Symbols - Guia de Consulta R?pida', 'Horie', 34, 11),
('8585184930', 'Crystal Reports - Guia de Consulta R?pida', 'Hugo Santos', 127, 0),
('8585184663', 'Delphi 4 Fun??es e Procedimentos - Guia de Consulta R?pida', 'Rubens Prates', 0, 22),
('8585184655', 'Delphi 4 Object Pascal - Guia de Consulta R?pida', 'Rubens Prates', 0, 22),
('8585184833', 'Dreamweaver - Guia de Consulta R?pida', 'Marcelo Silveira', 0, 0),
('8585184515', 'Emoticons - Guia de Consulta R?pida', 'Ricardo Minoru Horie', 48, 11),
('8585184442', 'Eudora Light - Guia de Consulta R?pida', 'Adlich', 24, 9),
('8585184876', 'Flash 5 - Guia de Consulta R?pida', 'Edgard Damiani', 0, 0),
('8585184868', 'XML - Guia de Consulta R?pida', 'Ot?vio C. D?cio', 96, 0),
('8585184736', 'Visual Basic 6 Controles Activex - Guia de Consulta R?pida', 'Hugo Santos', 0, 18),
('8585184469', 'Visual Basic Vers?o 4 - Guia de Consulta R?pida', 'Rubens Prates', 0, 18),
('8585184892', 'WAP - Guia de Consulta R?pida', 'Marcelo Silveira', 0, 0),
('858518423X', 'Windows Arquivos INI Vers?o 3.1 - Guia de Consulta R?pida', 'Rubens Prates', 30, 9),
('8585184183', 'Windows 3.1 - Guia de Consulta R?pida', 'Rubens Prates', 30, 9),
('8585184825', 'Windows Script Host - Guia de Consulta R?pida', 'Roberto Veiga', 96, 0),
('8585184817', 'WinZip - Guia de Consulta R?pida', 'Hugo Santos', 96, 0),
('8585184205', 'Word for Windows 6.0 - Guia de Consulta R?pida', 'Rubens Prates', 32, 9),
('8585184604', 'Photoshop 4.0 - Guia de Consulta R?pida', 'Marco Antonio Carneiro', 6789, 4567890),
('8585184698', 'PHP - Guia de Consulta R?pida', 'Herbert G Fischer', 0, 18),
('8585184477', 'PKZIP - Guia de Consulta R?pida', 'William Quirino Ferreira', 32, 9),
('8585184280', 'SQL - Guia de Consulta R?pida', 'Edison Liesse', 32, 11),
('858518471X', 'SQL Server 7 System Procedures - Guia de Consulta R?pida', 'Rubens Prates,Renato Piques', 0, 18),
('8585184701', 'SQL Server 7 Transact-SQL - Guia de Consulta R?pida', 'Rubens Prates,Renato Piques', 0, 0),
('8585184728', 'TCP/IP - Guia de Consulta R?pida', 'Rubens Prates,Luciano Palma', 0, 18),
('8585184558', 'Unix Comandos de Usu?rios - Guia de Consulta R?pida', 'Eduardo Marcan', 48, 13),
('8585184418', 'Turbo Pascal - Guia de Consulta R?pida', 'Rubens Prates,Dennis Cintra Leite', 32, 9),
('8585184388', 'HTML - Guia de Consulta R?pida', 'Rubens Prates', 32, 9),
('8585184906', 'ICQ - Guia de Consulta R?pida', 'Maria Alice De Castro', 96, 0),
('8585184914', 'Integrando PHP com MySQL - Guia de Consulta R?pida', 'Lucio Stoco', 94, 0),
('8585184256', 'Interrup??es do Bios - Guia de Consulta R?pida', 'Adlich', 32, 9),
('8585184264', 'Interrup??es do MS-DOS - Guia de Consulta R?pida', 'Adlich', 32, 9),
('8585184639', 'Java - Guia de Consulta R?pida', 'F?bio Ramon', 0, 18),
('8585184973', 'Java 2 - Guia de Consulta R?pida', 'F?bio Ramon', 144, 0),
('8585184884', 'JavaServer Pages - Guia de Consulta R?pida', 'Idemir Coelho', 96, 0),
('8585184396', 'JavaScript - Guia de Consulta R?pida', 'Rubens Prates', 32, 9),
('8585184744', 'JDBC2 - Guia de Consulta R?pida', 'F?bio Ramon', 96, 18),
('8575220063', 'BASH - Guia de Consulta R?pida', 'Hugo Santos', 96, 0),
('8575220047', 'UML - Guia de Consulta R?pida', 'Douglas Marcos da Silva', 95, 0),
('8575220055', 'Cascading Style Sheets (CSS) - Guia de Consulta R?pida', 'Luis Gustavo Amaral', 0, 0),
('8585184957', 'Linux Administra??o e Suporte', 'Chuck V. Tibet', 379, 59),
('8585184647', 'Linux Comandos De Usu?rios - Guia de Consulta R?pida', 'Eduardo Ma?an', 6789, 4567890),
('8585184760', 'Linux Interface Gr?fica KDE - Guia de Consulta R?pida', 'Frederico Reis', 92, 0),
('8585184922', 'Microsoft IIS 5 - Guia de Consulta R?pida', 'Roberto Veiga', 96, 0),
('8585184353', 'Microsoft Windows 95 - Guia de Consulta R?pida', 'Rubens Prates', 32, 9),
('8585184841', 'Microsoft Word 2000 - Guia de Consulta R?pida', 'Danielle Rossi', 96, 0),
('8585184434', 'mIRC - Guia de Consulta R?pida', 'Adlich', 32, 9),
('8585184787', 'MySQL - Guia de Consulta R?pida', 'Rubens Prates', 96, 0),
('8585184175', 'MS-DOS Vers?o 6.2 - Guia de Consulta R?pida', 'Rubens Prates', 32, 9),
('8585184590', 'Netiqueta - Guia de Consulta R?pida', 'Maria Alice De Castro', 32, 11),
('8585184361', 'Netscape Navigator - Guia de Consulta R?pida', 'Rubens Prates', 32, 9),
('8585184426', 'NetWare 4.1 Utilities - Guia de Consulta R?pida', 'Meirelles', 32, 9),
('8575220039', 'Desenvolvendo Websites com PHP 4', 'Juliano Niederauer', 256, 39),
('8575220101', 'HTTP - Guia de Consulta R?pida', 'D?cio Jr.', 0, 0),
('857522011X', 'Express?es Regulares - Guia de Consulta R?pida', 'Aur?lio Marinho Jargas', 96, 0),
('8575220071', 'JavaScript - Guia de Consulta R?pida', 'Edgard Damiani', 144, 0),
('8575220152', 'Seguran?a Nacional', 'Nelson Murilo de O. Rufino', 248, 45),
('8575220160', 'XHTML - Guia de Consulta R?pida', 'Juliano Niederauer', 0, 0),
('8575220128', 'PostgreSQL - Guia de Consulta R?pida', 'Juliano Niederauer', 0, 0),
('8575220136', 'Aprendendo Delphi 6 & Kylix', 'Fabr?cio Alex Sim?es', 320, 42),
('8575220144', 'Oracle 9i Built-in Packages - Guia de Consulta R?pida', 'Celso Henrique Poderoso de Oliveira', 0, 0),
('8575220098', 'Python - Guia de Consulta R?pida', 'Marco Catunda', 0, 0),
('857522008X', 'Tcl/TK - Guia de Consulta R?pida', 'Roberto L.S. Monteiro', 0, 0),
('8575220179', 'Formatos de Arquivos da Internet - Guia de Consulta R?pida', 'Marcelo Silveira', 0, 0),
('8585184795', 'Oracle 8 SQL - Guia de Consulta R?pida', 'Rubens Thiago de Oliveira', 96, 0),
('858518485X', 'Oracle 8i PL/SQL - Guia de Consulta R?pida', 'Celso Henrique Poderoso de Oliveira', 96, 0),
('8585184507', 'Pentium CMOS Setup - Guia de Consulta R?pida', 'Rubens Prates', 24, 9),
('8585184949', 'Samba - Guia de Consulta R?pida', 'D?cio Jr.', 0, 0),
('8585184809', 'Perl - Guia de Consulta R?pida', 'D?cio Jr.', 0, 0),
('8575220187', 'As Palavras Mais Comuns da L?ngua Inglesa', 'Rubens Queiroz de Almeida', 320, 24),
('8575220209', 'Oracle 9i SQL - Guia de Consulta R?pida', 'Rubens Thiago de Oliveira', 0, 0),
('8575220217', 'Aprendendo Java 2', 'Rodrigo Mello,Ramon Chiara,Renato Villela', 190, 42),
('8575220225', 'Read in English - Uma Maneira Divertida de Aprender Ingl?s', 'Rubens Queiroz de Almeida', 352, 48),
('8575220241', 'SQL - Curso Pr?tico', 'Celso Henrique Poderoso de Oliveira', 272, 42),
('8575220284', 'Web Marketing Usando Ferramentas de Busca', 'Marcelo Silveira', 160, 38),
('8575220268', 'DB2 UDB v.7 - Guia de Consulta R?pida', 'Jo?o Alberto de Oliveira Lima', 0, 0),
('777', 'Isso mesmo', 'Roberto Severo de A. Coelho', 6789, 4567890),
('857522025X', 'PHP com XML - Guia de Consulta R?pida', 'Juliano Niederauer', 96, 0),
('8575220322', 'Oracle 9i PL/SQL - Guia de Consulta R?pida', 'Celso Henrique Poderoso de Oliveira', 112, 0),
('8575220292', 'Mirando Resultados', 'Ricardo Almeida,Marcelo Oliveira', 208, 42),
('8575220330', 'Virtual Private Network - VPN', 'Lino Sarlo da Silva', 240, 43),
('8575220349', 'Desenvolvendo Aplica??es ASP.NET com Web Matrix', 'Daniel Wander', 320, 48),
('8575220306', 'Prote??o Jur?dica de Software', 'Alexandre Coutinho Ferrari', 192, 38),
('8575220314', 'ASP.NET Guia do Desenvolvedor', 'Felipe Cembranelli', 256, 39),
('8575220357', 'ASP.NET com C#', 'Alfredo Lotar', 384, 52),
('8575220365', 'Java e XML - Guia de Consulta R?pida', 'Ren? Rodrigues Veloso', 96, 0),
('8575220373', 'As palavras mais comuns da L?ngua Inglesa - 2? edi??o', 'Rubens Queiroz de Almeida', 312, 53),
('8575220381', 'Linux - Guia do Administrador do Sistema', 'Rubem E. Ferreira', 512, 85),
('857522039X', 'InterBase - Guia de Consulta R?pida', 'Juliano Niederauer', 96, 22),
('2323242', 'Redes', 'Celso', 20, 1200),
('5343433232', 'MIL e um', 'Higaif', 677, 65),
('4567890', 'Ãˆ um ', 'Tigo Joa', 34, 5545440),
('55553535', 'O mesmo e', 'Johugu', 5678, 1),
('9999999999999', '9999999999', '99999999999', 32767, 1e16),
('9999999999999', '999999999999999', '99999999999999', 32767, 100000000000000),
('9999999999999', '999999999999999', '99999999999999', 32767, 100000000000000),
('894238e125', '1984', 'George Orwell', 212, 165),
('32345645', 'O Velho e o Mar', 'Ernest Hemingway', 287, 200),
('87953214', 'Quincas Borba', 'Machado de Assis', 140, 80),
('315479856', 'Misto-Quente', 'CHARLES BUKOWSKI', 189, 99),
('437342612', 'Christine', 'Stephen King', 211, 230);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `codigo` mediumint(9) NOT NULL,
  `nome` varchar(80) NOT NULL,
  `descricao` text NOT NULL,
  `preco` float NOT NULL,
  `codigo_categoria` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`codigo`, `nome`, `descricao`, `preco`, `codigo_categoria`) VALUES
(3, 'VIDEOK? RAF ELECTR VMP 9000', 'Mostre todo o seu talento com este videok?! Suas reuni?es v?o ficar ainda mais animadas.', 699, 1),
(4, 'HOME THEATER GRADIENTE SOLUTION HTS 100', 'Transforme sua casa em um verdadeiro cinema e sinta-se como estivesse dentro do filme!', 1399, 1),
(5, 'AR CONDICIONADO SPRINGER INNOVARE', 'Gabinete em pl?stico de alta resist?ncia. F?cil instala??o e manuten??o.', 645, 2),
(6, 'REFRIGERADOR ELECTROLUX DC38', 'Combina modernidade e estilo, al?m de conservar seus alimentos na temperatura certa.', 1199, 2),
(7, 'ESTEIRA CORPORE EL?TRICA H-100', 'Robusta, suporta at? 100 kg de peso.', 574, 3),
(8, 'Fritadeira ElÃ©trica Airfryer Star Philips Walita', 'Com Airfryer Star Philips Walita vocÃª saboreia porÃ§Ãµes de batata frita, frango a passarinho, polenta e atÃ© churros, gostosos como frito e sem usar Ã³leo*, para vocÃª e sua famÃ­lia se alimentarem de maneira mais saudÃ¡vel e sem abrir mÃ£o do sabor!', 999, 7),
(9, 'Nutri Ninja Revolution', 'Nutri Ninja Revolution conta com a qualidade e 1 ano de garantia da Ninja, marca nÂº 01 em ultraprocessadores e extratores de sabor e saÃºde nos Estados Unidos. Nutri Ninja Revolution Ã© uma super exclusividade Polishop!', 1399, 211),
(10, 'Espelho DobrÃ¡vel Retratta Lighting Mirror Polishop', 'Com o Espelho DobrÃ¡vel Retratta Lighting Mirror, vocÃª vai contar com o aliado certo para cuidar da sua beleza a qualquer momento do dia!', 139, 889),
(11, 'Guarda-Chuva Ultimate Brella', 'EsqueÃ§a tudo o que vocÃª jÃ¡ viu sobre guarda-chuvas! Ultimate Brella Polishop Ã© o que vocÃª realmente precisa para enfrentar aqueles dias chuvosos e chegar completamente seco nos lugares.', 99, 12345),
(12, 'Caixa AcÃºstica RetrÃ´ Philco Premium', 'Agora, vocÃª e seus convidados podem curtir suas mÃºsicas direto do celular, tablet, pen drive e atÃ© do seu app de mÃºsicas, de um jeito muito mais estiloso e fÃ¡cil!', 1099, 5),
(13, 'Sanduicheira Platinum Philco Premium', 'Com a Sanduicheira Platinum Philco Premium vocÃª vai saborear sanduÃ­ches crocantes e deliciosos prontos em minutos, de forma rÃ¡pida e prÃ¡tica!', 149, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
