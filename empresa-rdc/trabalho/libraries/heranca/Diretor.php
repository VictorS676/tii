<?php

include_once 'Professor.php';

class Diretor extends Professor {
    private $horario;
    private $salario;

    function __construct($nome, $escola, $salario, $horario){        
        parent::__construct($nome, $escola);
        $this->salario = $salario;
        $this->horario = $horario;
    }

    public function gerenciar($nome, $escola){
        echo "O diretor $nome gerencia as organizações da escola $escola<br/>";
    }

    public function liderar($nome){
        echo "O diretor $nome tem como sua principal função liderar os funcionários.<br/>";
    }

    public function getSalario(){
        return $this->salario;
    }

    public function getHorario(){
        return $this->horario;
    }
}

?>