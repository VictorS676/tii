<?php

include_once 'Pessoa.php';

class Professor extends Pessoa {
    private $especialidade;
    private $escola;

    // Sobrescrita de método da classe pai
    function __construct($nome, $escola){
        parent::__construct($nome);// Sintaxe
        $this->escola = $escola;
    }

    public function pesquisar($tema){
        echo "Passo horas pesquisando sobre $tema<br/>";
    }

    public function lecionar($materia){
        echo "Todos os dias leciono sobre $materia <br/>";
    }

    public function getNome(){
        return "Professor ".parent::getNome();
    } 

}


?>