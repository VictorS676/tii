<?php

class Pessoa{
    // 1- Atributos
    private $nome;
    private $peso;
    private $idade;
    private $altura;

    // 2- Construtor
    function __construct($nome){
        $this->nome = $nome;
    }

    // 3- Métodos
    public function dormir(){
        echo 'Descanso de, pelo menos, 12 horas.<br/>';
    }

    public function comer($comida){
        echo "Eu gosto de comer $comida<br/>";
    }

    public function falar($assunto, $interlocutor = "eu mesmo"){// Se nenhuma informação for passada esse será o valor padrão
        echo "Costumo falar sobre $assunto com $interlocutor<br/>";
    }

    // Métodos acessores
    public function getNome(){
        return $this->nome;
    }

}

?>