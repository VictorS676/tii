<?php

class Imagem{
    private $imagem;
    private $descricao;
    private $table = "galeria";

    function __construct($imagem, $descricao){
        $this->imagem = $imagem;
        $this->descricao = $descricao;
    }

    public function getHTML(){
        $html = '<a href="'.BASEURL.'assets/img/'.$this->imagem.'" class="image">
                    <img src="'.BASEURL.'assets/img/'.$this->imagem.'" alt="'.$this->descricao.'" title="'.$this->descricao.'" />
                </a>';
        return $html;
    }
}

?>