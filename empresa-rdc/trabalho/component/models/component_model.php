<?php
include APPPATH.'component/libraries/component/Imagem.php';
function get_imagens(){
    $db = new DB();
    $v = $db->get('galeria');
    $html = '';
    foreach($v AS $data){
        $image = $data['imagem']; 
        $descricao = $data['descricao'];
        $imagem = new Imagem($image, $descricao);
        $html .= $imagem->getHTML();   
    }
    return $html;
}
?>