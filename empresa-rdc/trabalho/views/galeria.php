<div class="container">
    <div class="gallery-section mb-4">
      <div class="inner-width">
        <h2 class="text-center titulo display-4 pb-5 pt-1">Minha Galeria</h2>
        <div class="border"></div>
        <div class="gallery">

        <?= $components ?>

        </div>
      </div>
    </div>
</div>