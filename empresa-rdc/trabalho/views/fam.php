<div class="container">
    <div class="row">
        <div class="col-3">
            <img src="<?= BASEURL ?>assets/img/brasao-r.png" alt="" class="img-fluid"/>
        </div>
        <div class="col-9 text-center">
            <div class="align-middle">
                <h1 class="fml display-3 pt-5 mt-5">Família Sousa</h1>
            </div>
        </div>
    </div>
    <div class="line"></div>
    <div class="row">
        <div class="col-12">
            <p class="conteudo">O sobrenome Sousa, ou a comum variação Souza, de origem portuguesa, não se sabe ao certo de qual palavra ele se originou, uns acreditam que provem do latim saucia, que significa pedra, já outros dizem que provem de saza, que por sua vez significa seixo.</p>
            <p class="conteudo">Em Portugal estão um rio chamado Sousa, cuja origem é a palavra latina saza, e próximo ao rio há uma povoação chamada Souza, na qual tem sua origem na palavra latina saucia.</p>
            <img src="<?= BASEURL ?>assets/img/rio_sousa.jpg" alt="Rio Sousa" title="Rio Sousa" class="img-fluid w-75 rounded mx-auto d-block z-depth-1 mt-4 mb-4"/>
            <p class="conteudo">As referências mais antigas sobre esta família citam a região próxima ao rio Souza na qual, entre século VIII e XI, os seus donos passaram assimilar para si o nome do rio, desta linhagem gerou-se o ramo nobre da família, que tinha uma parentela com os reis de Castela, Leão e com antigos reis visigodos, por isso em alguns brasões da família Souza se vê alusões as armas de Castela e Leão.</p>
            <p class="conteudo">Os nobres da família Sousa descendem de dois ramos diferentes, os Sousas de Arronches, descendentes de Egas Gomes de Sousa, que viveu nos tempos do rei D. Afonso III de Portugal, tal família fez alianças com a casa real e deles descendem os condes de Miranda e de Arronches, assim como os duques de Lafões.</p>
            <p class="conteudo">  O outro ramo da família Sousa é do Prado, descendentes de Maria Pires Ribeira de Souza e de D. Afonso Diniz, filho ilegítimo do rei D. Afonso V, destes descendem os condes do Prado, e a estes estariam associados os Sousas Chichorro, os Sousas de Cancelhos, senhores de Baião, e os Sousas de Alcube.</p>
        </div>
    </div>
</div>    