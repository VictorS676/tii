<div class="container conteudo">
    <div class="row">
        <div class="col-12 text-center titulo bg-dark text-white pt-3 pb-2 rounded mb-4 mt-3">
            <h2 class="display-4">Hobbies & Sonhos</h2>
        </div>
        <p class="conteudo pt-2">Gosto de ler um bom livro, assistir um filme ruim e assistir partidas de futebol piores ainda, tenho o costume de acompanhar a temporada da NASCAR, ouvir um bom rock clássico e tento estudar alguma coisa sobre programação e tecnologia.</p>
        <p class="conteudo"> Lamentavelmente não possuo tantos Hobbies - atividades praticadas no tempo livre - ou não são tão chamativos que nem tantos outros, como: natação, artes marciais, tocar um instrumento, entre outras. Pois não tenho condições físicas, psicológicas e nem financeiras para manter um hobbie mais esotérico.</p>
    </div>
    <div class="line"></div>
    <div class="row">
        <h2 class="titulo display-4 mb-4">Sonhos</h2>
    </div>
</div>    