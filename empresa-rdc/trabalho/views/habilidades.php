<div class="container mt-5">
    <div class="row">
        <div class="col-md-4 col-sm-6">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th scope="row">Saúde</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Força</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Energia</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Inteligência</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Disciplina</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Criatividade</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Carisma</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Hab. Sociais</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Confiança</th>
                        <td><span class="classificacao"><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-4 col-sm-6">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th scope="row">Humor</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Paixão</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Empatia</th>
                        <td ><span class="classificacao"><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Atitude</th>
                        <td ><span class="classificacao"><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Divertido</th>
                        <td ><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Programar</th>
                        <td ><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Medos</th>
                        <td ><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Ciúmes</th>
                        <td ><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Timidez</th>
                        <td ><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-4 col-sm-6">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th scope="row">Paciêcia</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Determinação</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Cozinhar</th>
                        <td ><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Limpar</th>
                        <td ><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Consertar</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Preguiça</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Talento</th>
                        <td><span class="classificacao"><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Higiene</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Depressão</th>
                        <td><span class="classificacao"><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i></span></td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>