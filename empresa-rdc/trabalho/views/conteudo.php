<div class="container">
        <div class="row">
            <div class="col-12 text-center titulo bg-dark text-white pt-3 pb-2 rounded mb-4 mt-3">
                <h2 class="display-4">Meu Perfil</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 pl-5 pr-5">
                <img src="<?= BASEURL ?>assets/img/eu.jpg" class="img-fluid caixa rounded" alt="">
                <div class="caixa rounded pt-3 mt-3"><p class="citacao conteudo">A vida não tem graça sem alguns dragões.</p></div>
                <div class="caixa rounded pt-3 mt-3"><p class="conteudo" title="Música Tema">♫ Guns N' Roses - Welcome To The Jungle</p></div>
            </div>
            <div class="col-md-7">
                <div class="row justify-content-center mt-3">
                    <table class="tabela w-75">
                        <tbody>
                            <thead>
                                <th scope="row" colspan="4" class="text-center cor titulo"><h1 class="display-5 pt-1">Básico</h1></th>
                            </thead>
                            <tr> 
                                <th class="pl-3 pr-3">Nome</th>
                                <td>Victor Pedro de Sousa</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Prontuário</th>
                                <td colspan="2">180010-8</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Nascimento</th>
                                <td colspan="2">08/11/2000</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Idade</th>
                                <td colspan="2">18</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Gênero</th>
                                <td colspan="2">Masculino</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Ocupação</th>
                                <td colspan="2">Estudante</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Filiação</th>
                                <td colspan="2">Nenhuma</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row justify-content-center mt-4">
                    <table class="tabela w-75">
                        <tbody>
                            <thead>
                                <th scope="row" colspan="4" class="text-center cor titulo"><h1 class="display-5 pt-1">Aparência</h1></th>
                            </thead>
                            <tr> 
                                <th class="pl-3 pr-3">Altura</th>
                                <td>Baixo</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Peso</th>
                                <td colspan="2">Magro</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Cabelo</th>
                                <td colspan="2">Castanho</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Olhos</th>
                                <td colspan="2">Castanho-escuros</td>
                            </tr>
                            <tr>
                                <th class="pl-3 pr-3">Pele</th>
                                <td colspan="2">Branco/Amarelo</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>         