<div class="container">
    <div class="line"></div>
    <div class="row">
        <div class="row mb-4 ml-3">
            <div class="col-2">
                <img src="<?= BASEURL ?>assets/img/brasao-rr.png" alt="" class="img-fluid w-75"/> 
            </div>
            <div class="col-10 pt-2">
                <h1 class="fml">História</h1>
            </div>
        </div>
        <div class="col-12">
            <p class="conteudo">A origem dessa família na Península Ibérica, remonta ao tempo dos reis godos. D. Gomes Echigues (1010-1102), era governador das comarcas de Entre-Douro-e-Minho, senhor de Felgueiras, perto do mosteiro de Pombeiro (o qual fundou e onde jaz).</p>
            <p class="conteudo">D. Gomes governava tais terras por ter derrotado com uma lança o Rey de Castilla, D. Sancho. E por tal motivo, D. Fernando o nomeou para governar tais terras. D. Gomes comprou esse senhorio de Paio Moniz em 1040 pelo preço de dois bons cavalos. Teve da viúva Dª Gontrode Moniz, filha D. Múnio Fernandes de Touro, que por sua vez era filho do Rey de Castilla, D. Fernando e de Dª Ximena (sua cunhada).</p>
            <p class="conteudo">Do casal Gomes-Gontrode procede o filho D. Egas Gomes de Sousa, senhor das terras de Sousa, de Novelas e de Felgueiras, além de ter sido governador da mesma comarca que seu pai. D. Egas se casou com Dª Flâmula Gomes (também conhecida como Dª. Gontinha Gonçalves), filha de D. Gonçalo Trastamires da Maia e Dª Mécia Rodrigues. Dª Flâmula era trineta do Rey de León, D. Ramiro II. As gerações seguintes continuaram a usar o sobrenome.</p>
            <p class="conteudo">Dª. Maria Pais Ribeiro, senhora de Sousa é sexta neta do casal Egas-Flâmula. Dª Maria foi casada como D. Afonso Diniz, filho ilegítimo do Rei de Portugal, D. Afonso III com Dª. Maria Pires da Enxarada (dando início aos Sousa, senhores de Arronches).</p>
            <p class="conteudo">Dª Inês Lourenço de Valadares era prima coirmã de Dª Maria Pais Ribeiro, portanto, também era sexta neta do casal Egas-Flâmula. Dª Inês foi casada com D. Martim Afonso, chamado de Chichorro. D. Martim era outro filho ilegítimo do Rei de Portugal, D. Afonso III, logo, dessa vez com Ouroana (também conhecida como Madragana Ben Aloandro ou Mor Afonso, filha Aloandro Ben Bakr), logo era meio-irmão de D. Afonso Diniz. Dessa união surgiu o ramo dos Sousa Chichorros ou Sousas do Prado.</p>
        </div>
    </div>
</div>    