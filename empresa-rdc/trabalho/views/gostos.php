<div class="container"> 
    <div class="row sombra rounded">
        <div class="col-md-7 col-xs-12">
            <div class="box">
                <div class="content">
                    <img src="<?= BASEURL ?>assets/img/a.jpg" class="img-fluid w-100 rounded">
                </div>
            </div>
        </div>
        <div class="col-md-5 col-xs-12 sonhos rounded">
            <h1 class="titulo peso conteudo pt-2" style="font-weight:600;">Sonho #1</h1>
            <h2 class="peso conteudo pt-2 pb-2">Mitsubishi Eclipse '95 GTX</h2>
            <p class="conteudo"><span class="peso">Motivo: </span> Desde que vi este carro no filme <i>Velozes e Furiosos</i>, me apaixonei por ele e vivo dizendo que vou conseguir, um dia, comprar um carro deste mesmo modelo.</p>
        </div>
    </div>

    <div class="row mt-5 sombra2 rounded">
        <div class="col-md-5 col-xs-12 sonhos rounded">
            <h1 class="float-right titulo peso conteudo pt-2" style="font-weight:600;">Sonho #2</h1>
            <h2 class="float-right peso conteudo pt-2 pb-2">Kawasaki Z 900 RS</h2>
            <p class="float-right conteudo"><span class="peso">Motivo: </span> É uma moto com muita personalidade e única, quero ter uma para viajar pelo Brasil e pela América do Sul para conhecer os mais belos pontos turísticos de nosso continente.</p>
        </div>
        <div class="col-md-7 col-xs-12">
            <div class="box">
                <div class="content">
                    <img src="<?= BASEURL ?>assets/img/b.jpg" class="img-fluid w-100 rounded">
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5 sombra rounded">
        <div class="col-md-7 col-xs-12">
            <div class="box">
                <div class="content">
                    <img src="<?= BASEURL ?>assets/img/c.jpg" class="img-fluid w-100 rounded">
                </div>
            </div>
        </div>
        <div class="col-md-5 col-xs-12 sonhos rounded">
            <h1 class="titulo peso conteudo pt-2" style="font-weight:600;">Sonho #3</h1>
            <h2 class="peso conteudo pt-2 pb-2">Conhecer o Urulu</h2>
            <p class="conteudo"><span class="peso">Motivo: </span> Para mim, é o mais belo ponto turístico do país mais peculiar do mundo, a Austrália. Dizem que ele tem uma magnífica vista do pôr do sol, quero presenciar esse evento ao vivo.</p>
        </div>
    </div>

    <div class="row mt-5 sombra2 rounded">
        <div class="col-md-5 col-xs-12 sonhos rounded">
            <h1 class="float-right titulo peso conteudo pt-2" style="font-weight:600;">Sonho #4</h1>
            <h2 class="float-right peso conteudo pt-2 pb-2">Ver o AC/DC ao vivo</h2> 
            <p class="float-right conteudo"><span class="peso">Motivo: </span> Porque eles são a melhor banda de Hard Rock de todos os tempos e representam a cultura de um perído no tempo que está cada vez mais distante de nós, além do fato de que eles estão ficando mais idosos, então as chances de adiar esse sonho são pequenas.</p>
        </div>
        <div class="col-md-7 col-xs-12">
            <div class="box">
                <div class="content">
                    <img src="<?= BASEURL ?>assets/img/d.jpg" class="img-fluid w-100 rounded">
                </div>
            </div>
        </div>
    </div>  
    
    <div class="row mt-5 sombra conteudo rounded">
        <div class="col-md-7 col-xs-12 conteudo">
            <div class="box">
                <div class="content">
                    <img src="<?= BASEURL ?>assets/img/e.jpg" class="img-fluid w-100 rounded">
                </div>
            </div>
        </div>
        <div class="col-md-5 col-xs-12 sonhos rounded conteudo">
            <h1 class="titulo peso conteudo pt-2" style="font-weight:600;">Sonho #5</h1>
            <h2 class="peso conteudo pt-2 pb-2">Saltar de Paraquedas</h2>
            <p class="conteudo"><span class="peso">Motivo: </span> Em geral, o salto de paraquedas é realizado sobre bonitos cenários, que incluem paisagens incríveis. Supostamente, a adrenalina envolvida no esporte é tão grande, que é capaz de proporcionar um prazer enorme pela queda livre.</p>
        </div>
    </div>

</div>