<div class="wrapper">
        <nav id="sidebar">
            <div class="sidebar-header text-center">
                <h3><h1 class="display-5 titulo">Victor Sousa</h1></h3>
            </div>
            <ul class="list-unstyled components">
            <p class="citacao mt-4 mb-5">Quanto mais a gente agradece, mais coisas boas acontecem.</p>
                <li>
                    <a href="<?= BASEURL ?>index.php">Home</a>
                </li>
                <li>
                    <a href="<?= BASEURL ?>paginas/familia.php">Família</a>
                </li>
                <li>
                    <a href="<?= BASEURL ?>paginas/outros.php">Hobbies</a>
                </li>
            </ul>

            <div class="wrapperer">
                <input type="checkbox" name="" id="switch">
                <label for="switch" class="switch-label">
                    <div class="toggleer"></div>
                </label>
            </div>

            <div class="row justify-content-center text-white redes pb-1 pt-3">
                       <a href="https://br.pinterest.com/vicsousa008/" target="_blank" class="nav-link waves-effect waves-light">
                            <i class="fab fa-pinterest-square"></i>
                        </a>
                        <a href="https://www.linkedin.com/in/victor-sousa-49b1b2172/" target="_blank" class="nav-link waves-effect waves-light">
                            <i class="fab fa-linkedin"></i>
                        </a>
                    </div>
        </nav>
        
        <div id="content" class="conteudo">
            <nav class="navbar-expand-lg text-light">
                <div class="container-fluid">
                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </nav>