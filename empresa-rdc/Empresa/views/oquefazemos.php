<div class="container-fluid">	
	<section class="mt-5 mb-4 pr-5 pl-5">
		<div class="row">
			<div class="col-xs-12 col-md-8 pt-5">
				<h2 class="h2-responsive font-weight-bold pt-3 pl-1 pb-3">O que fazemos</h2>
				<p class="pr-5" >Nós desenvolvemos incríveis websites com gráficos estonteantes. Ajudamos e treinamos aspirantes a web designers e artistas gráficos para que alcancem seu potencial máximo.</p>
			</div>
			<div class="col-xs-12 col-md-4">	
				<img class="img-fluid" src="assets/img/oqf.png" />
			</div>
		</div>
	</section>
</div>