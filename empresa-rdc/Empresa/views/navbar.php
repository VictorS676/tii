<header>
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark white scrolling-navbar">
    <a style="color:black;" class="navbar-brand" href="#"><strong>HYPS</strong></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon elegant-color"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a style="color:black;font-family:'Open Sans';" class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a style="color:black;font-family:'Open Sans';" class="nav-link" href="#">Produtos/Serviços</a>
        </li>
        <li class="nav-item">
          <a style="color:black;font-family:'Open Sans';" class="nav-link" href="#">Objetivos</a>
        </li>
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
			  aria-expanded="false" style="color:black;font-family:'Open Sans';" >Empresa</a>
			<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
			  <a class="dropdown-item" href="#">Equipe</a>
			  <a class="dropdown-item" href="#">História</a>
			</div>
		 </li>
        <li class="nav-item">
          <a style="color:black;font-family:'Open Sans';" class="nav-link" href="#">Contato</a>
        </li>		  
      </ul>
      <ul class="navbar-nav nav-flex-icons">
        <li class="nav-item">
          <a class="nav-link"><i style="color:black;" class="fab fa-facebook-f"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link"><i style="color:black;" class="fab fa-twitter"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link"><i style="color:black;" class="fab fa-instagram"></i></a>
        </li>
      </ul>
	  <form class="form-inline">
		  <div class="md-form my-0">
			<input class="text-dark" class="form-control mr-sm-2" style=" border-bottom: 1px solid black;" type="text" placeholder="Buscar" aria-label="Search">
		  </div>
	  </form>
    </div>
  </nav>
</header>