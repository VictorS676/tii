<div class="container">	
	<section>	
		<div class="row">
			<div class="card col-md-4 mt2 pb-2 mx-auto">
			  <div class="view overlay">
				<img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(<?= $v[$card]['image'] ?>).jpg" alt="Card image cap">
				<a href="#!">
				  <div class="mask rgba-white-slight"></div>
				</a>
			  </div>
			  <div class="card-body">
				<h4 class="card-title"><?= $v[$card]['title'] ?></h4>
				<p class="card-text"><?= $v[$card]['desc']; ?></p>
				<a href="#" class="btn btn-cyan"><?= $v[$card]['label'] ?></a>
			  </div>
				<div class="card-footer">
					<p><?= dataBR() ?></p>
				</div>
			</div>
		</div>	
	</section>
</div>	