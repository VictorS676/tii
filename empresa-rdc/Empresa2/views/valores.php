<div class="container">	
	<section class="mt-5 mb-5">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center mb-5 pb-3" style="border-bottom: 1px solid #eeeeee;">Nossos valores empresariais</h1>
				<p class="text-center">Os valores da empresa definem a nossa cultura empresarial, e nossa equipe se esforça para vivê-los todos os dias em tudo o que fazemos</p>
				<div class="col-12">
					<div class="list-group-flush">
						<div class="list-group-item">
							<p class="mb-1"> <i class="fab fa-slack-hash fa-3x mr-5 mr-5  yellow accent-3 p-3 dark-text rounded" aria-hidden="true"></i>Simplicidade</p>
						</div>
						<div class="list-group-item">
							<p class="mb-1"><i class="fab fa-instagram fa-3x  mr-5 mr-5  blue-grey darken-4 p-3 white-text rounded " aria-hidden="true"></i>Transparência</p>
						</div>
						<div class="list-group-item">
							<p class="mb-1"><i class="fab fa-snapchat fa-3x  mr-5 mr-5  blue-grey darken-4 p-3 white-text rounded" aria-hidden="true"></i>Meritocracia</p>
						</div>
						<div class="list-group-item">
							<p class="mb-1"><i class="fab fa-facebook fa-3x  mr-5 mr-5   blue-grey darken-4 p-3 white-text rounded" aria-hidden="true"></i>Social</p>
						</div>
						<div class="list-group-item">
							<p class="mb-1"><i class="fab fa-google fa-3x  mr-5 mr-5   blue-grey darken-4 p-3 white-text rounded" aria-hidden="true"></i>Ajuda</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>	