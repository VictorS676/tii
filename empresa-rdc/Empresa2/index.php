<?php
include 'config/constants.php';
include 'views/header.php';
include 'views/navbar.php';

include 'views/jumbotron.php';
include 'views/valores.php';
include 'views/quemsomos.php';
include 'views/oquefazemos.php';
include 'views/pqsomos.php';
include 'views/habilidades.php';
include 'views/mapa.php';
include 'views/contato.php';

include 'views/rodape.php';
include 'views/footer.php';

?>