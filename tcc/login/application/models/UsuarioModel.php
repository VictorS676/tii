<?php
include_once APPPATH.'libraries/User.php';

class UsuarioModel extends CI_Model 
{
    
    public function salva_usuario(){
        if(sizeof($_POST) == 0) return;
        
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required',
                array('required' => 'Você deve definir uma %s.')
        );
        $this->form_validation->set_rules('confsenha', 'Confirmação de Senha', 'trim|required|matches[senha]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[usuario.email]');      

        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('form/myform');
        }
        else
        {
            $data = array( 
                'nome'=>  $_POST['nome'], 
                'email'=>  $_POST['email'], 
                'senha'=>  md5($_POST['senha'])
            );
			$user = new User();
            $user->cria_usuario($data);
            redirect('login/sucesso');
        }
    }
	
	public function logar($email, $senha)
	{
		$this->db->where("email", $email);
		$this->db->where("senha", $senha);
		$usuario = $this->db->get("usuario")->row_array();
		return $usuario;
	}	

}