<div class="section landing-section" id="contato">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <h2 class="text-center">FALE COMIGO</h2>
            <form class="contact-form">
              <div class="row">
                <div class="col-md-6">
                  <label>Nome</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="nc-icon nc-single-02"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Seu Nome">
                  </div>
                </div>
                <div class="col-md-6">
                  <label>Email</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="nc-icon nc-email-85"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Seu Email">
                  </div>
                </div>
              </div>
              <label>Mensagem</label>
              <textarea class="form-control" rows="4" placeholder="Escreva suas dúvidas ou elogios para nós!"></textarea>
              <div class="row">
                <div class="col-md-4 ml-auto mr-auto">
                  <button class="btn bot-dourado btn-lg btn-fill">Enviar Mensagem</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>