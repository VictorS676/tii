    <div class="section section-dark text-center">
      <div class="container">
        <h2 class="title">QUEM SOU</h2>
          <div class="row">
            <div class="col-md-4 mx-auto">
              <div class="card card-profile card-plain">
                <div class="card-avatar">
                  <a href="#avatar">
                    <img src="<?= base_url(); ?>public/assets/img/cliente.jpg" alt="...">
                  </a>
                </div>
                <div class="card-body">
                  <a href="#paper-kit">
                    <div class="author">
                      <h4 class="card-title">Laila Maria</h4>
                      <h6 class="card-category">Esteticista</h6>
                    </div>
                  </a>
                  <p class="card-description text-center"><q>Tudo começou tive o desejo de ter um negócio próprio, a partir daí comecei a procurar por cursos de especialização ligados à área da estética, especificamente na área de extensão de cílios</q></p>
                </div>
                <div class="card-footer text-center">
                  <a href="https://www.facebook.com/spacelashes/" class="btn btn-link btn-just-icon btn-neutral" target="_blank"><i class="fa fa-facebook"></i></a>
                  <a href="https://www.instagram.com/_space_lashes/" class="btn btn-link btn-just-icon btn-neutral" target="_blank"><i class="fa fa-instagram"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>