<div class="main">
    <div class="section text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <h2 class="title">CONHEÇA OS MODELOS DE CÍLIOS</h2>
            <h5 class="description">Para mulheres modernas e elegantes. </h5>
            <br>
          </div>
        </div><br/><br/>
        <div class="row">
          <div class="col-md-4 mx-auto">
            <div class="info">
              <div class="icon">
                <img src="<?= base_url(); ?>public/assets/img/fioafio.png" class="img-fluid" />
              </div>
              <div class="description">
                <h4 class="info-title">Fio a Fio</h4>
                <p>O alongamento de cílios fio a fio traz um método que consiste na aplicação de um fio sintético ou de seda sobre cada cílio natural.</p>
                <a style="display:none;" href="#" class="btn btn-link"><span class="link-dor" >Saiba +</span></a>
              </div>
            </div>
          </div>
          <div class="col-md-4 mx-auto">
            <div class="info">
              <div class="icon">
              <img src="<?= base_url(); ?>public/assets/img/volrusso.png" class="img-fluid" />
              </div>
              <div class="description">
                <h4 class="info-title">Volume Russo</h4>
                <p>No volume russo é aplicado um leque com maior quantidades de sintéticos e de espessura mais fina e mais leve.</p>
                <a style="display:none;" href="#" class="btn btn-link"><span class="link-dor">Saiba +</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	