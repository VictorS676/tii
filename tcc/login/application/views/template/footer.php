    <footer class="footer footer-black  footer-white">
      <div class="container">
        <div class="row">
          <nav class="footer-nav">
            <ul>
              <li style="display:none;"><a href="#">Galeria</a></li>
              <li><a href="<?= base_url(); ?>pages/historia">História</a></li>
            </ul>
          </nav>
          <div class="credits ml-auto">
            <span class="copyright">© Space Lashes <?= date('Y'); ?>, feito com <i class="fa fa-heart heart"></i> por <a target="_blank" class="cb-link" href="http://hospedagem.ifspguarulhos.edu.br/~gu1800078/cubo/">CUBO</a>.</span>
          </div>
        </div>
      </div>
    </footer>