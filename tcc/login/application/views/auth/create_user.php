<div class="page-header pt-5" style="background-image: url('<?= base_url(); ?>public/assets/img/fundo.jpg');">
    <div class="filter"></div>
    <div class="container pt-5">
      <div class="row">
        <div class="col-lg-6 mx-auto">
          <div class="card card-register"> 
                  <div class="text-center">
                        <h2><?= lang('create_user_heading');?></h2>
                        <p><?= lang('create_user_subheading');?></p>
                  </div>
                  <div id="infoMessage"><?= $message;?></div>
                  <?= form_open("auth/create_user");?>
                        <p>
                              <?= lang('create_user_fname_label', 'first_name');?><br/>
                              <?= form_input($first_name);?>
                        </p>
                        <p>
                              <?= lang('create_user_lname_label', 'last_name');?><br/>
                              <?= form_input($last_name);?>
                        </p>
                        <?php
                              if($identity_column!=='email') {
                              echo '<p>';
                              echo lang('create_user_identity_label', 'identity');
                              echo '<br />';
                              echo form_error('identity');
                              echo form_input($identity);
                              echo '</p>';
                              }
                        ?>
                        <p>
                              <?= lang('create_user_company_label', 'company');?><br/>
                              <?= form_input($company);?>
                        </p>
                        <p>
                              <?= lang('create_user_email_label', 'email');?><br/>
                              <?= form_input($email);?>
                        </p>
                        <p>
                              <?= lang('create_user_phone_label', 'phone');?><br/>
                              <?= form_input($phone);?>
                        </p>
                        <p>
                              <?= lang('create_user_password_label', 'password');?><br/>
                              <?= form_input($password);?>
                        </p>
                        <p>
                              <?= lang('create_user_password_confirm_label', 'password_confirm');?><br/>
                              <?= form_input($password_confirm);?>
                        </p>
                  <div class="col-6 mx-auto">
                        <p><?= form_submit('submit', lang('create_user_submit_btn'), 'class="btn bot-dourado btn-block btn-round"');?></p>
                  </div>
            <?= form_close();?>
            </div>
        </div>
      </div>
    </div>
    <div class="footer register-footer text-center">
      <h6>© Space Lashes <?= date('Y'); ?>, feito com <i class="fa fa-heart heart"></i> por <a class="cb-link" target="_blank" href="http://hospedagem.ifspguarulhos.edu.br/~gu1800078/cubo/">CUBO</a>.</h6>  
    </div>
  </div>  