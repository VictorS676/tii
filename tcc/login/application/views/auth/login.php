<div class="page-header pt-5" style="background-image: url('http://localhost/space_lashes/public/assets/img/fundo.jpg');">
  <div class="filter"></div>
    <div class="container pt-5">
      <div class="row">
        <div class="col-lg-4 ml-auto mr-auto">
          <div class="card card-register">

            <h1 class="h3 mb-4 text-center" ><?= lang('login_heading');?></h1>
            <p><?= lang('login_subheading');?></p>
            <div id="infoMessage"><?= $message;?></div>
            <?= form_open("auth/login");?>

              <p>
                <?= lang('login_identity_label', 'identity');?>
                <?= form_input($identity); ?>
              </p>
              <p>
                <?= lang('login_password_label', 'password');?>
                <?= form_input($password);?>
              </p>
              <p style="display:none;">
                <?= lang('login_remember_label', 'remember');?>
                <div class="form-check text-center">
				          <label class="form-check-label">
                    <?= form_checkbox('remember', '1', FALSE, 'id="remember"', 'class="form-check-input"');?>
                    <!-- <span class="form-check-sign"></span> -->
                  </label>
                </div>
              </p>
              <p><?= form_submit('submit', lang('login_submit_btn'), 'class="btn bot-dourado btn-block btn-round"');?></p>

            <?= form_close();?>

            <p class="forgot">
              <a href="forgot_password" class="btn btn-link btn-danger">
                <?= lang('login_forgot_password');?>
              </a>
            </p>

            </div>
        </div>
      </div>
    </div>
    <div class="footer register-footer text-center">
      <h6>© Space Lashes <?= date('Y'); ?>, feito com <i class="fa fa-heart heart"></i> por <a class="cb-link" target="_blank" href="http://hospedagem.ifspguarulhos.edu.br/~gu1800078/cubo/">CUBO</a>.</h6>  
    </div>
  </div>  