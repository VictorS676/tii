<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('template/navbar');
		$this->load->view('template/intro');
		
        $this->load->view('template/servicos');
        $this->load->view('template/sobre');
        $this->load->view('template/valores');
        $this->load->view('template/galeria');
        $this->load->view('template/contato');
        
		$this->load->view('template/footer');
		$this->load->view('template/scripts');
	}
}
