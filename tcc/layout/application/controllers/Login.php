<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('template/navbar');
		
		$this->load->view('extra/login');
		
		$this->load->view('template/scripts');
	}

	public function registro()
	{
		$this->load->view('template/header');
		$this->load->view('template/navbar');
		
		$this->load->view('extra/cadastro');
		
		$this->load->view('template/scripts');
	}

}

?>	