    <div class="section section-dark text-center">
      <div class="container">
        <h2 class="title">QUEM SOU</h2>
          <div class="row">
            <div class="col-md-4 mx-auto">
              <div class="card card-profile card-plain">
                <div class="card-avatar">
                  <a href="#avatar">
                    <img src="<?= base_url(); ?>public/assets/img/faces/clem-onojeghuo-3.jpg" alt="...">
                  </a>
                </div>
                <div class="card-body">
                  <a href="#paper-kit">
                    <div class="author">
                      <h4 class="card-title">Laila Maria</h4>
                      <h6 class="card-category">Esteticista</h6>
                    </div>
                  </a>
                  <p class="card-description text-center">
                    Teamwork is so important that it is virtually impossible for you to reach the heights of your capabilities or make the money that you want without becoming very good at it.
                  </p>
                </div>
                <div class="card-footer text-center">
                  <a href="#" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
                  <a href="#" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-facebook"></i></a>
                  <a href="#" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-instagram"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>