 <nav class="navbar navbar-expand-lg fixed-top navbar-transparent " color-on-scroll="300">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="<?= base_url(); ?>">
          <span class="display-6"><span class="prim">S</span>PACE <span class="prim">L</span>ASHES</span>
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#">História</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Extensão de Cílios</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Galeria</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contato</a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>login/registro" class="btn bot-semlinha-dourado btn-round">Cadastro</a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>login" class="btn bot-dourado btn-round">Login</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>