<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="<?= base_url(); ?>public/assets/img/letras2.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Space Lashes</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>public/assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?= base_url(); ?>public/assets/css/paper-kit.css" rel="stylesheet" />
  <link href="<?= base_url(); ?>public/assets/css/estilo.css" rel="stylesheet" />
</head>
<body class="index-page sidebar-collapse">