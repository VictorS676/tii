    <script src="<?= base_url(); ?>public/assets/js/core/jquery-3.4.1.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>public/assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>public/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?= base_url(); ?>public/assets/js/core/mdb.min.js"></script>
	
    <script src="<?= base_url(); ?>public/assets/js/plugins/bootstrap-switch.js"></script>
    <script src="<?= base_url(); ?>public/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>public/assets/js/plugins/moment.min.js"></script>
    <script src="<?= base_url(); ?>public/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>public/assets/js/paper-kit.js?v=2.2.0" type="text/javascript"></script>
    <script>
      $(document).ready(function() {

        if ($("#datetimepicker").length != 0) {
          $('#datetimepicker').datetimepicker({
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            }
          });
        }

        function scrollToDownload() {

          if ($('.section-download').length != 0) {
            $("html, body").animate({
              scrollTop: $('.section-download').offset().top
            }, 1000);
          }
        }
      });
    </script>
</body>
</html>