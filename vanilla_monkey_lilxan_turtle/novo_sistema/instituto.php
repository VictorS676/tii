<!DOCTYPE html>
<html>
<div class="macaxera">
</div>
<div id='cssmenu'>
<ul>
   <li><a href='index.php#'>área do aluno</a></li>
   <li><a href='professor.php#'>área do professor</a></li>
   <li class='active'> <a href='instituto.php#'>Instituto</a></li>
   <li><a href='contato.php'>Contato</a></li>
</ul>
</div>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campus Chico Lopes</title>
    <link rel="stylesheet" type="text/css" href="inde.css"/>
<link rel="shortcut icon" type="image/x-icon" href="imagens/favicon.png">
<body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.2/flexslider.css">
<link rel="stylesheet" href="flex.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.2/jquery.flexslider.js"></script>
<script>
$(document).ready(function () {
if (jQuery().flexslider) {
    $('.flexslider-ticker').each(function() {
      var tickerSettings =  {
        animation: "slide",
        animationLoop: false,
        selector: ".items > .item",
        move: 1,
        controlNav: false,
        slideshow: true,
        direction: 'vertical'
      };
      $(this).flexslider(tickerSettings);
    });
    $('.flexslider').each(function() {
      var sliderSettings =  {
        animation: $(this).attr('data-transition'),
        selector: ".slides > .slide",
        controlNav: false,
        smoothHeight: true,
        prevText: "",
        nextText: "",
        sync: $(this).data('slidernav') || '',
        start: function(slider) {
          if (slider.find('[data-slide-bg-stretch=true]').length > 0) {
            slider.find('[data-slide-bg-stretch=true]').each(function() {
              if ($(this).data('slide-bg')) {
                $(this).backstretch($(this).data('slide-bg'));
              }
            });
          }
        }
      };     
      $('html').addClass('has-flexslider');
      $(this).flexslider(sliderSettings);
    });
    $(window).delay(1000).trigger('resize');
}})
</script>
<div class="container slider-container">
   <div class="row">
      <div class="col-md-12 slider-left">
         <section class="flexslider-wrapper">
            <div id="main-slider" class="flexslider" data-transition="fade" data-page-class="slider-full-width">
               <div class="slides">
                  <div data-slide-alt="alt" data-slide-bg-stretch=true class="slide slide-bg" data-slide-bg="imagens/ce.jpg">
                     <div class="slide-caption">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="slide-text">
                                 <div class="slide-title">Escola Siena</div>
                                 Excelência em educação para crianças e jovens
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div data-slide-alt="alt" data-slide-bg-stretch=true class="slide slide-bg" data-slide-bg="imagens/ce1.jpg">
                     <div class="slide-caption">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="slide-text">
                                 <div class="slide-title">Qualidade de ensino</div>
                                 Os professores são bem qualificados, atenciosos e, além disso, as salas de aulas possuam alta tecnologia com lousas digitais.
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div data-slide-alt="alt" data-slide-bg-stretch=true class="slide slide-bg" data-slide-bg="imagens/ce2.jpg">
                     <div class="slide-caption">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="slide-text">
                                 <div class="slide-title">Segurança</div>
                                 Aqui, na Escola Siena, a segurança para com nossos alunos é uma prioridade incorruptível
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </section>
         </div>
      </div>
   </div>
</div>

<div class="circle-1">
  <h1>Visão</h1>
  <p>Ser reconhecido como referencial de qualidade na educação brasileira.</p>
  <i class="fas fa-flask"></i>
</div>

<div class="circle-2">
  <h1>Missão</h1>
  <p>Formar alunos competentes e autônomos, cognitivamente e moralmente, a partir do desenvolvimento de suas potencialidades intelectuais, afetivas e motoras e de sua conscientização diante de deveres e direitos enquanto cidadãos.</p>
<i class="fas fa-toolbox" id="sob"></i>
</div>

<div class="circle-3">
  <h1>Objetivos Educacionais</h1>
  <p>Melhorar permanentemente o processo de ensino-aprendizagem, a vivência de valores éticos e a prática da cidadania.</p>
<i class="fas fa-book-open"></i>
</div>
<div class="espaco">
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>	
<div class="usando">
<h1>Referência Nacional em Educação</h1>
<p>Entendemos que, na Escola Siena, mais importante do que aprender é desenvolver a capacidade de aprender, pensar, conviver e resolver problemas. Procuramos oferecer aos nossos alunos uma educação mais formativa possível e não apenas informativa.</p>
<?php include('ensinos.php'); ?>
</div>
<style type="text/css">

.usando{
	background-color:#2196F3;
	width:100%;
	padding-top:2%;
	padding-bottom:2%;
	padding-left:8%;
	padding-right:8%;
	color:#fff;
	text-align:center;
}

.usando h1{
	font-size:36px;
}

.usando p{
	font-size:18px;
}

i{
	font-size:82px;
	margin-left:35%;
	margin-top:5%;
}

#sob{
	margin-top:-2%;
}

h1{
	font-size:24px;
}

.circle-1,
.circle-2,
.circle-3 {
  color: #fff;
  padding: 5%;
  width: 380px;
  height: 380px;
  margin: 0 -15px;
  mix-blend-mode: multiply;
  display:block;
  float:left;
  font-size:16px;
  line-height:18px;
}

.circle-1 {
  background: #0277BD;
  margin-left:10%;
  border-radius: 50% 50% 50% 70%/50% 50% 70% 60%;
}
.circle-2 {
  background: #2196F3;
  border-radius: 80% 30% 50% 50%/50%;
}
.circle-3 {
  background: #1A237E;
  border-radius: 40% 40% 50% 40%/30% 50% 50% 50%;
}

@media (max-width: 600px) {
  body {
    flex-direction: column;
  }
}


	@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,900');
	@import url('https://fonts.googleapis.com/css?family=Raleway:100,100i,300,400,500,600,700,800');

	body {
		color: #6b6b6b;
		font-family: 'Roboto', sans-serif;
		font-size: 16px;
		line-height: 24px;
	}

.profilebox {
	width: 175px;
	height: 150px;
	background: #eee;
	cursor: pointer;
	float: left;
	margin: 30px;
}

.profilebox1 {
	background-image: url("https://image.ibb.co/kzeSqw/lunch1.jpg");
	background-position: center center;
	background-size: cover;
	perspective: 450px;
	position: relative;
}

.profilebox2 {
	background-image: url("https://image.ibb.co/kzeSqw/lunch1.jpg");
	background-position: center center;
	background-size: cover;
	perspective: 450px;
	position: relative;
}

.profilebox3 {
	background-image: url("https://image.ibb.co/kzeSqw/lunch1.jpg");
	background-position: center center;
	background-size: cover;
	perspective: 450px;
	position: relative;
}

.profilebox4 {
	background-image: url("https://image.ibb.co/kzeSqw/lunch1.jpg");
	background-position: center center;
	background-size: cover;
	perspective: 450px;
	position: relative;
}

.profilebox5 {
	background-image: url("https://image.ibb.co/kzeSqw/lunch1.jpg");
	background-position: center center;
	background-size: cover;
	perspective: 450px;
	position: relative;
}

.profilebox .SocialIcons {
	display: flex;
	flex-direction: column;
	left: 0;
	opacity: 0;
	position: absolute;
	top: 15px;
	transform: rotateY(90deg);
	transform-origin: left center 0;
	transition: all 350ms ease;
}

.profilebox .SocialIcons a {
	background: #ffffff none repeat scroll 0 0;
	font-size: 20px;
	height: 50px;
	margin-bottom: 1px;
	position: relative;
	text-align: center;
	width: 50px;
}

.profilebox .SocialIcons a i {
	left: 50%;
	position: absolute;
	top: 50%;
	transform: translateX(-50%) translateY(-50%);
}

.profilebox .SocialIcons a:nth-child(1) {
}
.profilebox .SocialIcons a:nth-child(2) {
}
.profilebox .SocialIcons a:nth-child(3) {
}
.profilebox:hover .SocialIcons {
	opacity: 1;
	left: 15px;
	transform: rotateY(0deg);
}
.profilebox .profileInfo {
	background: #ffffff none repeat scroll 0 0;
	bottom: 0;
	height: 50px;
	left: 0;
	margin: 0 auto;
	position: absolute;
	right: 0;
	width: 90%;
	transform: rotatex(90deg);
	transform-origin: bottom center 0;
	transition: all 350ms ease;
	opacity: 0;
}
.profilebox .profileInfo h3 {
	font-size: 20px;
	margin: 15px 0;
	text-align: center;
}
.profilebox:hover .profileInfo {
	opacity: 1;
	bottom: 15px;
	transform: rotatex(0deg);
}
</style>
<br/><br/>
<div class="social">
		<div class="section">
			<div class="rede" id="facebook">
				<img class="icone" src="imagens/facebook.png" />
			</div>
			<div class="rede" id="twitter">
				<img class="icone" src="imagens/twitter.png" />
			</div>
			<div class="rede" id="instagram">
				<img class="icone" src="imagens/instagram.png" />
			</div>
			<div class="rede" id="youtube">
				<img class="icone" src="imagens/youtube.png" />
			</div>
		</div>
	</div>  
	<br/><br/><br/><br/><br/>  <center>
<div class="macaquinho"> 
<u>Escola Siena</u><br/>
<b>Rua:</b> Conselheira Moreiro do Barroso Nº 311 – Snt. Thereza – CEP: 02480-222 – São Paulo – SP<br/>
<b>Telefones:</b> (011) 5562-2555 / 4305-5632 / 91155-7635<br/>
<b>E-mail:</b> contato@escolasiena.com.br<br/>
</div> 
</center>

</body>
</html>