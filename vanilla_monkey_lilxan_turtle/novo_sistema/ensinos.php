	<ul id="projects">
		<li id="p1" class="flipper">
			<div class="front"><img src="imagens/inf.jpg" height="100%" width="auto">
			</div>
			<div class="back">
				<h2>Educação Infantil</h2>
			</div>
		</li>
		<li id="p2" class="flipper">
			<div class="front"><img src="imagens/fund1.jpg" height="auto" width="100%">
			<h3>Ensino Fundamental I</h3>
			</div>
			<div class="back">
				<h2>Ensino Fundamental I</h2>
			</div>
		</li>
		<li id="p3" class="flipper">
			<div class="front"><img src="imagens/fund2.jpg" height="auto" width="100%"><h2>Ensino Fundamental II</h2></div>
			<div class="back">
				<h2>Ensino Fundamental II</h2>
			</div>
		</li>
	</ul>
<style type="text/css">

#projects {
	width: 1200px;
	margin: 30px auto;
	perspective: 1000;
}

#projects .flipper {
	width: 400px;
	height: 300px;
	display: inline-block;
	background-color:#fff;
	color:#1565C0;
	
	-webkit-transform: scale(0.90);
	-moz-transform: scale(0.90);
	-o-transform: scale(0.90);
	-ms-transform: scale(0.90);
	transform: scale(0.90);

	-webkit-box-shadow: 0 3px 5px rgba(0,0,0,.2);
	box-shadow: 0 3px 5px rgba(0,0,0,.2);

	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	-o-transition: all 0.3s;
	-ms-transition: all 0.3s;
	transition: all 0.3s;

	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	-ms-transform-style: preserve-3d;
	transform-style: preserve-3d;
	position: relative;
}

#projects .flipper:hover {
	-webkit-transform: scale(1);
	-moz-transform: scale(1);
	-o-transform: scale(1);
	-ms-transform: scale(1);
	transform: scale(1);

	-webkit-box-shadow: 0 5px 25px rgba(0,0,0,.2);
	box-shadow: 0 5px 25px rgba(0,0,0,.2);
	cursor: pointer;
}

#projects .flipper.blur {
	-webkit-filter: blur(3px);

	-webkit-transform: scale(0.88);
	-moz-transform: scale(0.88);
	-o-transform: scale(0.88);
	-ms-transform: scale(0.88);
	transform: scale(0.88);
	filter: alpha(opacity=60);
	opacity: 0.6;
}

#projects .flipper.rotate {
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

#projects:hover .clicked {
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.front,.back {
	width: 400px;
	height: 300px;
	position: absolute;
	top: 0;
	left: 0;
	backface-visibility: hidden;
}

.front {
	z-index: 2;
}

.back {
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	transform: rotateY(180deg);
	width: 360px;
	height: 260px;
	padding: 20px;
	z-index: 1;
	background: white;
}

h3{
	margin-top:0.75%;
}

</style>
<script type="text/javascript">
$('#projects > li').hover(function(){
  $(this).siblings().addClass('blur');
}, function(){
  $(this).removeClass('clicked').siblings().removeClass('blur');
  
});

$('#projects > li').click(function(e){
  $(this).addClass('clicked');
});
</script>