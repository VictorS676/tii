<section class="mounth" id="january">
  <header>
    <h1>NOVEMBRO 2018</h1>
  </header>
  <article>
    <div class="days">
      <b>SEG</b>
      <b>TER</b>
      <b>QUA</b>
      <b>QUI</b>
      <b>SEX</b>
      <b>SAB</b>
      <b>DOM</b>
    </div>
    <div class="dates">
      <span class="disable">25</span>
      <span class="disable">26</span>
      <span class="disable">27</span>
      <span class="disable">28</span>
      <span class="disable">29</span>
      <span class="disable">30</span>
      
      <span>1</span>
      <span>2</span>
      <span>3</span>
      <span>4</span>
      <span>5</span>
      <span>6</span>
      <span class="active">7</span>
      <span>8</span>
      <span>9</span>
      <span>10</span>
      <span>11</span>
      <span>12</span>
      <span>13</span>
      <span>14</span>
      <span>15</span>
      <span>16</span>
      <span>17</span>
      <span>18</span>
      <span class="fest">19</span>
      <span class="fest">20</span>
      <span>21</span>
      <span>22</span>
      <span class="semprov">23</span>
      <span class="semprov">24</span>
      <span class="semprov">25</span>
      <span class="semprov">26</span>
      <span class="semprov">27</span>
      <span class="semprov">28</span>
      <span>29</span>
      <span>30</span>
      
      <span class="disable">1</span>
      <span class="disable">2</span>
      <span class="disable">3</span>
      <span class="disable">4</span>
      <span class="disable">5</span>
    </div>
  </article>
  
  <article class="avisos">
	<h1>Avisos: </h1>
		<p><i class="dias1">19-20</i> — Confraternização.</p>
		<p><i class="dias2">23-28</i> — Semana de provas.</p>
</article>
</section>


<style type="text/css">

::selection {
  background: rgba(0,0,0,.05);
}

ul, h1, h2, h3, h4 {
  margin: 0; padding: 0;
  list-style: none;
}
a, li,
header nav span,
article span {
  text-decoration: none;
  transition: all .25s ease-in-out;
}
h1 {
  font: 300 28px 'Open Sans';
  color: #4c5373;
}

section.mounth {
  overflow: hidden;
  display: block;
  float: left;
  width: 420px; height: auto;
  background: #fff;
  box-shadow: 0 1px 1px rgba(0,0,0,.25);
  border-radius: 5px;
  margin-left:3%;
  margin-right:5%;
}
  header {
    position: relative;
    display: block;
    height: 80px;
    background: #e9eaf0;
    box-shadow: inset 0 -1px 0 rgba(0,0,0,.06);
  }
    header h1 {
      margin-left: 30px;
      line-height: 80px;
    }
    header nav {
      overflow: hidden;
      position: absolute;
      display: block;
      right: 30px; top: 30px;
    }
      header nav span {
        display: block;
        float: left;
        width: 11px; height: 20px;
        margin-left: 30px;
        cursor: pointer;
        background: url('http://ysfu.net/sprite.png') no-repeat;
      }
        header nav span:first-of-type {
          background-position: -77px -4px;
        }
          header nav span:first-of-type:hover {
            background-position: -106px -4px;
          }
        header nav span:last-of-type {
          background-position: -90px -4px;
        }
          header nav span:last-of-type:hover {
            background-position: -119px -4px;
          }

article {padding: 6px 0 25px 25px}
  article b,
  article span {
    display: block;
    float: left;
    width: 45px; height: 45px;
    margin: 4px;
    text-align: center;
  }
  article b {
    font: 600 16px/45px 'Open Sans';
    color: #91d9ff;
  }
  article span {
    font: 300 19px/45px 'Open Sans';
    color: #4c5373;
    box-shadow: inset 0 0 1px 1px #d9dce5;
    border-radius: 2px;
    cursor: pointer;
  }
  article span.disable {
    color: #b6bacc;
    box-shadow: inset 0 0 1px 1px #f3f4f7;
  }
    article span:not(.disable):hover {
      background: #cdd0dd;
      box-shadow:
        inset 0 0 1px 1px #d9dce5,
        0 1px 3px -1px #cdd0dd;
    }
    article span:not(.disable):active {
      background: #b9becf;
      box-shadow:
        inset 0 1px 2px rgba(0,0,0,.18),
        0 0 0 #cdd0dd;
    }
    article span.active,
    article span.active:hover,
    article span.active:active {
      color: #fff;
      box-shadow:
        inset 0 0 1px 1px #1565C0,
        0 1px 3px -1px #1565C0;
      background: #1565C0;
    }
  article div {
    display: block;
    overflow: hidden;
    clear: both;
  }

    article span.semprov,
    article span.semprov:hover,
    article span.semprov:active {
      color: #fff;
      box-shadow:
        inset 0 0 1px 1px #6A1B9A,
        0 1px 3px -1px #6A1B9A;
      background: #6A1B9A;
    }
  
      article span.fest,
    article span.fest:hover,
    article span.fest:active {
      color: #fff;
      box-shadow:
        inset 0 0 1px 1px #F4511E,
        0 1px 3px -1px #F4511E;
      background: #F4511E;
    }
  
  
  article.avisos{
	overflow: hidden;
	display: block;
	float: left;
	width: 395px; height: auto;
	background: #fff;
	box-shadow: 0 1px 1px rgba(0,0,0,.25);
	border-radius: 5px;
  }
 
	.dias1{
		color:#F4511E;
	}
 
 	.dias2{
		color:#6A1B9A;
	}
</style>