<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Campus Chico Lopes</title>
			<link rel="stylesheet" type="text/css" href="inde.css"/>
			<link rel="shortcut icon" type="image/x-icon" href="imagens/favicon.png">
				<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
				<script type="text/javascript">
$(document).ready(function(){ <!-- JQuery -->
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
				</script>
</head>
<body>
	<div class="macaxera"></div>
		<div id='cssmenu'>
			<ul>
				<li><a href='index.php'>área do aluno</a></li>
				<li><a href='professor.php'>área do professor</a></li>
				<li><a href='instituto.php'>Instituto</a></li>
				<li class='active'><a href='contato.php'>Contato</a></li>
			</ul>
		</div>
		
		<section class="ajuda">
			<form class="login-caixa" action="pergunta.php" method="post" onSubmit="return valida_campos();">
				<h1>Dúvidas</h1>
					<div class="textocaixa">
						<i class="fas fa-question-circle"></i>
						<input type="email" id="email_visit" name="email_visit" placeholder="Insira seu e-mail para resposta" class="" required="required">
					</div>
					<div class="textocaixa">
						<i class="fas fa-comments"></i>
						<textarea id="comentario" name="comentario" placeholder="Digite sua dúvida." rows="12" cols="50" maxlength="220"></textarea>
					</div>
						<button type="submit" name="manda" class="btnw">Mandar</button>			
			</form>
		</section>
<style type="text/css">
@import "https://use.fontawesome.com/releases/v5.5.0/css/all.css";

body{
	margin:0;
	padding:0;
}

section.ajuda{
	width:100%;
}

.login-caixa{
	width:50%;
	border-radius:5%;
	border-style:double;
	border-color:#000;
	border-width:6px;
	position:relative;
	margin-top:40%;
	left:50%;
	transform:translate(-50%,-50%);
	color:#000;
	background-color:#3db2e1;	
}

.login-caixa h1{
	float:center;
	font-size:40px;
	border-bottom:6px solid #000;
	margin-bottom:50px;
	padding:13px 0;
	text-align:center;
	text-transform:uppercase;
}
	
.textocaixa{
	width:100%;
	overflow:hidden;
	font-size:20px;
	padding:10px 0;
	margin:8px;
	border-bottom:1px solid #fff;
	padding-left:3px;
	padding-right:12px;
}

input::placeholder {
  color: #000;
}

.textocaixa i{
	width:26px;
	float:left;
	text-align:center;
}

.textocaixa input{
	border:none;
	outline:none;
	background:none;
	color:#fff;
	font-size:18px;
	width:80%;
	float:left;
	margin: 0 10px;
}

.btnw{
	width:40%;
	background:#fff;
	border:2px solid #fff;
	color:#000;
	padding:5px;
	font-size:18px;
	cursor:poniter;
	margin:12px 0;
	transition:0.8s;
	margin-top:5%;
	border-radius:20px;
	margin-bottom:2%;
	text-align:center;
	margin-left:30%;
}

.btnw:hover{
	background-color:#A0BCD8;
}
	
textarea{
	font-size:18px;
    background:none;
	border:none;
}	

textarea::placeholder {
    color: #000;
	font-family: 'Source Sans Pro', sans-serif;	
}

</style>
	<div class="social">
		<div class="section">
			<div class="rede" id="facebook">
				<img class="icone" src="imagens/facebook.png" />
			</div>
			<div class="rede" id="twitter">
				<img class="icone" src="imagens/twitter.png" />
			</div>
			<div class="rede" id="instagram">
				<img class="icone" src="imagens/instagram.png" />
			</div>
			<div class="rede" id="youtube">
				<img class="icone" src="imagens/youtube.png" />
			</div>
		</div>
	</div><br/><br/><br/><br/><br/>
	<center>
		<div class="macaquinho"> 
			<u>Escola Siena</u><br/>
			<b>Rua:</b> Conselheira Moreiro do Barroso Nº 311 – Snt. Thereza – CEP: 02480-222 – São Paulo – SP<br/>
			<b>Telefones:</b> (011) 5562-2555 / 4305-5632 / 91155-7635<br/>
			<b>E-mail:</b> contato@escolasiena.com.br<br/>
		</div> 
	</center>
</body>
</html>