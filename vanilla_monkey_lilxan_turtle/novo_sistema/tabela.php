<style>
	.demo {
		width:50%;
		border:1px dotted #C0C0C0;
		border-collapse:collapse;
		padding:5px;
	}
	.demo caption {
		text-align:center;
	}
	.demo th {
		border:1px dotted #C0C0C0;
		padding:5px;
		background:#F0F0F0;
	}
	.demo td {
		border:1px dotted #C0C0C0;
		text-align:center;
		padding:5px;
		background:#FFFFFF;
	}
</style>
<table class="demo">
	<caption>Boletim Escolar Bimestral</caption>
	<thead>
	<tr>
		<th>Matérias</th>
		<th>Prova</th>
		<th>Trabalho</th>
		<th>Média</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>&nbsp;Matemática</td>
		<td>&nbsp;10</td>
		<td>&nbsp;9</td>
		<td>&nbsp;9.5</td>
	</tr>
	<tr>
		<td>&nbsp;Português</td>
		<td>&nbsp;5</td>
		<td>&nbsp;6</td>
		<td>&nbsp;6.5</td>
	</tr>
	<tr>
		<td>&nbsp;Inglês</td>
		<td>&nbsp;4</td>
		<td>&nbsp;7</td>
		<td>&nbsp;6</td>
	</tr>
	<tr>
		<td>&nbsp;Ciências</td>
		<td>&nbsp;5</td>
		<td>&nbsp;5</td>
		<td>&nbsp;5</td>
	</tr>
	</tbody>
</table>