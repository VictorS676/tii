<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

        public function index()
        {
                $this->load->view('template/header');

                $this->load->helper(array('form', 'url'));
                $this->load->library('form_validation');

                $this->load->model('UsuarioModel', 'model');
                $this->model->salva_usuario();
                $this->load->view('form/myform');

                $this->load->view('template/footer');
                $this->load->view('template/scripts');
        }

        public function sucesso()
        {
                $this->load->view('template/header');

                $this->load->view('form/formsuccess');

                $this->load->view('template/footer');
                $this->load->view('template/scripts');     
        }

}