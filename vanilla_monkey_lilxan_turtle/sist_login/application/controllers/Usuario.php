<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller 
{
	
	public function index()
	{
		$this->template->show('home');
	}
	
	public function editar($id)
	{
        
        $this->load->view('template/header');
		$this->load->view('form/logout');
        $this->load->model('UsuarioModel', 'model');
        $data['user'] = $this->model->read($id);
		
        $this->load->view('form/form_update', $data);
		
        $this->model->edita_usuario($id);
        
        $this->load->view('template/footer');
        $this->load->view('template/scripts');
    }
	
}

?>