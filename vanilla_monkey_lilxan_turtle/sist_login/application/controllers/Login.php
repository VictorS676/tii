<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{

        public function autentica()
        {
                $this->load->view('template/header');

				$this->load->view('form/loginform');
                $this->load->model('UsuarioModel', 'model');
				
				$email = $this->input->post("email");
				$senha = md5($this->input->post("senha"));
	
				$usuario = $this->model->logar($email, $senha);
				
				if($usuario){
					$this->session->set_userdata("usuario_logado", $usuario);
					$this->session->set_flashdata("success", "Logado com sucesso");
					redirect('login/perfil');
					
				}else{
					$this->session->set_flashdata("danger", "Usuário e/ou senha inválidos!");
				}	
				
                $this->load->view('template/footer');
                $this->load->view('template/scripts');
        }
		
		public function logout()
		{
			$this->session->unset_userdata("usuario_logado");
			// $this->session->set_flashdata("Deslogado com Sucesso!");
			redirect("http://localhost/sist_login/");
		}
		
		public function perfil()
		{
			$this->load->view('template/header');

            $this->load->view('user/perfil');
			
            $this->load->view('template/footer');
            $this->load->view('template/scripts');
		}

}