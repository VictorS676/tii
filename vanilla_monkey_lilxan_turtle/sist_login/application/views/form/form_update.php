<div class="container mt-5 pt-4">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <form class="text-center p-5" method="POST">
                <p class="h4 mb-4">Edite seu Perfil</p>
                <input value="<?= isset($user) ? $user['nome'] : '' ?>" type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">
                <input value="<?= isset($user) ? $user['email'] : '' ?>" type="email" id="email" name="email" class="form-control mb-4" placeholder="E-Mail">
                <input value="<?= isset($user) ? $user['senha'] : '' ?>" type="password" id="senha" name="senha" class="form-control mb-4" placeholder="Senha">
                <button class="btn btn-info btn-block my-4" type="submit">Salvar</button>
            </form>
        </div>
    </div>    
</div> 