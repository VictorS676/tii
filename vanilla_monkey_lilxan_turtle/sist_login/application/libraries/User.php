<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class User extends CI_Object 
{

    public function lista()
    {
        $rs = $this->db->get_where('usuario');
        echo $this->db->last_query();
        $result = $rs->result_array();
        return $result;
    }
    
    public function cria_usuario($data)
    {
        $this->db->insert('usuario', $data);
    }

	public function login()
	{
		
	}	
    
    public function edita_usuario($data, $id)
    {
        $this->db->update('usuario', $data, "id = $id");				                             
    }
    
    public function user_data($id)
    {
        $cond = array('id' => $id);
        $rs = $this->db->get_where('usuario', $cond);
        return $rs->row_array();
    }
    
    public function delete($id)
    {
        $cond = array('id' => $id);
        $this->db->delete('usuario', $cond); 
    }
    
}