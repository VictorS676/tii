<?php

defined('BASEPATH') OR exit('Nenhum tipo de acesso via script é permitido.');

class Login extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('LogModel');
  }

  function index(){
    $this->load->view('common/header');
    $this->load->view('util/navbar');
    $this->load->view('login_view');
    $this->load->view('util/footer');
  }

  function auth(){
    $email    = $this->input->post('email',TRUE);
    $password = $this->input->post('password',TRUE);
    $validate = $this->LogModel->validate($email,$password);
    if($validate->num_rows() > 0){
        $data  = $validate->row_array();
        $id  = $data['user_id'];
        $name  = $data['user_name'];
        $email = $data['user_email'];
        $level = $data['user_level'];
        $sesdata = array(
            'id'  => $id,
            'username'  => $name,
            'email'     => $email,
            'level'     => $level,
            'logged_in' => TRUE
        );
        $this->session->set_userdata($sesdata);
        // access login for admin
        if($level === '1'){
            redirect('Adm');

        // access login for staff
        }elseif($level === '2'){
          redirect('home');
        }


    }
    
    else{
      redirect('login');
    }
  }


  public function cadastrar(){
     

      $this->load->view('util/navbar');
      $this->load->view('common/header');
      $this->load->model('ProdutoModel', 'model');
  

    $data['user_name'] = $this->input->post('user_name');
    $data['user_email'] = $this->input->post('user_email');
    $data['user_password'] = $this->input->post('user_password');
    $data['user_level'] = $this->input->post('user_level');
    $data['user_tel'] = $this->input->post('user_tel');
     
    
  $this->load->view('formulariocadastrar.php');
  
      if ($this->model->insere_u($data)){
        redirect('login');
      }
   
  }


  function logout(){
      $this->session->sess_destroy();
      redirect('login');
  }

}
