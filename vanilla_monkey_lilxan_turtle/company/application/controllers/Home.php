<?php

defined('BASEPATH') OR exit('Nenhum tipo de acesso via script é permitido.');

class Home extends CI_Controller {

    public function index(){
        $html = $this->load->view('util/navbar', null, true);
        $html .= $this->load->view('content/banner', null, true);
        $html .= $this->load->view('content/about', null, true);
        $html .= $this->load->view('content/contact', null, true);
        $html .= $this->load->view('util/footer', null, true);
        $this->show($html);
    }

    public function sobre(){
        $html = $this->load->view('util/preloader', null, true);
        $html .= $this->load->view('common/navbar', null, true);
        $html .= $this->load->view('content/about', null, true);
        $html .= $this->load->view('common/footer', null, true);
        $this->show($html);
    }

    public function contato(){
        $html = $this->load->view('util/preloader', null, true);
        $html .= $this->load->view('common/navbar', null, true);
        $html .= $this->load->view('content/contact', null, true);
        $html .= $this->load->view('common/footer', null, true);
        $this->show($html);
    }

 function show($html){
        $aux = $this->load->view('common/header', null, true);
        $aux .= $html;
        $aux .= $this->load->view('common/footer', null, true); 
        echo $aux;
    }

    public function cadastrar_mensa(){
    
        $data['nome'] = $this->input->post('nome');
        $data['email'] = $this->input->post('email');
        $data['motivo'] = $this->input->post('motivo');
        $data['telefone'] = $this->input->post('telefone');
        $data['mensagem'] = $this->input->post('mensagem');
        $this->load->view('common/header');
    
        if ($this->db->insert('mensagens_home', $data)){
      echo '  
    <div class="container text-center mt-5 pt-5">
    <h3 class="text-danger">Agradecemos sua mensagem ! <strong class="text-danger">
    
    <h3><a href="../">Voltar</a></h3>
    <br>
      ';
       header("refresh: 3;../");
    
        }
    }

    public function mensagens(){
        $this->load->view('util/navbar');
        $this->load->view('common/header');
        $this->load->model('MensageModel', 'model');
        $data['table_mensagens'] = $this->model->carrega_tabela();
        $this->load->view('common/table_mensage.php', $data);
      
        }  
      
public function deleta_m($id){
    $this->load->model('MensageModel', 'model');
    $this->model->deleta_me($id);
    redirect('home/mensagens');
  
  }
  
public function deleta_produto($id){
  
    $this->load->model('ProdutoModel', 'model');
    $this->model->deleta_prod($id);
    redirect('/adm/listaprodu');
  
  }   
  
public function deleta_car($id){
  
    $this->load->model('ProdutoModel', 'model');
    $this->model->deleta_car($id);
    redirect('/brands/listacar');
  
  }   
}    