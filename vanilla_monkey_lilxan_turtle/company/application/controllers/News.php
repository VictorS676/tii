<?php

defined('BASEPATH') OR exit('Nenhum tipo de acesso via script é permitido.');

class News extends CI_Controller {

    public function index(){
        $html = $this->load->view('util/navbar', null, true);
        $html .= $this->load->view('content/news_header', null, true);
        $html .= $this->load->view('content/card', null, true);
        $html .= $this->load->view('content/contact', null, true);
        $html .= $this->load->view('util/footer', null, true);
        $this->show($html);
    }

    function show($html){
        $aux = $this->load->view('common/header', null, true);
        $aux .= $html;
        $aux .= $this->load->view('common/footer', null, true); 
        echo $aux;
    }

}