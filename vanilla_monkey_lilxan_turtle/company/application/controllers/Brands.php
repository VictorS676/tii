<?php

defined('BASEPATH') OR exit('Nenhum tipo de acesso via script é permitido.');

class Brands extends CI_Controller {

    public function index(){
        $this->load->view('util/navbar');
        $this->load->view('common/header');
        $this->load->model('ProdutoModel', 'model');
        $data['ver_produtos'] = $this->model->gerapr();
        $this->load->view('common/view_produtos.php', $data); 
        $this->load->view('util/footer');



    }

    function show($html){
        $aux = $this->load->view('common/header', null, true);
        $aux .= $html;
        $aux .= $this->load->view('common/footer', null, true); 
        echo $aux;
    }

    
public function manda_prod(){
     
    $this->load->helper('file');

    $this->load->model('PaisagemModel', 'paisagem');


      $this->load->view('util/navbar');
      $this->load->view('common/header');
      $this->load->model('ProdutoModel', 'model');
  
   $data['imagens'] = $this->paisagem->salva();


    $cas['nome'] = $this->input->post('nome');
    $cas['preco'] = $this->input->post('preco');
    $cas['saber'] = $this->input->post('saber');
    $cas['imagem'] = $this->input->post('titulo_img');
     
    
  $this->load->view('formularioproduto.php');
  
      if ($this->model->insere_p($cas, $data)){
        redirect('/adm');
      }
   
  }
    
public function manda_procar(){


      $this->load->view('util/navbar');
      $this->load->view('common/header');
      $this->load->model('ProdutoModel', 'model');
  

    $cas['idcara'] = $this->input->post('idcara');
    $cas['nomecara'] = $this->input->post('nomecara');
    $cas['emailcara'] = $this->input->post('emailcara');
    $cas['nome'] = $this->input->post('nome');
    $cas['preco'] = $this->input->post('preco');
     
    
  $this->load->view('view_produtos.php');
  
      if ($this->model->insere_car($cas, $data)){
        redirect('/adm');
      }
   
  }

  public function listacar(){
  
    $this->load->view('common/header');
    $this->load->view('util/navbar');
    $this->load->model('ProdutoModel', 'model');
    $data['tabela_car'] = $this->model->geracar();
    $this->load->view('common/tabelacar.php', $data);
  
    }
  
public function ver_produtin($id){
  
  $this->load->view('util/navbar');
  $this->load->view('common/header');

$this->load->model('ProdutoModel', 'model');

  $cas['idcara'] = $this->input->post('idcara');
    $cas['nomecara'] = $this->input->post('nomecara');
    $cas['emailcara'] = $this->input->post('emailcara');
    $cas['nome'] = $this->input->post('nome');
    $cas['preco'] = $this->input->post('preco');
 
  
  $data['user'] = $this->model->read($id);
  $this->load->view('view_produtos', $data);
  //$this->model->edita_post($id);

  $this->load->view('common/footer');

  if ($this->model->insere_car($cas, $data)){
    redirect('/adm');
  }
}

  
}