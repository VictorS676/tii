<section id="contact" class="contact-area pt-5 pb-5 gray-bg">
    <div class="mt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-20">
                        <h5 class="sub-title mb-15">Novo</h5>
                        <h2 class="title">Cadastrar novo produto</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="contact-form">
                        <form method="POST" id="contact-form" action="assets/contact.php" data-toggle="validator">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="single-form form-group">
                                        <input type="text" name="produto[tipo]" placeholder="Tipo" data-error="Preencha este campo." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group pt-5 mx-auto">
                                        <label>Pequeno</label>
                                        <input type="radio" name="size" value="Pequeno">
                                        
                                        <label>Médio</label>
                                        <input type="radio" name="size" value="Médio">
                                        
                                        <label>Grande</label>
                                        <input type="radio" name="size" value="Grande">
                                        
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="single-form form-group">
                                        <input type="text" name="produto[cor]" placeholder="Cor" data-error="Insira o motivo da sua mensagem." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="single-form form-group">
                                        <input type="number" name="produto[preco]" placeholder="Preço" data-error="Insira o preço." required>
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                </div>
            
                                <div class="col-md-12">
                                    <div class="single-form form-group text-center">
                                        <button type="submit" class="main-btn mb-3">Enviar</button>
                                    </div> 
                                </div>
                            </div> 
                        </form>
                    </div> 
                </div>
            </div> 
        </div> 
    </div> 
</section>