<section id="about" class="about-area pt-5 mt-4 pb-130">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about-image mt-50 clearfix">
                    <div class="single-image float-left">
                        <img src="<?= base_url('assets/img/000001.jfif'); ?>" alt="About">
                    </div> 
                    <div data-aos="fade-right" class="about-btn">
                        <a class="main-btn" href="#"><span>15</span> Anos de Experiência</a>
                    </div>
                    <div class="single-image image-tow float-right">
                        <img src="<?= base_url('assets/img/000002.jfif'); ?>" alt="About">
                    </div> 
                </div> 
            </div>
            <div class="col-lg-6">
                <div class="about-content mt-45">
                    <h4 class="about-welcome">Sobre Nós</h4>
                    <h3 class="about-title mt-10 pb-3">15 anos de estilo!</h3>
                    <p class="mt-25">Um pouco da história da empresa 
                        <br/> <br/>e da cliente.</p>
                </div> 
            </div>
        </div> 
    </div> 
</section>
