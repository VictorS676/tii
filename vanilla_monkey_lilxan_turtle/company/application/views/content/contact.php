<section id="contact" class="contact-area pt-5 pb-5 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center pb-20">
                    <h5 class="sub-title mb-15">Fale Conosco</h5>
                    <h2 class="title">Mande suas mensagens</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="contact-form">
                    <form id="contact-form" action="<?= base_url('index.php/home/cadastrar_mensa') ; ?>" method="POST" data-toggle="validator">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="single-form form-group">
                                    <input type="text" name="nome" placeholder="Nome" data-error="Preencha este campo." required="required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single-form form-group">
                                    <input type="email" name="email" placeholder="Email" data-error="Insira um e-mail válido." required="required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single-form form-group">
                                    <input type="text" name="motivo" placeholder="Motivo da Mensagem" data-error="Insira o motivo da sua mensagem." required="required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single-form form-group">
                                    <input type="text" name="telefone" placeholder="Telefone" data-error="Insira seu telefone." required="required">
                                    <div class="help-block with-errors"></div>
                                </div> 
                            </div>
                            <div class="col-md-12">
                                <div class="single-form form-group">
                                    <textarea placeholder="Sua Mensagem" name="mensagem" data-error="Escreva sua mensagem." required="required"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <p class="form-message"></p>
                            <div class="col-md-12">
                                <div class="single-form form-group text-center">
                                    <button type="submit" class="main-btn mb-3">Enviar</button>
                                </div> 
                            </div>
                        </div> 
                    </form>
                </div> 
            </div>
        </div> 
    </div> 
</section>