 <header id="home" class="header-area">
    <div class="shape header-shape-one">
        <img src="<?= base_url('assets/mdb/images/banner/shape/shape-1.png'); ?>" alt="shape">
    </div>
    <div class="shape header-shape-tow animation-one">
        <img src="<?= base_url('assets/mdb/images/banner/shape/shape-2.png'); ?>" alt="shape">
    </div> 
    <div class="shape header-shape-three animation-one">
        <img src="<?= base_url('assets/mdb/images/banner/shape/shape-3.png'); ?>" alt="shape">
    </div>
    <div class="shape header-shape-fore">
        <img src="<?= base_url('assets/mdb/images/banner/shape/shape-4.png'); ?>" alt="shape">
    </div> 
    <div class="navigation-bar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" href="#">
                            <!-- <img src="<?= base_url('assets/mdb/images/logo.png'); ?>" alt="Logo"> -->
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul id="nav" class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="page-scroll" href="<?= base_url();?>">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="page-scroll" href="#about">Sobre</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= base_url('index.php/brands'); ?>">Catálogo</a>
                                </li>
                                <li class="nav-item">
                                    <a class="page-scroll" href="<?= base_url('#contact"'); ?>>Contato</a>
                                </li>
                                <li class="nav-item">
                                    <a class="page-scroll" href="<?= base_url('index.php/login');?>"><i class="fas fa-door-closed"></i></a>
                                </li>
                                 <?php if($this->session->userdata('level')==('1')){  ?>
                                <li class="nav-item">
                                    <a class="page-scroll" href="<?= base_url('index.php/adm');?>"><i class="fas fa-lock"></i></a>
                                </li>
                                 <?php } ?>
                                <li class="nav-item">
                                    <a class="page-scroll" href="<?= base_url('index.php/brands/listacar');?>"><i class="fas fa-shopping-cart"></i></a>
                                </li>
                                
  <?php if($this->session->userdata('level')==('2')||$this->session->userdata('level')==('1')){  ?>
                               
                        <li class="nav-item">
                                    <a class="page-scroll" href="<?= base_url('index.php/login/logout'); ?>">Sair</a>
                                </li>
  <?php } ?>
                            </ul>
                        </div>
                        <div class="navbar-btn ml-20 d-none d-sm-block">
                            <a class="main-btn" href="https://chat.whatsapp.com/BcD7JQd8SUY9glVLLFBELw"><i class="lni-phone"></i> Grupo do WhatsApp</a>
                        </div>
                    </nav> 
                </div>
            </div> 
        </div> 
    </div>
</header>