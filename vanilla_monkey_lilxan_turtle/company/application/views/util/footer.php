<footer id="footer" class="footer-area">
    <div class="footer-widget pt-80 pb-130">
        <div class="container pt-5">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-8">
                    <div class="footer-logo mt-50">
                        <a href="#">
                            <img src="<?= base_url('assets/img/logo.jfif'); ?>" alt="Logo">
                        </a>
                        <ul class="footer-info">
                            <li>
                                <div class="single-info">
                                    <div class="info-icon">
                                        <i class="lni-phone-handset"></i>
                                    </div>
                                    <div class="info-content">
                                        <p>+55 (11) 99423-4567</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="single-info">
                                    <div class="info-icon">
                                        <i class="lni-envelope"></i>
                                    </div>
                                    <div class="info-content">
                                        <p>contatinho@vips.suporte.com.br</p>
                                    </div>
                                </div> 
                            </li>
                        </ul>
                        <ul class="footer-social text-center mt-2">
                            <li><a href="#"><i class="fab fa-facebook-f mt-3"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram mt-3"></i></a></li>                        </ul>
                    </div> 
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="footer-link mt-2">
                        <div class="f-title">
                            <h4 class="title">Menu</h4>
                        </div>
                        <ul class="mt-4">
                            <li><a href="<?= base_url('#about') ?>">Sobre</a></li>
                            <li><a href="<?= base_url('index.php/brands') ?>">Catálogo</a></li>
                            <li><a href="<?= base_url('#contact') ?>">Contato</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="footer-link mt-2">
                        <div class="f-title">
                            <h4 class="title">Produtos</h4>
                        </div>
                        <ul class="mt-4">
                            <li>Bolsas</a></li>
                            <li>Sapatos</a></li>
                            <li>Acessórios</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 col-sm-8">
                    <div class="footer-newsleter mt-2">
                        <div class="f-title">
                            <h4 class="title">Company Bag</h4>
                        </div>
                        <p class="mt-4">Slogan da empresa.</p>
                    </div>
                </div>
            </div>
        </div> 
    </div> 
    <div class="copyright-area mt-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright text-center  pb-1">
                        <p>Sistema por VIPS</p>
                    </div>
                </div>
            </div> 
        </div> 
    </div> 
</footer>