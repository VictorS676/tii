  <script src="<?= base_url('assets/mdb/js/vendor/modernizr-3.6.0.min.js'); ?>"></script>
  <script src="<?= base_url('assets/mdb/js/vendor/jquery-1.12.4.min.js'); ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/mdb/js/jquery-3.4.1.min.js'); ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/mdb/js/popper.min.js'); ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/mdb/js/bootstrap.min.js'); ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/mdb/js/mdb.min.js'); ?>"></script>
  <script src="<?= base_url('assets/mdb/js/wow.min.js'); ?>"></script>
  <script src="<?= base_url('assets/mdb/js/slick.min.js'); ?>"></script>
  <script src="<?= base_url('assets/mdb/js/scrolling-nav.js'); ?>"></script>
  <script src="<?= base_url('assets/mdb/js/jquery.easing.min.js'); ?>"></script>
  <script src="<?= base_url('assets/mdb/js/aos.js'); ?>"></script>
  <script src="<?= base_url('assets/mdb/js/main.js'); ?>"></script>
</body>
</html>