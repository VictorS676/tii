<?php
include_once APPPATH.'libraries/MensageLib.php';
class MensageModel extends CI_Model{
    
    public function deleta_me($id){
        $mensagens= new MensageLib();
        $mensagens->deleta($id);
    }
     public function carrega_tabela(){
        $html ='';

        $mensagens = new MensageLib();
        $data = $mensagens->lista();

        foreach ($data as $assunto) {
            $html .= '<tr>';
            $html .= '<td>'.$assunto['id'].'</td>';
            $html .= '<td><p href="'.base_url('index.php/cliente/detalhe/'.$assunto['id']).'">'.$assunto['nome'].'</p></td>';
            $html .= '<td>'.$assunto['email'].'</td>';
            $html .= '<td>'.$assunto['telefone'].'</td>';
            $html .= '<td>'.$assunto['motivo'].'</td>';
            $html .= '<td>'.$assunto['mensagem'].'</td>';
            $html .= '<td>'.$assunto['enviado'].'</td>';
            $html .= '<td>'.$this->action_buttons($assunto['id']).'</td>';
            $html .= '</tr>';   
        }
        return $html;
    }

    private function action_buttons($id){
        $html = '<a href="'.base_url('index.php/home/deleta_m/'.$id).'">';
        $html .= '<i class="far fa-trash-alt text-danger"></i></a>';
        return $html;
    }


}