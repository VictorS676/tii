<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class MensageLib extends CI_Object {
    

    public function lista(){
        $rs = $this->db->get_where('mensagens_home');
        $result = $rs->result_array();
        return $result;
    }
    
    public function deleta($id){
        $cond = array('id' => $id);
        return $this->db->delete('mensagens_home', $cond); 
    }
    
}