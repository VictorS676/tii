<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Campus Chico Lopes</title>
			<link rel="stylesheet" type="text/css" href="inde.css"/>
			<link rel="shortcut icon" type="image/x-icon" href="imagens/favicon.png">
				<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
				<script type="text/javascript">
$(document).ready(function(){ <!-- JQuery -->
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
				</script>
</head>
<body>
	<div class="macaxera"></div>
		<div id='cssmenu'>
			<ul>
				<li class='active'><a href='index.php#'>área do aluno</a></li>
				<li><a href='professor.php#'>área do professor</a></li>
				<li><a href='instituto.php#'>Instituto</a></li>
				<li><a href='#'>Contato</a></li>
			</ul>
		</div><center></br></br></br></br></br>
    <section class="hero is-success is-fullheight">   
    <div class="banana">
	</br>                   
	<div id="titulo" > <h3>ÁREA DO ALUNO</h3>  </div>
       <?php if(isset($_SESSION['nao_autenticado'])):  ?>                   
	<div class="notification is-danger">
		<script type="text/javascript">
			alert('ERRO: Usuário ou senha inválidos.');
		</script>
	</div>          
	<?php
         endif;
         unset($_SESSION['nao_autenticado']);
    ?>                 
    <div class="girafffas">
        <select>
            <option>Escolha uma opção:</option>
            <option value="login">Login</option>
            <option value="cadastro">Cadastro</option>
        </select>
    </div> 
	<div class="login box"><br/><br/>
		<form action="login.php" method="POST">                         												  	   
			<div class="pera">
               <label for="name"> Usuário: </label></br></br>   
			   <label for="name" class="fome"> Senha: </label>
			</div>
			<div class="tomate">
			   	<input id name="usuario" type="text" class="txt bradius" placeholder="Seu usuário." autofocus="" /></br></br>
			  	<input name="senha" class="txt bradius" type="password" placeholder="Sua senha." autofocus=""/>
			</div></br></br>
			<div class="vem">	
                <input type="submit" class="sb" value="Entrar"/></br></br>
			</div>	
		</form>
	</div>
		<div class="cadastro box"><br/><br/>
			<form method="post" action="inserir.php">
				<div class="pera">
					<label> Usuário: </label></br></br>   
					<label class="fome"> Senha: </label>
				</div>
				<div class="tomate">
					<input id="usuario" name="usuario" type="text" class="txt bradius" placeholder="Escolha seu nome de usuário" autofocus="" /></br></br>
					<input name="senha" class="txt bradius" type="password" placeholder="Escolha sua senha" autofocus=""/>
				</div></br></br>
				<div class="vem">	
					<input type="submit" class="sb" value="Cadastrar"/></br></br>
				</div>	
			</form>
		</div>											
	</div>
	</section></center></br></br>
	
	<div class="social">
		<div class="section">
			<div class="rede" id="facebook">
				<img class="icone" src="imagens/facebook.png" />
			</div>
			<div class="rede" id="twitter">
				<img class="icone" src="imagens/twitter.png" />
			</div>
			<div class="rede" id="instagram">
				<img class="icone" src="imagens/instagram.png" />
			</div>
			<div class="rede" id="youtube">
				<img class="icone" src="imagens/youtube.png" />
			</div>
		</div>
	</div>  
</body>
</html>