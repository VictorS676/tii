/*
SQLyog Community v12.3.2 (64 bit)
MySQL - 5.0.91-community : Database - syslogin
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`syslogin` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `syslogin`;

/*Table structure for table `usuarionivel` */

DROP TABLE IF EXISTS `usuarionivel`;

CREATE TABLE `usuarionivel` (
  `NvlID` smallint(4) NOT NULL auto_increment,
  `NvlNome` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`NvlID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `UsrID` smallint(4) NOT NULL auto_increment,
  `UsrNome` text collate utf8_unicode_ci NOT NULL,
  `UsrSenha` text collate utf8_unicode_ci NOT NULL,
  `UsrEmail` text collate utf8_unicode_ci NOT NULL,
  `UsrNvl` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`UsrID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
