<?php
/*
Esse arquivo é especifico para teste do Sistema de Login em PHP. Qualquer edição ou alteração os créditos devem permanecer.
->	Author: Eduardo Palandrani
->	Date: 2017-10-31 20:34:15
*/
session_start();

if(isset($_SESSION['UserLogado'])){
	include('config/login.php');
}else{
	include('login.html');
}