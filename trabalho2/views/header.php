<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Um website feito para contar um pouco sobre mim, o autor, e minha família, além de meus sonhos e objetivos.">
        <meta name="author" content="Victor Sousa">
        <meta name="generator" content="Visual Studio Code">
        <meta name="keywords" content="eu, autor, perfil, história, design"> 
            <title>Site Pessoal Victor</title>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
                <link rel="stylesheet" href="<?= BASEURL ?>assets/mdb/css/estilo.css">
                <link href="<?= BASEURL ?>assets/mdb/css/bootstrap.min.css" rel="stylesheet">
                <link href="<?= BASEURL ?>assets/img/favicon.png" type="image/x-icon" rel="shortcut icon">
				<link href="<?= BASEURL ?>assets/mdb/css/mdb.min.css" rel="stylesheet">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
                    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
                    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    </head>
<body>