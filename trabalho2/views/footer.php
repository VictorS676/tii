    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?= BASEURL ?>assets/mdb/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?= BASEURL ?>assets/mdb/js/dark.js"></script>
	  <script type="text/javascript" src="<?= BASEURL ?>assets/mdb/js/popper.min.js"></script>
	  <script type="text/javascript" src="<?= BASEURL ?>assets/mdb/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= BASEURL ?>assets/mdb/js/mdb.js"></script>
    <script type="text/javascript" src="<?= BASEURL ?>assets/mdb/js/sidebar.js"></script>
    <script type="text/javascript" src="<?= BASEURL ?>assets/mdb/js/galeria.js"></script>
</body>
</html>