<?php 

include 'config/constants.php';
include APPPATH.'views/header.php';
include APPPATH.'views/menu.php';

include APPPATH.'views/conteudo.php';
include APPPATH.'views/habilidades.php';
include APPPATH.'views/caracteristicas.php';

include APPPATH.'libraries/util/DB.php';
include APPPATH.'component/models/component_model.php';
$components = get_imagens();
include APPPATH.'views/galeria.php';

include APPPATH.'views/rodape.php';
include APPPATH.'views/footer.php';

?>