<?php
###############################################
#	Desenvolvido por: Altamiro D. Rodrigues   #
#		E-mail: altamiro27@gmail.com		  #
#	(75) 3622 - 4145 // (75) 8816 - 9835	  #
#	MSN: altamiro@nitrofox.com.br			  #
#		http://www.trilhadesign.com.br		  #
###############################################	

###############################################
#    Modificado em 15/05/2016 por REGISRMP    #
###############################################

?>
<?php

define('HOST', 'localhost');
define('USER', 'root');
define('PASS', '');
define('DBNAME', 'agenda');

$conn = new PDO('mysql:host=' . HOST . ';dbname=' . DBNAME . ';', USER, PASS);

/**
* Formata data
*/
function to_date($data)
{
    return date("d/m/Y H:i:s", strtotime($data));
}

?>