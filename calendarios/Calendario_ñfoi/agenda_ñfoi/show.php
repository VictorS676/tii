<?php
###############################################
#	Implementado por: Ibrahim Brumate         #
#		E-mail: periscuelo@hotmail.com		  #
#	Desenvolvido por: Altamiro D. Rodrigues   #
#		E-mail: altamiro27@gmail.com		  #
#	(75) 3622 - 4145 // (75) 8816 - 9835	  #
#	MSN: altamiro@nitrofox.com.br			  #
#		http://www.trilhadesign.com.br		  #
###############################################

###############################################
#    Modificado em 15/05/2016 por REGISRMP    #
###############################################
?>
<?php
include "config.php";

if (isset($_GET["id"])) {
	$id = $_GET["id"];
}
else {
	$id = "";	
}	

?>
<html>
<head>
<title>Detalhes do Compromisso</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="template.css" rel="stylesheet" type="text/css">

</head>

<body>
<table style="width: 90%; height: 100%" border="0" align="center" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF" class="texto">
  
  <tr>
    <td class="text" valign="top">
<br>
<table width="98%" align="center" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="text">
	<fieldset><legend>AGENDA DE COMPROMISSOS</legend><br>
<?php

$sql = mysql_query("select c.*, u.nome from calendario c, usuarios u where u.id = c.codusuario and c.id = $id");
while ($dados = mysql_fetch_array($sql)){ 

	$month = $dados["mes"];
	$catgoria = $dados["categoria"];

switch ($month) {
    case 01:  $month_name = "Janeiro";	break;
    case 02:  $month_name = "Fevereiro";	break;
    case 03:  $month_name = "Março";	break;
    case 04:  $month_name = "Abril";	break;
    case 05:  $month_name = "Maio";	break;
    case 06:  $month_name = "Junho";	break;
    case 07:  $month_name = "Julho";	break;
    case 08:  $month_name = "Agosto";	break;
    case 09:  $month_name = "Setembro";	break;
    case 10: $month_name = "Outubro";	break;
    case 11: $month_name = "Novembro";	break;
    case 12: $month_name = "Dezembro";	break;
   }

switch ($catgoria) {
    case 1:  $cat_name = "<font color=#666666><strong>Nenhuma</strong></font>";	break;
    case 2:  $cat_name = "<font color=#CC3300><strong>Importante</strong></font>";	break;
    case 3:  $cat_name = "<font color=#6666CC><strong>Neg�cios</strong></font>";	break;
    case 4:  $cat_name = "<font color=#669900><strong>Pessoal</strong></font>";	break;
    case 5:  $cat_name = "<font color=#999900><strong>Folga</strong></font>";	break;
    case 6:  $cat_name = "<font color=#FF9900><strong>Deve ser atendido</strong></font>";	break;
    case 7:  $cat_name = "<font color=#FF00FF><strong>Anivers�rio</strong></font>";	break;
    case 8:  $cat_name = "<font color=#FF3300><strong>Liga��o Telef�nica</strong></font>";	break;
   }
?>				
	  <center><strong><font color="orange">Compromisso alterado com sucesso.</font></strong><br><br></center>
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="text">
		<tr>
		  <td width="92"><div align="right"><strong>Data:</strong></div></td>
		  <td><?php echo $dados["dia"]; ?> de <?php echo $month_name; ?> de <?php echo $dados["ano"]; ?></td>
		</tr>
		<tr>
		  <td width="110"><div align="right"><strong>Data de inclus&atilde;o:</strong></div></td>
		  <td><?php if (isset($dados["dh_inclusao"])) { echo to_date($dados["dh_inclusao"]); } ?></td>
		</tr>	
		<tr>
		  <td width="110"><div align="right"><strong>Ultima altera&ccedil;&atilde;o:</strong></div></td>
		  <td><?php if (isset($dados["dh_ult_alteracao"])) { echo to_date($dados["dh_ult_alteracao"]); } ?></td>
		</tr>			
		<tr>
		  <td><div align="right"><strong>Assunto:</strong></div></td>
		  <td><?php echo $dados["assunto"]; ?></td>
		</tr>
		<tr>
		  <td><div align="right"><strong>Categoria:</strong></div></td>
		  <td><?php echo $cat_name; ?></td>
		  </tr>
		<tr>
		  <td><div align="right"><strong>Local:</strong></div></td>
		  <td><?php echo $dados["local"]; ?></td>
		</tr>
		<tr>
		  <td valign="top"><div align="right"><strong>Descri&ccedil;&atilde;o:</strong></div></td>
		  <td valign="top"><?php echo $dados["descricao"]; ?></td>
		</tr>
		<tr>
		  <td valign="top"><div align="right"><strong>Usuario:</strong></div></td>
		  <td valign="top"><?php echo $dados["nome"]; ?></td>
		</tr>		
		<tr>
		  <td>&nbsp</td>
		  <td>
		  <table cellpadding="0" cellspacing="0" class="texto">
			  <td width="20"><div align="center"><img src="ico-edit.gif" width="16" height="16" border="0"></div></td>
			  <td width="40"><a href="alterarcompromisso.php?id=<?php echo $dados["id"]; ?>"><font color="#666666"><strong>Editar</strong></font></a></td>
			  <td width="21"><div align="center"><img src="ico-lixo.gif" width="16" height="16" border="0"></div></td>
			  <td width="48"><a href="vercompromissos.php?id=<?php echo $dados['id']; ?>&acao=excluir<?php echo "&dia=".$dados["dia"]."&mes=".$dados["mes"]."&ano=".$dados["ano"]?>"><font color="#666666"><strong>Apagar</strong></font></a></td>
		  </table>
		  </td>
		</tr>
	</table>	  
<?php
}
?>	
<br>  
</fieldset>		</td>
  </tr>
</table>    </td>
  </tr>
</table>
</body>
</html>
