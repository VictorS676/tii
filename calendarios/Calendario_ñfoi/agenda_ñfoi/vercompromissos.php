<?php
###############################################
#	Implementado por: Ibrahim Brumate         #
#		E-mail: periscuelo@hotmail.com		  #
#	Desenvolvido por: Altamiro D. Rodrigues   #
#		E-mail: altamiro27@gmail.com		  #
#	(75) 3622 - 4145 // (75) 8816 - 9835	  #
#	MSN: altamiro@nitrofox.com.br			  #
#		http://www.trilhadesign.com.br		  #
###############################################

###############################################
#    Modificado em 15/05/2016 por REGISRMP    #
###############################################
?>
<?php
include "config.php";

if (isset($_GET["acao"])) {
	$acao = $_GET["acao"];
}
else {
	$acao = "";	
}	

if ($acao=="excluir") {
		$id = $_GET["id"];
		$dia = $_GET["dia"];
		$mes = $_GET["mes"];
		$ano = $_GET["ano"];

		$sql = mysql_query("delete from calendario where id = '$id'");

		echo "<script type=\"text/javascript\">alert(\"Compromisso excluido com sucesso.\");</script>";
		echo "<script language='javascript'>parent.frames[1].location.reload()</script>";
}

?>
<html>
<head>
<title>Agendar Compromisso</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="template.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function valida_data() {
	var Form;
    Form = document.cadastro;
    var lDay = parseInt(Form.dia.value), 
		lMon = parseInt(Form.mes.value), 
		lYear = parseInt(Form.ano.value),
        BiY = (lYear % 4 == 0 && lYear % 100 != 0) || lYear % 400 == 0,
        MT = [1, BiY ? -1 : -2, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1];
    return lMon <= 12 && lMon > 0 && lDay <= MT[lMon - 1] + 30 && lDay > 0;
}


function valida_form() {
    var Form;
    Form = document.cadastro;
    if (Form.dia.value.length == 0) {
        alert("Selecione o Dia!");
        Form.dia.focus();
        return false;
    }
    if (Form.mes.value.length == 0) {
        alert("Selecione o M�s!");
        Form.mes.focus();
        return false;
    }
    if (Form.ano.value.length == 0) {
        alert("Selecione o Ano!");
        Form.ano.focus();
        return false;
    }
	if (valida_data() == false) {
		alert("Data inv�lida!");
        Form.dia.focus();
        return false;
	}	 
    if (Form.assunto.value.length == 0) {
        alert("Campo Assunto n�o pode ser vazio!");
        Form.assunto.focus();
        return false;
    }
    if (Form.categoria.value.length == 0) {
        alert("Selecione uma Categoria!");
        Form.categoria.focus();
        return false;
    }
    if (Form.local.value.length == 0) {
        alert("Campo Local n�o pode ser vazio!");
        Form.local.focus();
        return false;
    }
    if (Form.descricao.value.length == 0) {
        alert("Campo Descri��o n�o pode ser vazio!");
        Form.descricao.focus();
        return false;
    }

    return true;
}
</script>
</head>

<body>
<table style="width: 90%; height: 100%" border="0" align="center" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF" class="texto">
  
  <tr>
    <td class="text" valign="top">
<br>
<table width="98%" align="center" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="text">
	<fieldset><legend>AGENDA DE COMPROMISSOS</legend><br>
<?php
$codusuario = 1;
if (isset($_GET["dia"])) {
	$dia = $_GET["dia"];
}
else {
	$dia = date("d");
}
	
if (isset($_GET["mes"])) {
	$mes = $_GET["mes"];
}
else {
	$mes = date("m");
}

if (isset($_GET["ano"])) {
	$ano = $_GET["ano"];
}
else {
	$ano = date("Y");
}

echo "<center><strong><font color=\"orange\">Mostrando compromissos de $dia/$mes/$ano</font></strong><br><br></center>";
$sql = mysql_query("select c.*, u.nome from calendario c, usuarios u where u.id = c.codusuario and dia = '$dia' and mes = '$mes' and ano = '$ano' and codusuario = '$codusuario'");
$teste = mysql_fetch_array($sql);
if (mysql_num_rows($sql) == 0) {
	echo "<meta http-equiv='refresh' content='0;URL=load.php'>";
}
$lpp = 100; // Especifique quantos resultados voc&ecirc; quer por p&aacute;gina
$total = mysql_num_rows($sql); // Esta fun&ccedil;&atilde;o ir&aacute; retornar o total de linhas na tabela
$paginas = ceil($total / $lpp); // Retorna o total de p&aacute;ginas
if(!isset($pagina)) { $pagina = 0; } // Especifica uma valor para variavel pagina caso a mesma n&atilde;o esteja setada
$inicio = $pagina * $lpp; // Retorna qual ser&aacute; a primeira linha a ser mostrada no MySQL
$sql = mysql_query("select c.*, u.nome from calendario c, usuarios u where u.id = c.codusuario and dia = '$dia' and mes = '$mes' and ano = '$ano' and codusuario = '$codusuario' order by dh_inclusao LIMIT $inicio, $lpp"); // Executa a query no MySQL com o limite de linhas.

while ($dados = mysql_fetch_array($sql)){ 

	$month = $dados["mes"];

	$catgoria = $dados["categoria"];

	switch ($month) {
		case 01:  $month_name = "Janeiro";	break;
		case 02:  $month_name = "Fevereiro";	break;
		case 03:  $month_name = "Mar�o";	break;
		case 04:  $month_name = "Abril";	break;
		case 05:  $month_name = "Maio";	break;
		case 06:  $month_name = "Junho";	break;
		case 07:  $month_name = "Julho";	break;
		case 08:  $month_name = "Agosto";	break;
		case 09:  $month_name = "Setembro";	break;
		case 10: $month_name = "Outubro";	break;
		case 11: $month_name = "Novembro";	break;
		case 12: $month_name = "Dezembro";	break;
	   }

	switch ($catgoria) {
		case 1:  $cat_name = "<font color=#666666><strong>Nenhuma</strong></font>";	break;
		case 2:  $cat_name = "<font color=#CC3300><strong>Importante</strong></font>";	break;
		case 3:  $cat_name = "<font color=#6666CC><strong>Neg�cios</strong></font>";	break;
		case 4:  $cat_name = "<font color=#669900><strong>Pessoal</strong></font>";	break;
		case 5:  $cat_name = "<font color=#999900><strong>Folga</strong></font>";	break;
		case 6:  $cat_name = "<font color=#FF9900><strong>Deve ser atendido</strong></font>";	break;
		case 7:  $cat_name = "<font color=#FF00FF><strong>Anivers�rio</strong></font>";	break;
		case 8:  $cat_name = "<font color=#FF3300><strong>Liga��o Telef�nica</strong></font>";	break;
	   }
?>				
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="text">
		<tr>
		  <td width="92"><div align="right"><strong>Data:</strong></div></td>
		  <td><?php echo $dados["dia"]; ?> de <?php echo $month_name; ?> de <?php echo $dados["ano"]; ?></td>
		</tr>
		<tr>
		  <td width="110"><div align="right"><strong>Data de inclus&atilde;o:</strong></div></td>
		  <td><?php if (isset($dados["dh_inclusao"])) { echo to_date($dados["dh_inclusao"]); } ?></td>
		</tr>	
		<tr>
		  <td width="110"><div align="right"><strong>Ultima altera&ccedil;&atilde;o:</strong></div></td>
		  <td><?php if (isset($dados["dh_ult_alteracao"])) { echo to_date($dados["dh_ult_alteracao"]); } ?></td>
		</tr>			
		<tr>
		  <td><div align="right"><strong>Assunto:</strong></div></td>
		  <td><?php echo $dados["assunto"]; ?></td>
		</tr>
		<tr>
		  <td><div align="right"><strong>Categoria:</strong></div></td>
		  <td><?php echo $cat_name; ?></td>
		  </tr>
		<tr>
		  <td><div align="right"><strong>Local:</strong></div></td>
		  <td><?php echo $dados["local"]; ?></td>
		</tr>
		<tr>
		  <td valign="top"><div align="right"><strong>Descri&ccedil;&atilde;o:</strong></div></td>
		  <td valign="top"><?php echo $dados["descricao"]; ?></td>
		</tr>
		<tr>
		  <td valign="top"><div align="right"><strong>Usuario:</strong></div></td>
		  <td valign="top"><?php echo $dados["nome"]; ?></td>
		</tr>			
		<tr>
		  <td>&nbsp</td>
		  <td>
		  <table cellpadding="0" cellspacing="0" class="texto">
			  <td width="20"><div align="center"><img src="ico-edit.gif" width="16" height="16" border="0"></div></td>
			  <td width="40"><a href="alterarcompromisso.php?id=<?php echo $dados["id"]; ?>"><font color="#666666"><strong>Editar</strong></font></a></td>
			  <td width="21"><div align="center"><img src="ico-lixo.gif" width="16" height="16" border="0"></div></td>
			  <td width="48"><a href="?id=<?php echo $dados['id']; ?>&acao=excluir<?php echo "&dia=$dia&mes=$mes&ano=$ano"?>"><font color="#666666"><strong>Apagar</strong></font></a></td>
		  </table>
		  </td>
		</tr>
	</table>
	<br>
<?php
}
?><br>
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="texto">
	  <tr>
		<td height="20"><center>
			<?php
if ($total > $lpp) {

if($pagina > 0) {
   $menos = $pagina - 1;
   $url = "$PHP_SELF?pagina=$menos$dia=$dia&mes=$mes&ano=$ano";
   echo "<a href=\"$url\"><strong>Anterior</strong></a>"; // Vai para a p&aacute;gina anterior
}
for($i=0;$i<$paginas;$i++) { // Gera um loop com o link para as p&aacute;ginas
if ($i == $pagina) { 
   echo " |</font> <a href=#><strong><font color=#669900>$i</font></strong></a>";
    } 
    // as demais p&aacute;ginas deve ser linkadas... 
    else { 
   $url = "$PHP_SELF?pagina=$i&dia=$dia&mes=$mes&ano=$ano";
   echo " | <a href=\"$url\"><strong>$i</strong></a>";
    } 

}
if($pagina < ($paginas - 1)) {
   $mais = $pagina + 1;
   $url = "$PHP_SELF?pagina=$mais&dia=$dia&mes=$mes&ano=$ano";
   echo " | <a href=\"$url\"><strong>Pr&oacute;xima</strong></a>";
}

}
?>
                            </center></td>
                          </tr>
                      </table>		  
	  
</fieldset>		</td>
  </tr>
</table>    </td>
  </tr>
</table>
</body>
</html>
