<?php
session_start();
ob_start();
if(isset($_SESSION['nome_usu_sessao'])){
	echo 'Olá '.$_SESSION['nome_usu_sessao'].', Seja bem vindo!';
}
?>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Asriel: Protótipo de plataforma para cursos online, Versão: Alpha 1.0" />
		<meta name="keywords" content="Asriel, desenvolvimento de jogos, cursos, alunos, instrutores, tempo, aulas, online" />
		<meta name="author" content="Shantal de Morais Mantovani & Victor Pedro de Sousa" />
		<meta name="generator" content="Notepad++"/>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"/>
			<link rel="shorcut icon" href="imagens/a.png" />
			<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
	</head>
<header>
		<nav>
			<div class="menu">
				<div class="logo">
					<a href="index.php">
						<img src="imagens/fc.png" height="50px">
						<p class="text2">Asriel</p>
					</a>
				</div>
				<div class="cat_drop">
					<div class="dropdown">
						<button class="dropbtn"><i class="fas fa-th"></i> Categorias</button>		
						<div class="dropdown-conteudo">
							<a href="desengames.php">Desenvolvimento de Games</a>
							<a href="webdesign.php">Web Design</a>
							<a href="ingles.php">Inglês</a>
							<a href="html.php">HTML</a>
							<a href="javascript.php">Javascript</a>
						</div> 
					</div>	
				</div>
				<div class="busca">
					<form class="buscando" name="busque" action="crud/select.php" method="post">
						<input type="text" name="buscar" id="buscar" class="buscar" placeholder="Buscar cursos"></input>
							<button class="busca" type="submit"><i class="fas fa-search"></i></button>
					</form>
				</div>
				<div class="torn_inst">
					<a href="inst_cadastro.php" class="inst_cad">Torne-se um instrutor</a>
				</div>
				<div class="logon_cadastro">
					<a href="cadastro.php" class="log_cad">Cadastro</a>
				</div>	
				<div class="logi">
					<a href="login.php" class="log_cad2">Login</a>
				</div>
			</div>
		</nav>
	</header>
