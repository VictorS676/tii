<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
	<footer>
		<div class="links">
			<aside class="ld">
				<a href="cadastro.php">Cadastro</a><br/>
				<a href="login.php">Login</a><br/>
				<a href="torninst.php">Torne-se um instrutor</a><br/>
			</aside>
			<aside class="ld2">
				<a href="sobrenos.php">Sobre Nós</a><br/>
				<a href="carreiras.php">Carreiras</a><br/>
				<a href="noticias.php">Notícias</a><br/>
			</aside>
			<aside class="ld3">
				<a href="suporte.php">Suporte</a><br/>
				<a href="duvidas.php">Dúvidas</a><br/>
			</aside>
					<div class="middle">
						<a class="btn face" href="https://pt-br.facebook.com/">
							<i class="fab fa-facebook-f"></i>
						</a>
						<a class="btn twi" href="https://twitter.com/?lang=pt-br">
							<i class="fab fa-twitter"></i>
						</a>
						<a class="btn insta" href="https://www.instagram.com/?hl=pt-br">
							<i class="fab fa-instagram"></i>
						</a>
						<a class="btn goo" href="https://plus.google.com/discover">
							<i class="fab fa-google"></i>
						</a>
						<a class="btn ytb" href="https://www.youtube.com/?gl=BR">
							<i class="fab fa-youtube"></i>
						</a>
					</div>
		</div>
		<div class="dados">
			<div class="logo">
				<a href="index.php">
					<img src="imagens/fc.png" height="50px">
				</a>
			</div>
			<div class="direitos">
				<p>Copyright © 2018 Asriel, Inc.</p>
			</div>
			<div class="burocracia">
				<a href="termos.php">Termos de Uso</a>
				<a href="politicapriv.php">Política de Privacidade</a>
				<a href="propint.php">Propriedade Intelectual</a>
			</div>
		</div>
	</footer>
</body>
</html>	