<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8"/>
		<meta name="author" content="Shantal de Morais Mantovani & Victor Pedro de Sousa"/>
		<meta name="description" content="Casdastre-se e aproveite todas as nossas opções de cursos online"/>
		<meta name="generator" content="Notepad++"/>
		<meta name="keywords" content="cursos, aprender, educação, login"/>
			<link rel="stylesheet" type="text/css" href="css/estilo.css"/>
			<link rel="shortcut icon" type="image/x-icon" href="imagens/favicon.png"/>
				<title> Cadastro de Professores </title>
	</head>
		<body>
			<form action="crud/inserirprof.php" method="post" name="profcad" class="profcad" id="profcad">
			<input style="display:none;" class="profid" id="prof_id" name="prof_id" value="prof_id" type="number" maxlength="8"/>
				<label>Nome: </label>
					<input name="nome" type="text" maxlength="25" placeholder="Insira seu nome aqui." class="nome" id="nome"/><br/><br/>
				<label>Sobrenome: </label>
					<input name="sobrenome" type="text" maxlength="25" placeholder="Insira seu sobrenome aqui." class="sobrenome" id="sobrenome"/><br/><br/>
				<label>RG: </label>
					<input name="rg" type="number" maxlength="20" placeholder="Insira seu RG aqui, sem pontos e traços." class="rg" id="rg"/><br/><br/>
				<label>CPF: </label>
					<input name="cpf" type="number" maxlength="20" placeholder="Insira seu CPF aqui, sem pontos e traços." class="cpf" id="cpf"/><br/><br/>
				<label>Idade: </label>
					<input name="idade" type="number" maxlength="2" placeholder="Insira sua idade aqui." class="idade" id="idade"/><br/><br/>
				<label>E-Mail: </label>
					<input name="email" type="email" maxlength="60" placeholder="Insira seu E-mail aqui." class="email" id="email"/><br/><br/>
				<label>Senha: </label>
					<input name="senha" type="password" maxlength="30" placeholder="Crie uma senha." class="senha" id="senha"/><br/><br/>
				<label>Confirme a senha: </label>
					<input name="resenha" type="password" maxlength="30" placeholder="Redigite a senha." class="resenha" id="resenha"/><br/><br/>
			</form>
		</body>
</html>