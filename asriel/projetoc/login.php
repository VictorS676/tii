<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Login</title>
<script type="text/javascript">
function valida_campos(){
	if(document.cadastro.getElementById('nome_usu').value == ''){
		alert('Por favor, preencha os campos obrigatórios!');
		document.cadastro.getElementById('nome_usu').focus();
		return false;
	}
	if(document.cadastro.getElementById('email_usu').value == ''){
		alert('Por favor, preencha os campos obrigatórios!');
		document.getElementById('email_usu').focus();
		return false;
	}	
	if(document.cadastro.getElementById('senha_usu').value == '' || document.cadastro.getElementById('senha_usu').value != document.cadastro.getElementById('senha2_usu').value){
		alert('As senhas não conferem, por favor, redigite!');
		document.getElementById('senha_usu').focus();
		return false;
	}
}
</script>		
	<form class="login-caixa" action="valida.php" method="post">
		<h1>Login</h1>
			<div class="textocaixa">
				<i class="far fa-user"></i>
				<input type="text" id="email_usu" name="email_usu" placeholder="E-Mail" class="" required="required">
			</div>
			<div class="textocaixa">
				<i class="fas fa-fingerprint"></i>
				<input type="password" id="senha_usu" name="senha_usu" placeholder="Senha" class="" required="required">
			</div>
				<button type="submit" name="login" class="btnw">Entrar</button>			
	</form>
<style type="text/css">

@import "https://use.fontawesome.com/releases/v5.5.0/css/all.css";

body{
	margin:0;
	padding:0;
	font-family:sans-serif;
	background:rgb(20,20,20);
	background-size:cover;
}

.login-caixa{
	width:32%;
	position:relative;
	top:50%;
	left:50%;
	transform:translate(-50%,-50%);
	color:#fff;
}

.login-caixa h1{
	float:left;
	font-size:40px;
	border-bottom:6px solid #3090C7;
	margin-bottom:50px;
	padding:13px 0;
}
	
.textocaixa{
	width:100%;
	overflow:hidden;
	font-size:20px;
	padding:10px 0;
	margin:8px;
	border-bottom:1px solid #3090C7;
}

.textocaixa i{
	width:26px;
	float:left;
	text-align:center;
}

.textocaixa input{
	border:none;
	outline:none;
	background:none;
	color:white;
	font-size:18px;
	width:80%;
	float:left;
	margin: 0 10px;
}

.btnw{
	width:100%;
	background:none;
	border:2px solid #3090C7;
	color:white;
	padding:5px;
	font-size:18px;
	cursor:poniter;
	margin:12px 0;
	transition:0.8s;
	margin-top:15%;
	border-radius:2px;
}

.btnw:hover{
	background-color:#46C7C7;
	border:2px solid #46C7C7;	
}

input.cado{ margin-top:5%; }
.desce{margin-top:5%;}

.servicos{
	background:#f1f1f1;
	text-align:center;
}

.servicos h1{
	display:inline-block;
	text-transform:uppercase;
	border-bottom:4px solid #3EA99F;
	font-size:20px;
	padding-bottom:10px;
	margin-top:40px;
}

.cen{
	max-width:1200px;
	margin:auto;
	overflow:hidden;
	padding:20px;
}

.servico{
	display:inline-block;
	width:calc(100%/3);
	margin:0 -2px;
	padding:20px;
	box-sizing:border-box;
	cursor:pointer;
	transition:0.4s;
}

.servico:hover{
	background:#ddd;
}

.servico i{
	color:#3EA99F;
	font-size:34px;
	margin-bottom:30px;
}

.servico h2{
	font-size:18px;
	text-transform:uppercase;
	font-weight:500;
	margin:0;
}

.servico p{
	color:gray;
	font-size:15px;
	font-weight:500;
}

@media screen and (max-width:800px){
	.servico{
			width:50%;
	}
}

@media screen and (max-width:500px){
	.servico{
			width:100%;
	}
}

</style>