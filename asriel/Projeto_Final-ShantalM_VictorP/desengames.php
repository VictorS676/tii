<title>Desenvolvimento de Games</title>
<?php 
include('menu.php');

if(isset($_SESSION['nome_usu_sessao'])){
?>
	<p>Aqui é o conteúdo restrito<a href="logout.php">Sair</a></p>

<?php
}else{
?>
<div class="fundo">
<div class="ops">
	<h1>Ops...</h1>
		<h3>Esta é uma área restrita, por favor, efetue <span>Login</span> ou <span>Cadastre-se</span>.</h3>
	<img src="imagens/b.gif"/>
</div>	
</div>
	<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Press+Start+2P');
	
	div.fundo{
		position:relative;
		width:100%;
		background:rgb(20,20,20);
	}
	
	div.ops{
		position:relative;
		margin-top:4%;
		margin-bottom:4%;
		margin-left:25%;
		margin-right:25%;
	}
	
	.ops h1{
		font-family: 'Press Start 2P', cursive;
		text-align:center;
		font-size:32px;
		color:#009999;
	}
	
	.ops h3{
		font-size:14px;
		text-indent:3%;
		color:#f1f1f1;
		font-family: 'Press Start 2P', cursive;
	}
	
	.ops span{
		color:#009999;
	}
	
	.ops img{
		margin-left:25%;
		margin-top:3%;
		width:40%;
	}
	</style>
<?php
}

include('footer.php');
?>