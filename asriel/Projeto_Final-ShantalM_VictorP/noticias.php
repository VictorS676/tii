	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
		<title>Notícias e Novidades sobre Asriel</title>
			<link rel="shorcut icon" href="imagens/a.png"/>
<body>
<?php
include ('menu.php');
?>
	<div class="not-caixa">
		<h1>Notícias e Novidades</h1>
			<div class="noti">
				<h5>Novembro 14, 2018</h5>
				<h3>ENSINO A DISTÂNCIA EM ALTA REDUZ FATIA DE GIGANTE DO SETOR.</h3>
				<img src="imagens/ead7.png" />
				<p>O ensino a distância passa por um período de forte crescimento desde o ano passado, quando o governo voltou a liberar a oferta de cursos. O número total de alunos cresceu 16% em 2017, para 1,6 milhão, enquanto o ensino presencial encolheu 1%, para 4,6 milhões de alunos. O filão virou foco de todos os grandes grupos de ensino, mas dados inéditos da consultoria Atmã mostram que quem se beneficiou mesmo foram os grupos regionais. </p>
			</div>
			<div class="noti">
				<h5>Novembro 13, 2018</h5>
				<h3>ENSINO SUPERIOR VOLTA A CRESCER NO PAÍS, MAS SÓ NA MODALIDADE A DISTÂNCIA.</h3>
				<img src="imagens/e.jpg" />
				<p>O número de alunos matriculados no ensino superior aumentou 3% em 2017, após estagnação no ano anterior. O crescimento, no entanto, só ocorreu na modalidade a distância. No ano passado, o país tinha 8,3 milhões de alunos em cursos de nível superior (presencial e a distância), contra 8,05 milhões em 2016. Os dados são do Censo da Educação Superior de 2017, divulgado pelo MEC (Ministério da Educação).</p>
			</div>
			<div class="noti">
				<h5>Outubro 12, 2018</h5>
				<h3>EAD CRESCE EM RITMO MAIOR DO QUE O ENSINO PRESENCIAL.</h3>
				<img src="imagens/e3.jpg" />
				<p>A educação a distância cresce em ritmo mais acelerado do que o ensino presencial e já é opção para quase metade das pessoas que buscam uma graduação. Pesquisa divulgada da Associação Brasileira de Mantenedoras de Ensino Superior (ABMES) – que representa grande parte do ensino superior particular do País – mostra que 44% dos entrevistados optariam por essa modalidade, enquanto 56% dizem que preferem o ensino presencial. </p>
			</div>
			<div class="noti">
				<h5>Outubro 11, 2018</h5>
				<h3>POLOS DE ENSINO SUPERIOR A DISTÂNCIA CRESCEM 133% EM UM ANO.</h3>
				<img src="imagens/e4.jpg" />
				<p>Em pouco mais de um ano, o total de polos de ensino superior a distância subiu de 6.583 para 15.394, segundo dados do MEC (Ministério da Educação). A alta de 133% resulta de um decreto que diminuiu as exigências para a oferta da modalidade.

Entre as mudanças estão a autonomia para que as instituições criem seus polos —antes era preciso visita prévia de técnicos do ministério— e o credenciamento de instituições na modalidade EaD sem a exigência da oferta simultânea de cursos presenciais. </p>
			</div>
			<div class="noti" id="abaixa">
				<h5>Setembro 8, 2018</h5>
				<h3>Instrutor do Asriel em destaque: Geraldo Ramos</h3>
				<img src="imagens/e5.jpg" />
				<p>O instrutor do Asriel, Geraldo Ramos, sempre esteve procurando, sem sucesso, um curso de React Testing, ele naturalmente decidiu aprender muito mais sobre o assunto criando um curso no Asriel. Leia mais abaixo sobre a experiência de Geraldo ensinando on-line pela primeira vez!</p>
			</div>
	</div>

<style type="text/css">

section.noticias{
	width:100%;
	height:100vh;
	overflow:hidden;
}

div.not-caixa{
	width:100%;
	margin-top:2.5%;
}

h1{
	color:#5499C7;
}

h3{
	color:#5499C7;
	padding-left:30%;
}

h5{
	color:#2C3E50;
	padding-left:2%;
}

p{
	position:relative;
	color:#212F3D;
	text-align:justify;
	text-indent:4%;
	padding-right:4%;
}

div.noti{
	border-color:rgb(160,160,160);
	border-style:solid;
	border-width:0px 0px 1px 0px;
	width:70%;
	display:block;
	margin-top:1%;
	margin-left:15%;
	transition:0.8s;
}

div.noti:hover{
	background-color:#ddd;
}

div.noti:hover img{
	opacity: 0.8;
}

.noti img{
	width:auto;
	height:120px;
	float:left;
	margin-right:4%;
	margin-bottom:4%;
	padding-left:2%;
	transition:0.8s;
	opacity: 1;
	border-radius:2%;	
}

.not-caixa h1{
	text-align:center;
	font-size:32px;
}

div#abaixa{
	margin-bottom:7.5%;
}

</style>
<?php
include('footer2.php');
?>	