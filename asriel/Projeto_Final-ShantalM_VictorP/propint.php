<!-- <!DOCTYPE html> -->
<html lang="pt-br">
	<head>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"/>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Asriel: Protótipo de plataforma para cursos online, Versão: Alpha 1.0" />
		<meta name="keywords" content="Asriel, desenvolvimento de jogos, cursos, alunos, instrutores, tempo, aulas, online" />
		<meta name="author" content="Shantal de Morais Mantovani & Victor Pedro de Sousa" />
		<meta name="generator" content="Notepad++"/>
			<link rel="shortcut icon" type="image/x-icon" href="imagens/"/>
			<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
			<link rel="shorcut icon" href="imagens/a.png" />
				<title>Asriel - Política de Privacidade</title>
	</head>		
<body>
	<header>
		<nav>
			<div class="menu">
				<div class="logo">
					<a href="index.html">
						<img src="imagens/fc.png" height="50px">
						<p class="text2">Asriel</p>
					</a>
				</div>
				<div class="cat_drop">
					<div class="dropdown">
						<button class="dropbtn"><i class="fas fa-th"></i> Categorias</button>		
						<div class="dropdown-conteudo">
							<a href="#">Desenvolvimento de Games</a>
							<a href="#">Web Design</a>
							<a href="#">Inglês</a>
							<a href="#">HTML</a>
							<a href="#">Javascript</a>
						</div> 
					</div>	
				</div>
				<div class="busca">
					<form class="buscando" name="busque" action="crud/select.php" method="post">
						<input type="text" name="buscar" id="buscar" class="buscar" placeholder="Buscar cursos"></input>
							<button class="busca" type="submit"><i class="fas fa-search"></i></button>
					</form>
				</div>
				<div class="torn_inst">
					<a href="inst_cadastro.php" class="inst_cad">Torne-se um instrutor</a>
				</div>
				<div class="logon_cadastro">
					<a href="cadastro.php" class="log_cad">Cadastro</a>
				</div>	
				<div class="logi">
					<a href="login.php" class="log_cad2">Login</a>
				</div>
			</div>
		</nav>
		<section class="banner">
			<img src="imagens/banner6.png" width="100%"/>
			<img src="imagens/banner6.png" width="100%"/>
				<div class="msg">
					<h1><i>Tudo é possível</i></h1>
					<p>Construa seu futuro com os melhores cursos e certificações on-line do Brasil.</p>			
				</div>
				<div class="info">
					<h4><i class="far fa-address-book"></i> 80.000 visitantes anuais</h4>
						<p>Conheça uma Variedade de temas atuais.</p>
					<h4><i class="fas fa-briefcase"></i> Especialistas do Setor</h4>
						<p>Encontre o professor ideal para seu ritmo.</p>	
					<h4><i class="fas fa-user-clock"></i> Acesso Vitalício</h4>
						<p>Estude no seu ritmo do seu jeito.</p>				
				</div>
		</section>
	</header>
	<main>
		<section class="pp">
		<h1>PROPRIEDADE INTELECTUAL</h1>
<h5>Rev. 27.11.2018</h5>
<p><b>1. APLICABILIDADE. </b>A venda de todo e qualquer serviço e/ou produto do Vendedor
incluídos e/ou fornecidos em relação com esses serviços (“Mercadorias”) deve ser
condicionada e sujeita aos seguintes termos e condições (“Termos e Condições”), que
devem ser parte integrante de qualquer acordo entre o Comprador e o Vendedor. A
aceitação do Comprador de qualquer Cotação, proposta ou oferta feita pelo Vendedor para
a venda de seus serviços e/ou mercadorias (coletivamente, “Cotação”) está expressamente
sujeita a estes Termos e Condições, e nenhum dos Termos e Condições pode ser
adicionado, modificado, substituído ou alterado, exceto como revisto por escrito pelo
Vendedor. Todos os pedidos de serviços e/ou Mercadorias recebidos pelo Vendedor devem
ser regulados apenas por estes Termos e Condições, não obstante os termos e condições
em qualquer pedido de compra, ordem de lançamento ou qualquer outro formulário
emitido pelo Comprador. O Vendedor, por meio deste instrumento, faz objeção a
quaisquer termos e condições que possam ser encontrados em qualquer pedido de
compra, pedido de liberação, ou em qualquer outro formulário emitido pelo Comprador e,
por meio deste, comunica aqui ao Comprador que foram rejeitados.</p>
<p><b>2. PREÇO. </b>Os preços cotados pelo Vendedor devem permanecer fixos por um período de
trinta (30) dias a partir da data de Cotação, desde que, no entanto, o Vendedor se reserve
o direito de, a qualquer momento, antes da aceitação de uma Cotação pelo Comprador,
ajustar tais preços por meio de notificação por escrito ao Comprador sobre qualquer tal
ajuste. As cotações fornecidas são baseadas nos preços da compra do Comprador de todo
o escopo de serviços e/ou Mercadorias identificados em uma Cotação. Se menos de todo o
escopo de serviços e/ou Mercadorias identificados em uma Cotação for pedido pelo
Comprador, os preços podem variar. Salvo disposição em contrário em uma Cotação, os
serviços de instalação, comissionamento, supervisão e/ou serviços iniciais não estão
incluídos no preço de serviços e/ou Mercadorias a serem fornecidos pelo Vendedor. O
Comprador deve pagar ao Vendedor até a extensão dos serviços prestados ou pela
quantidade de mercadorias embarcadas, caso o Vendedor seja incapaz, por qualquer
motivo, de fornecer e/ou enviar todo o escopo de serviços e/ou produtos identificados em
uma Cotação. Os preços cotados pelo Vendedor excluem todos os impostos (exceto
impostos incidentes sobre o lucro do Vendedor), incluindo impostos dos governos federal,
estadual, uso provincial e local, por vendas, propriedade ou impostos semelhantes, e o
Comprador deve pagar todos os impostos na íntegra ou reembolsar o Vendedor por
quaisquer tais impostos pagos pelo Vendedor.</p>
<p><b>3. ESCOPO DOS SERVIÇOS E MERCADORIAS. </b>Os serviços e/ou produtos fornecidos pelo
Vendedor nos termos de uma Cotação são limitados exclusivamente, a pedido expresso do
Comprador, aos serviços e/ou Mercadorias expressamente identificados em tal Cotação.
Como resultado, o Vendedor não assume a responsabilidade e/ou obrigação devido à falta
em fornecer quaisquer outros serviços e/ou Mercadorias. Modificações, acréscimos ou
exclusões em ou a partir do escopo referenciado em uma Cotação, só serão efetivos caso
evidenciados, por escrito e assinados pelo Vendedor, e a venda de toda e qualquer
Mercadoria afetada por essa alteração, adição ou exclusão deve ser sujeita a estes Termos
e Condições, quer seja ou não referenciada na Cotação.</p>
<p><b>4. COBRANÇA MÍNIMA. </b>Todos os serviços e/ou produtos fornecidos pelo Vendedor estão
sujeitos a uma taxa mínima de cem dólares (US$ 100,00) (ou o equivalente em moeda
local). Caso a quantidade total de serviços e/ou Mercadorias adquiridos pelo Comprador
(excluindo custos de frete) resultar em um custo de menos de cem dólares (US$100,00) (ou
o equivalente em moeda local), o Vendedor reserva-se o direito de cobrar do Comprador a
diferença entre o preço dos serviços e/ou Mercadorias compradas e cem dólares
(US$100,00) (ou o equivalente em moeda local) como um custo adicional pelos serviços
e/ou Mercadorias adquiridos.</p>
<p><b>5. CONDIÇÕES DE PAGAMENTO. </b>Salvo disposição em contrário da Cotação, todas as
faturas do Vendedor devem ser pagas pelo Comprador no prazo de quinze (15) dias a
contar da data da fatura. Caso o Comprador não pague, no tempo devido, as faturas, o
Vendedor terá o direito de suspender todo o trabalho e as entregas e emitir uma taxa
equivalente de atraso, o que for menor entre um e meio por cento (1,5%) por mês (dezoito
por cento (18%) por ano) ou a taxa máxima permitida por lei sobre todas as faturas em
aberto ou faturas não pagas de acordo com os Termos e Condições. O Comprador deverá
reembolsar o Vendedor por todas as despesas, independentemente da sua natureza ou
tipo (incluindo honorários advocatícios), relacionadas de alguma forma à coleta do
Vendedor de faturas não pagas, de acordo com estes Termos e Condições, ou incorridos
pelo Vendedor na aplicação destes Termos e Condições. Salvo disposição em contrário em
uma Cotação, para todos os serviços e/ou Mercadorias com um preço de Cotação ou de
pedido de compra de mais de 30 mil dólares (US$30.000,00) (ou o equivalente em moeda
local), o Comprador deve pagar um depósito no valor de quarenta por cento (40%) do
preço (devido após o recebimento do Vendedor do pedido de compra do Comprador). O
Comprador deve fazer pagamentos progressivos, como indicado na Cotação aplicável, ou
de outra forma acordado por escrito e assinado pelo Comprador e Vendedor. O Comprador
não terá direito a compensar quaisquer montantes devidos ao Vendedor com qualquer
pagamento ou outra obrigação que o Vendedor ou qualquer de suas afiliadas possa dever
ao Comprador.</p>
<p><b>6. CANCELAMENTO. </b>Um pedido de compra pode ser cancelado pelo Vendedor, a qualquer
momento, caso (a) o Comprador não cumpra rigorosamente com os termos que regem o
pedido, (b) o Comprador tornar-se insolvente ou fizer uma cessão para o benefício dos
credores, (c) uma petição de falência ou insolvência for apresentada por ou contra o
Comprador ou (d) quantias devidas ao Vendedor pelo Comprador não tiverem sido pagas.
Após o cancelamento de um pedido de compra, o Comprador será obrigado a pagar ao
Vendedor o preço por todos os serviços realizados até o momento e todas as Mercadorias
que podem ser completadas e enviadas dentro de trinta (30) dias da data do cancelamento,
todas as ferramentas especiais para as quais compromissos foram feitos pelo Vendedor, e
todos os custos do Vendedor, despesas e lucro razoável para o trabalho em andamento a
partir da data do cancelamento.</p>
<p><b>7. APROVAÇÃO DE CRÉDITO. </b>Todos os pedidos estão sujeitos à aprovação de crédito do
Comprador pelo Vendedor. O Vendedor reserva-se o direito de recusar o embarque ou
prestação de todos e quaisquer serviços e/ou venda de Mercadorias identificados(as) em
qualquer Cotação ou pedido de compra, de modificar as condições de pagamento
identificadas nesse instrumento ou na Seção 5 deste ou de cancelar sem multa ou cobrar
qualquer pedido ou contrato formado e sobre os serviços e/ou Mercadorias
identificados(as) na Cotação aplicável ou pedido de compra caso, a seu exclusivo critério e
por qualquer motivo, o Vendedor requisitar e for incapaz de assegurar garantias de
pagamento aceitáveis do Comprador para os serviços e/ou produtos identificados em uma
Cotação ou pedido de compra.</p>
<p><b>8. TERMOS DE ENTREGA E ATRASOS. </b>Salvo disposição em contrário identificada em uma
Cotação, todas as transferências de mercadorias são FCA desde a fábrica, armazém ou doca
do Vendedor, como definido pelo Incoterms 2010, e todos os riscos de perda com relação a
quaisquer mercadorias enviadas passarão para o Comprador quando essas mercadorias
forem entregues ao transportador em tal fábrica, armazém ou doca. A titularidade das
mercadorias deve se transferir ao Comprador após o recebimento do Vendedor de
pagamento integral de todas as mercadorias e serviços prestados de acordo com uma
Cotação e/ou pedido de compra.</p>
<p><b>a. Entrega. </b>Todas as datas para o envio e/ou entrega de mercadorias e/ou para prestação
de serviços são aproximadas. O Vendedor não se responsabiliza por atraso ou falha em
fazer o envio e/ou entrega de Mercadorias ou início, execução ou conclusão de serviços até
qualquer data identificada por qualquer motivo. Em caso de atraso, independentemente da
causa, as partes devem concordar sobre uma nova data para o envio e/ou entrega de
Mercadorias e/ou início, execução ou conclusão dos Serviços. Em caso de qualquer atraso
causado pelo Comprador, este deve pagar ao Vendedor por todos os custos e despesas
incorridos pelo Vendedor relacionado a esse atraso.</p>
<p><b>b. Despesas de Frete. </b>Qualquer referência aos custos de frete contidos em uma Cotação é
uma estimativa. O Vendedor não é responsável por quaisquer diferenças que possam
ocorrer entre estimativas de frete contidas em uma Cotação e o frete real aplicável no
momento do embarque, e o Comprador será responsável por todos os custos a ele
associados.</p>
<p><b>c. Embalagem. </b>Salvo disposição em contrário identificada em uma Cotação, os preços
cotados não incluem o custo de exportação ou embalagem especial de Mercadorias e o
Comprador deve ser responsável por esses custos adicionais associados a essa exportação e
embalagens.</p>
<p><b>d. Custo das Mercadorias. </b>Salvo disposição em contrário em uma Cotação, o Comprador
deve pagar todos os aumentos de custo do Vendedor avaliados para materiais
incorporados nos serviços e/ou Mercadorias, incluindo, sem limitação, sobretaxas de aço,
cobre e combustível, na medida em que tais aumentos excederem os custos estimados
usados pelo Vendedor para desenvolver uma Cotação em dez por cento (10%) e que
ocorrer após a emissão de uma Cotação, mas antes da entrega das mercadorias e/ou
entrega das Mercadorias.</p>
<p><b>e. Condição do Local. </b>O Comprador garante que o local onde as mercadorias devem ser
entregues e/ou instaladas e/ou em que os serviços devem ser realizados deve estar pronto
e adequado para a entrega do Vendedor e/ou instalação de Mercadorias e/ou prestação de
serviços. As obrigações do Comprador, neste sentido, incluem, sem limitação, a remoção
de todos os obstáculos e instituição de segurança adequada, medidas de proteção de
propriedade, funcionários, agentes e prestadores de serviços do Vendedor. O Comprador
será responsável por todos os custos e despesas associados ao atraso do Vendedor e/ou
incapacidade de fornecer e/ou instalar quaisquer Mercadorias e/ou realizar quaisquer
serviços relacionados à falha do Comprador em estar em conformidade com esta
disposição. O Vendedor em nada garante a suficiência do local para os serviços e/ou
Mercadorias referenciadas em uma Cotação.</p>
<p><b>9. DIREITO DE GARANTIA. </b>O Comprador concede ao Vendedor uma garantia real sobre as
mercadorias para garantir o saldo devedor do preço e todas as outras obrigações do
Comprador ao Vendedor que possam surgir. O Comprador autoriza o Vendedor a
apresentar todas as declarações de financiamento necessárias e outros documentos
similares necessários para aperfeiçoar a garantia real aqui concedida e irrevogavelmente
concede ao Vendedor uma procuração para assinar quaisquer documentos em nome do
Comprador relacionado a isso.</p>
<h3>10. GARANTIAS.</h3>
<p>a. Produtos fabricados pelo Vendedor (“Produtos do Vendedor”). Na medida em que os
Produtos do Vendedor forem incorporados no escopo de serviços e/ou Mercadorias
declaradas em uma Cotação, tais Produtos do Vendedor serão garantidos de acordo com a
Garantia Padrão do Vendedor para produtos do Vendedor desse tipo em vigor na data de
venda (incorporado por referência como se totalmente reescrito aqui e uma cópia do qual
está anexa à Cotação ou disponível em www.konecranes.com.br ou a pedido do
Comprador). Reparações e substituições fornecidas em conformidade com a Garantia
Padrão não estendem a garantia original fornecida com a Mercadoria no momento da
venda.</p>
		</section>
			<a class="gotopbtn" href="#">
				<i class="fas fa-arrow-up"></i>
			</a>
	</main>	
	<footer>
		<div class="links">
			<aside class="ld">
				<a href="#">Cadastro</a><br/>
				<a href="#">Login</a><br/>
				<a href="#">Torne-se um instrutor</a><br/>
			</aside>
			<aside class="ld2">
				<a href="#">Sobre Nós</a><br/>
				<a href="#">Carreiras</a><br/>
				<a href="#">Notícias</a><br/>
			</aside>
			<aside class="ld3">
				<a href="#">Suporte</a><br/>
				<a href="#">Dúvidas</a><br/>
			</aside>
					<div class="middle">
						<a class="btn face" href="https://pt-br.facebook.com/">
							<i class="fab fa-facebook-f"></i>
						</a>
						<a class="btn twi" href="https://twitter.com/?lang=pt-br">
							<i class="fab fa-twitter"></i>
						</a>
						<a class="btn insta" href="https://www.instagram.com/?hl=pt-br">
							<i class="fab fa-instagram"></i>
						</a>
						<a class="btn goo" href="https://plus.google.com/discover">
							<i class="fab fa-google"></i>
						</a>
						<a class="btn ytb" href="https://www.youtube.com/?gl=BR">
							<i class="fab fa-youtube"></i>
						</a>
					</div>
		</div>
		<div class="dados">
			<div class="logo">
				<a href="#">
					<img src="imagens/fc.png" height="50px">
				</a>
			</div>
			<div class="direitos">
				<p>Copyright © 2018 Asriel, Inc.</p>
			</div>
			<div class="burocracia">
				<a href="termos.php">Termos de Uso</a>
				<a href="politicapriv.php">Política de Privacidade</a>
				<a href="proint.php">Propriedade Intelectual</a>
			</div>
		</div>
	</footer>
	
</body>	
</html>