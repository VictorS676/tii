	<footer>
		<div class="links">
			<aside class="ld">
				<a href="cadastro.php">Cadastro</a><br/>
				<a href="login.php">Login</a><br/>
				<a href="torninst.php">Torne-se um instrutor</a><br/>
			</aside>
			<aside class="ld2">
				<a href="sobrenos.php">Sobre Nós</a><br/>
				<a href="carreiras.php">Carreiras</a><br/>
				<a href="noticias.php">Notícias</a><br/>
			</aside>
			<aside class="ld3">
				<a href="suporte.php">Suporte</a><br/>
				<a href="duvidas.php">Dúvidas</a><br/>
			</aside>
					<div class="middle">
						<a class="btn face" href="https://pt-br.facebook.com/">
							<i class="fab fa-facebook-f"></i>
						</a>
						<a class="btn twi" href="https://twitter.com/?lang=pt-br">
							<i class="fab fa-twitter"></i>
						</a>
						<a class="btn insta" href="https://www.instagram.com/?hl=pt-br">
							<i class="fab fa-instagram"></i>
						</a>
						<a class="btn goo" href="https://plus.google.com/discover">
							<i class="fab fa-google"></i>
						</a>
						<a class="btn ytb" href="https://www.youtube.com/?gl=BR">
							<i class="fab fa-youtube"></i>
						</a>
					</div>
		</div>
		<div class="dados">
			<div class="logo">
				<a href="index.php">
					<img src="imagens/fc.png" height="50px">
				</a>
			</div>
			<div class="direitos">
				<p>Copyright © 2018 Asriel, Inc.</p>
			</div>
			<div class="burocracia">
				<a href="termos.php">Termos de Uso</a>
				<a href="politicapriv.php">Política de Privacidade</a>
				<a href="propint.php">Propriedade Intelectual</a>
			</div>
		</div>
	</footer>
	<style type="text/css">
		
footer{
	overflow:hidden;
	position:relative;
	width:100%;
	height:280px;
	background-color:#FEFCFF;
	color:#148F77;
	-webkit-box-shadow: 0px 17px 48px 10px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 17px 48px 10px rgba(0,0,0,0.75);
	box-shadow: 0px 17px 48px 10px rgba(0,0,0,0.75);
	margin:0;
	padding:0;
	border-style: solid;
    border-width: 0px 0px 6px 0px;
	border-color:#45B39D;
}

footer a {
	color:#148F77;
	text-decoration:none;
	transition:0.4s;
}

footer a:hover{
	color:#117A65;
	font-weight:bold;
}

div.dados{
	position:relative;
	width:100%;
	margin-top:3.2%;
	height:100px;
    border-style: solid;
    border-width: 2px 0px 0px 0px;
	border-color:#E5E7E9;
}

.direitos{
	width:20%;
	height:100px;
}

.direitos p{
	margin-left:20%;
	margin-top:12.5%;
}

.burocracia{
	position:relative;
	width:40%;
	margin-left:59%;
	margin-top:-4.9%;
}

.burocracia a {
	padding-left:5%;
}

div.links{
	width:100%;
	height:140px;
	position:relative;
}

aside{
	margin-left:1%;
}

.ld{
	margin-top:2%;
	padding-left:2%;
	width:20%;
}

aside.ld2{
	margin-left:22.5%;
	width:20%;
	position:relative;
	top:-42.5%;
    border-style: solid;
    border-width: 0px 0px 0px 2px;
	border-color:#E5E7E9;
	padding-left:1%;
}

aside.ld3{
	margin-left:42.5%;
	width:20%;
	position:relative;
	top:-86.5%;
    border-style: solid;
    border-width: 0px 0px 0px 2px;
	border-color:#E5E7E9;
	padding-left:1%;
}

.middle i{
	color:#3498db;
}

.middle{
	position:relative;
	top:-100%;
	left:70%;
	transform:translateY(-50%);
	width:25%;
	text-align:center;
}

.btn{
	display:inline-block;
	width:45px;
	height:45px;
	background:#f1f1f1;
	margin:10px;
	border-radius:30%;
	box-shadow:0 5px 15px -5px #00000070;
	overflow:hidden;
	position:relative;
}

.btn i{
	line-height:45px;
	font-size:16px;
	transition:0.2s linear;
}

.btn:hover i{
	transform:scale(1.3);
	color:#f1f1f1;
}

.btn::before{
	content:"";
	position:absolute;
	width:120%;
	height:120%;
	background:#3498db;
	transform:rotate(45deg);
	left:-110%;
	top:90%;
}

.btn:hover::before{
	animation: aaa 0.7s 1;
	top:-10%;
	left:-10%;
}

.face i{
	color:#2471A3;
}

.face:hover i{
	color:#f1f1f1;
}

.face::before{
	background:#2471A3;
}

.goo i{
	color:#CB4335;
}

.goo:hover i{
	color:#f1f1f1;
}

.goo::before{
	background:#CB4335;
}

.ytb i{
	color:#FF0000;
}

.ytb:hover i{
	color:#f1f1f1;
}

.ytb::before{
	background:#FF0000;
}

.insta i{
	color:#7D3C98;
}

.insta:hover i{
	color:#f1f1f1;
}

.insta::before{
	background:linear-gradient(90deg, #03a9f4, #f441a5, #ffeb3b, #03a9f4);
}

.alunos h2{
	margin-left:35%;
	line-height:100px;
	font-size:32px;
}
	
	</style>