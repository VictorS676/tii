-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29-Nov-2018 às 02:54
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_curso`
--

CREATE TABLE `tb_curso` (
  `id_curso` int(11) NOT NULL,
  `nome_curso` varchar(60) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `instrutor_cod` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_curso`
--

INSERT INTO `tb_curso` (`id_curso`, `nome_curso`, `descricao`, `instrutor_cod`) VALUES
(1, 'Desenvolvimento de Games', 'Aprenda a programar jogos, conheça o curso de desenvolvimento de games um curso completo que chegou no mercado para que você domine a programação de jogos.', 1),
(2, 'Web Design', 'Com o curso, você aprenderá a utilizar as ferramentas para a criação e o desenvolvimento de websites. Você terá o conhecimento necessário para elaborar projetos e executar as atividades necessárias.', 2),
(3, 'Inglês', 'Em nossos cursos de inglês, os alunos desenvolvem suas habilidades comunicativas no novo idioma, encontram desafios e alcançam grandes conquistas.', 3),
(4, 'HTML', 'Com esse curso de HTML, você vai aprender a criar uma página web, utilizando e editando códigos em HTML.\r\nEsse curso de HTML foi desenvolvido para o ensino da criação e edição de páginas WEB.', 4),
(5, 'Javascript', 'Neste curso você irá aprender desde os primeiros conceitos em JavaScript como variáveis, condicionais e funções, até conceitos mais avançados como JavaScript assíncrono.', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_instrutor`
--

CREATE TABLE `tb_instrutor` (
  `id_inst` int(10) NOT NULL,
  `nome_inst` varchar(70) NOT NULL,
  `senha_inst` varchar(40) NOT NULL,
  `cpf_inst` int(18) NOT NULL,
  `id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuario`
--

CREATE TABLE `tb_usuario` (
  `id_usu` int(11) NOT NULL,
  `nome_usu` varchar(100) NOT NULL,
  `email_usu` varchar(150) NOT NULL,
  `senha_usu` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_usuario`
--

INSERT INTO `tb_usuario` (`id_usu`, `nome_usu`, `email_usu`, `senha_usu`) VALUES
(3, 'Matias Feliz PimentÃ£o', 'matiasfeliz21@outlook.com.br', '81dc9bdb52d04dc20036dbd8313ed055'),
(7, 'HolegÃ¡rio BeÃ§a', 'bencola@outlook.com', '81b073de9370ea873f548e31b8adc081'),
(12, 'Ana Luisa', 'ana.luisa99@outlook.com', '63ab910cb3a7bc89faae5a46aa337aa22f5f4d30'),
(13, 'Jorge Miguel', 'jm@gmail.com', '81b073de9370ea873f548e31b8adc081'),
(16, 'Leonardo Lima', 'limalimalima333@bol.com.br', '4607e782c4d86fd5364d7e4508bb10d9'),
(17, 'Asriel Dreemurr', 'asriel.1@gmail.com', '608f0b988db4a96066af7dd8870de96c'),
(18, 'Flowey The Flower', 'ftf12@hotmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(19, 'Luisa Silveira', 'lulu.sisi@bol.com', '0e9dc723f98cc86f4e1424243f99170b'),
(20, 'Mariana Lira', 'marilira099@terra.com.br', '0d1d5461bc549b37a09bc2dc49b14faf'),
(23, 'Thin Lizzy', 'thin.lizzy@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(24, 'Maria Tereza', 'mary.tere@gmail.com', '202cb962ac59075b964b07152d234b70'),
(25, 'Emilly Raphaella', 'emyrapha@outlook.com', 'e10adc3949ba59abbe56e057f20f883e'),
(27, 'Papyrus', 'papyrus.mata@hotmail.com', '61c50b8274c54acb2288fab0b97a91cf'),
(28, 'ValÃ©ria Pereira', 'val.depe@terra.com.br', 'c33367701511b4f6020ec61ded352059');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_curso`
--
ALTER TABLE `tb_curso`
  ADD PRIMARY KEY (`id_curso`);

--
-- Indexes for table `tb_instrutor`
--
ALTER TABLE `tb_instrutor`
  ADD PRIMARY KEY (`id_inst`);

--
-- Indexes for table `tb_usuario`
--
ALTER TABLE `tb_usuario`
  ADD PRIMARY KEY (`id_usu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_curso`
--
ALTER TABLE `tb_curso`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_instrutor`
--
ALTER TABLE `tb_instrutor`
  MODIFY `id_inst` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_usuario`
--
ALTER TABLE `tb_usuario`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
