	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
		<title>Asriel - Cadastro</title>
			<link rel="shorcut icon" href="imagens/a.png" />
<script type="text/javascript">
function valida_campos(){
	if(document.getElementById('nome_usu').value == ''){
		alert('Por favor, preencha os campos obrigatórios!');
		document.getElementById('nome_usu').focus();
		return false;
	}
	if(document.getElementById('email_usu').value == ''){
		alert('Por favor, preencha os campos obrigatórios!');
		document.getElementById('email_usu').focus();
		return false;
	}	
		if(document.getElementById('senha_usu').value == '' || document.getElementById('senha_usu').value != document.getElementById('senha2_usu').value){
		alert('As senhas não conferem, por favor, redigite!');
		document.getElementById('senha_usu').focus();
		return false;
	}
}
</script>
<body>
<?php

include ('menu.php');

?>
	<form action="crud/inserir.php" class="login-caixa" name="cadastro" method="post" autocomplete="off" onSubmit="return valida_campos();">
		<h1>Cadastro</h1>
			<div class="textocaixa">
				<i class="fas fa-user desce"></i>
					<input type="text" id="nome_usu" name="nome_usu" placeholder="Digite seu nome" class="cado" required="required">
			</div>
			<div class="textocaixa">
				<i class="fas fa-envelope desce"></i>
					<input type="email" id="email_usu" name="email_usu" placeholder="Digite seu email" class="cado" required="required">
			</div>
			<div class="textocaixa">
				<i class="fas fa-key desce"></i>
					<input type="password" id="senha_usu" name="senha_usu" placeholder="Escolha sua senha" class="cado" required="required">
			</div>
			<div class="textocaixa">
				<i class="fas fa-unlock desce"></i>
					<input type="password" id="senha2_usu" name="senha2_usu" placeholder="Redigite sua senha" class="cado" required="required">
			</div>
			<div class="ciente">
				<h5>Ao se cadastrar em nosso portal, você se declara ciente de nossos 
				<span href="termos.php">Termos de Uso</span> em conjunto com a nossa <span href="politicapriv.php">Política de Privacidade</span>.</h5>
			</div>
						<input type="submit" class="btnw" value="Cadastre-se"/>
	</form>
</body>	

<?php

include ('footer.php');

?>

<style type="text/css">
@import "https://use.fontawesome.com/releases/v5.5.0/css/all.css";

body{
	background-image: radial-gradient(circle, #052336, #002f58, #003a7b, #00439e, #1347be);
}

.login-caixa{
	width:32%;
	position:relative;
	top:60%;
	left:50%;
	transform:translate(-50%,-50%);
	color:#f1f1f1;
}

.login-caixa h1{
	float:left;
	font-size:40px;
	border-bottom:6px solid #3090C7;
	margin-bottom:50px;
	padding:13px 0;
}
	
.textocaixa{
	width:100%;
	overflow:hidden;
	font-size:20px;
	padding:10px 0;
	margin:8px;
	border-bottom:1px solid #3090C7;
}

.textocaixa i{
	width:26px;
	float:left;
	text-align:center;
}

.textocaixa input{
	border:none;
	outline:none;
	background:none;
	color:#f1f1f1;
	font-size:18px;
	width:80%;
	float:left;
	margin: 0 10px;
}

input::placeholder {
  color: #f1f1f1;
}

.btnw{
	width:100%;
	background:none;
	border:2px solid #3090C7;
	color:#3090C7;
	padding:5px;
	font-size:18px;
	cursor:poniter;
	margin:12px 0;
	transition:0.8s;
	margin-top:15%;
	border-radius:2px;
	margin-bottom:10%;
}

.btnw:hover{
	background-color:#46C7C7;
	border:2px solid #46C7C7;	
	color:#fff;
}

input.cado{ 
	margin-top:5%;
	}

.desce{
	margin-top:5%;
	}
	
	h5{
		text-indent:5%;
	}
	
	span{
		color:#46C7C7;
	}
</style>