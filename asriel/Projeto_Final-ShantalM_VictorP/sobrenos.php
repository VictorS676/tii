	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
		<title>Sobre Nós do Asriel</title>
			<link rel="shorcut icon" href="imagens/a.png"/>
<body>
<?php
include ('menu.php');
?>

<div class="team-section">
	<h1>Sobre Nós</h1>
	<span class="border"></span>
	<div class="ps">
		<a href="#p1"><img src="imagens/asrielperfil.jpg" alt=""></a>
		<a href="#p2"><img src="imagens/floweyperfil.jpg" alt=""></a>
	</div>
</div>

<div class="section" id="p1">
	<span class="name">Asriel Dreemurr</span>
	<span class="border"></span>	
	<p>
	Asriel Dreemurr é o filho biológico de Toriel e Asgore, e também é o irmão adotivo do/a Primeiro Humano Caído. Ele é a forma original de Flowey, príncipe do Underground, e o Boss Final da Rota Pacifista Verdadeira.
	</p>
</div>

<div class="section" id="p2">
	<span class="name">Flowey the flower</span>
	<span class="border"></span>	
	<p> Flowey é o primeiro personagem apresentado e o antagonista da rota Neutra e Pacifista que existem em Undertale. Ele introduz as mecânicas de encontros compartilhando com você "pétalas da amizade", que na verdade são projéteis que causam dano.
	</p>
</div>

<style type="text/css">

*{
	margin:0;
	padding:0;
	font-family:sans-serif;
}

.team-section{
	overflow:hidden;
	text-align:center;
	padding:60px;
}

.team-section h1{
	text-transform:uppercase;
	margin-bottom:60px;
	color:#006064;
	font-size:40px;
}

.border{
	display:block;
	margin:auto;
	width:160px;
	height:3px;
	background:#006064;
	margin-bottom:40px;
}

.ps{
	margin-bottom:40px;
}

.ps a{
	display:inline-block;
	margin:0 30px;
	width:160px;
	height:160px;
	overflow:hidden;
	border-radius:50%;
}

.ps a img{
	width:100%;
}

.section{
	padding-top:5%;
	width:600px;
	margin:auto;
	font-size:20px;
	color:#000;
	text-align:justify;
	height:0;
	overflow:hidden;
}

.section:target{
	height:auto;
}

.name{
	display:block;
	margin-bottom:30px;
	text-align:center;
	text-transform:uppercase;
	font-size:22px;
	color:#006064;
}

</style>
<?php
include('footer.php');
?>	