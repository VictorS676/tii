<!-- <!DOCTYPE html> -->
<html lang="pt-br">
	<head>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"/>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Asriel: Protótipo de plataforma para cursos online, Versão: Alpha 1.0" />
		<meta name="keywords" content="Asriel, desenvolvimento de jogos, cursos, alunos, instrutores, tempo, aulas, online" />
		<meta name="author" content="Shantal de Morais Mantovani & Victor Pedro de Sousa" />
		<meta name="generator" content="Notepad++"/>
			<link rel="shortcut icon" type="image/x-icon" href="imagens/"/>
			<link rel="shorcut icon" href="imagens/a.png" />			
			<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
				<title>Asriel - Termos de Uso</title>
	</head>		
<body>
	<header>
		<nav>
			<div class="menu">
				<div class="logo">
					<a href="index.php">
						<img src="imagens/fc.png" height="50px">
						<p class="text2">Asriel</p>
					</a>
				</div>
				<div class="cat_drop">
					<div class="dropdown">
						<button class="dropbtn"><i class="fas fa-th"></i> Categorias</button>		
						<div class="dropdown-conteudo">
							<a href="desengames.php">Desenvolvimento de Games</a>
							<a href="webdesign.php">Web Design</a>
							<a href="ingles.php">Inglês</a>
							<a href="html.php">HTML</a>
							<a href="javascript.php">Javascript</a>
						</div> 
					</div>	
				</div>
				<div class="busca">
					<form class="buscando" name="busque" action="crud/select.php" method="post">
						<input type="text" name="buscar" id="buscar" class="buscar" placeholder="Buscar cursos"></input>
							<button class="busca" type="submit"><i class="fas fa-search"></i></button>
					</form>
				</div>
				<div class="torn_inst">
					<a href="inst_cadastro.php" class="inst_cad">Torne-se um instrutor</a>
				</div>
				<div class="logon_cadastro">
					<a href="cadastro.php" class="log_cad">Cadastro</a>
				</div>	
				<div class="logi">
					<a href="login.php" class="log_cad2">Login</a>
				</div>
			</div>
		</nav>
		<section class="banner">
			<img src="imagens/banner6.png" width="100%"/>
			<img src="imagens/banner6.png" width="100%"/>
				<div class="msg">
					<h1><i>Tudo é possível</i></h1>
					<p>Construa seu futuro com os melhores cursos e certificações on-line do Brasil.</p>			
				</div>
				<div class="info">
					<h4><i class="far fa-address-book"></i> 80.000 visitantes anuais</h4>
						<p>Conheça uma Variedade de temas atuais.</p>
					<h4><i class="fas fa-briefcase"></i> Especialistas do Setor</h4>
						<p>Encontre o professor ideal para seu ritmo.</p>	
					<h4><i class="fas fa-user-clock"></i> Acesso Vitalício</h4>
						<p>Estude no seu ritmo do seu jeito.</p>				
				</div>
		</section>
	</header>
	<main>
		<section class="pp">
			<h2>Termos de Uso para Asriel Cursos</h2>
				<p>Todas as suas informações pessoais recolhidas, serão usadas para o ajudar a tornar a sua visita no nosso site o mais produtiva e agradável possível.</p><p>A garantia da confidencialidade dos dados pessoais dos utilizadores do nosso site é importante para o Asriel Cursos.</p><p>Todas as informações pessoais relativas a membros, assinantes, clientes ou visitantes que usem o Asriel Cursos serão tratadas em concordância com a Lei da Proteção de Dados Pessoais de 26 de outubro de 1998 (Lei n.º 67/98).</p>
				<p>A informação pessoal recolhida pode incluir o seu nome, e-mail, número de telefone e/ou telemóvel, morada, data de nascimento e/ou outros.</p><p>O uso do Asriel Cursos pressupõe a aceitação deste Acordo de privacidade. A equipa do Asriel Cursos reserva-se ao direito de alterar este acordo sem aviso prévio. Deste modo, recomendamos que consulte a nossa política de privacidade com regularidade de forma a estar sempre atualizado.</p>
			<h2>Os anúncios</h2>
				<p>Tal como outros websites, coletamos e utilizamos informação contida nos anúncios. A informação contida nos anúncios, inclui o seu endereço IP (Internet Protocol), o seu ISP (Internet Service Provider, como o Sapo, Clix, ou outro), o browser que utilizou ao visitar o nosso website (como o Internet Explorer ou o Firefox), o tempo da sua visita e que páginas visitou dentro do nosso website.</p>
			<h2>Cookie DoubleClick Dart</h2>
				<p>O Google, como fornecedor de terceiros, utiliza cookies para exibir anúncios no nosso website;</p>
				<p>Com o cookie DART, o Google pode exibir anúncios com base nas visitas que o leitor fez a outros websites na Internet;</p>
			<h2>Os Cookies e Web Beacons</h2>
				<p>Utilizamos cookies para armazenar informação, tais como as suas preferências pessoas quando visita o nosso website. Isto poderá incluir um simples popup, ou uma ligação em vários serviços que providenciamos, tais como fóruns.</p>
				<p>Em adição também utilizamos publicidade de terceiros no nosso website para suportar os custos de manutenção. Alguns destes publicitários, poderão utilizar tecnologias como os cookies e/ou web beacons quando publicitam no nosso website, o que fará com que esses publicitários (como o Google através do Google AdSense) também recebam a sua informação pessoal, como o endereço IP, o seu ISP, o seu browser, etc. Esta função é geralmente utilizada para geotargeting (mostrar publicidade de Lisboa apenas aos leitores oriundos de Lisboa por ex.) ou apresentar publicidade direcionada a um tipo de utilizador (como mostrar publicidade de restaurante a um utilizador que visita sites de culinária regularmente, por ex.).</p>
				<p>Você detém o poder de desligar os seus cookies, nas opções do seu browser, ou efetuando alterações nas ferramentas de programas Anti-Virus, como o Norton Internet Security. No entanto, isso poderá alterar a forma como interage com o nosso website, ou outros websites. Isso poderá afetar ou não permitir que faça logins em programas, sites ou fóruns da nossa e de outras redes.</p>
			<h2>Ligações a Sites de terceiros</h2>
				<p>O Asriel Cursos possui ligações para outros sites, os quais, a nosso ver, podem conter informações / ferramentas úteis para os nossos visitantes. A nossa política de privacidade não é aplicada a sites de terceiros, pelo que, caso visite outro site a partir do nosso deverá ler a politica de privacidade do mesmo.</p>
				<p>Não nos responsabilizamos pela política de privacidade ou conteúdo presente nesses mesmos sites.</p>
		</section>
			<a class="gotopbtn" href="#">
				<i class="fas fa-arrow-up"></i>
			</a>
	</main>	
	
<?php 
include('footer.php');
?>