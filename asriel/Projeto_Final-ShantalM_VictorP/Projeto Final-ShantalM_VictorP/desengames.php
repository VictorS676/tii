<title>Desenvolvimento de Games</title>
<?php 
include('menu.php');
if(isset($_SESSION['nome_usu_sessao'])){
?>
<section class="test">
	<h1>Aulas de Desenvolvimento de Games</h1>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Zoeo88XG8pM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br/><br/>
<iframe width="560" height="315" src="https://www.youtube.com/embed/1JA12I_uUSw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br/><br/>
<iframe width="560" height="315" src="https://www.youtube.com/embed/BAPrBqEENdc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br/><br/>
    <h3>Mais aulas serão postadas ao longo do tempo, não se preocupe...</h3>
</section>
	<style type="text/css">

	section.test{
		width:60%;
		position:relative;
        padding-top:12.5%;
		margin-left:30%;
	}
	
	h1{
		margin-left:-30%;
		text-align:center;
		border:5px solid #339999;
		border-width:0px 0px 5px 0px;
		font-size:32px;
		margin-bottom:10%;
		padding-bottom:3.5%;
	}
	
	iframe{
		padding-bottom:8%;
	}
	
	h3{
		padding-bottom:8%;
		margin-left:2%;		
        color:#000033 ;		
	}
    </style>	
<?php
}else{
?>
<div class="fundo">
<div class="ops">
	<h1>Ops...</h1>
		<h3>Esta é uma área restrita, por favor, efetue <span>Login</span> ou <span>Cadastre-se</span>.</h3>
	<img src="imagens/b.gif"/>
</div>	
</div>
	<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Press+Start+2P');
	
	div.fundo{
		position:relative;
		width:100%;
		background:rgb(20,20,20);
	}
	
	div.ops{
		position:relative;
		margin-top:4%;
		margin-bottom:4%;
		margin-left:25%;
		margin-right:25%;
	}
	
	.ops h1{
		font-family: 'Press Start 2P', cursive;
		text-align:center;
		font-size:32px;
		color:#009999;
	}
	
	.ops h3{
		font-size:14px;
		text-indent:3%;
		color:#f1f1f1;
		font-family: 'Press Start 2P', cursive;
	}
	
	.ops span{
		color:#009999;
	}
	
	.ops img{
		margin-left:25%;
		margin-top:3%;
		width:40%;
	}
	</style>
<?php
}

include('footer.php');
?>