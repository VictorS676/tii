<footer>
		<div class="links">
			<aside class="ld">
				<a href="cadastro.php">Cadastro</a><br/>
				<a href="login.php">Login</a><br/>
				<a href="inst_cadastro.php">Torne-se um instrutor</a><br/>
			</aside>
			<aside class="ld2">
				<a href="sobrenos.php">Sobre Nós</a><br/>
				<a href="noticias.php">Notícias</a><br/>
			</aside>
		</div>
		<div class="dados">
			<div class="logo">
				<a href="index.php">
					<img src="imagens/fc.png" height="50px">
				</a>
			</div>
			<div class="direitos">
				<p>Copyright © 2018 Asriel, Inc.</p>
			</div>
			<div class="burocracia">
				<a href="termos.php">Termos de Uso</a>
				<a href="politicapriv.php">Política de Privacidade</a>
				<a href="propint.php">Propriedade Intelectual</a>
			</div>
		</div>
	</footer>
	<style type="text/css">
		
footer{
	overflow:hidden;
	position:relative;
	width:100%;
	height:280px;
	background-color:#FEFCFF;
	color:#148F77;
	-webkit-box-shadow: 0px 17px 48px 10px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 17px 48px 10px rgba(0,0,0,0.75);
	box-shadow: 0px 17px 48px 10px rgba(0,0,0,0.75);
	margin:0;
	padding:0;
	border-style: solid;
    border-width: 0px 0px 6px 0px;
	border-color:#45B39D;
}

footer a {
	color:#148F77;
	text-decoration:none;
	transition:0.4s;
}

footer a:hover{
	color:#117A65;
	font-weight:bold;
}

div.dados{
	position:relative;
	width:100%;
	margin-top:3.2%;
	height:100px;
    border-style: solid;
    border-width: 2px 0px 0px 0px;
	border-color:#E5E7E9;
}

.direitos{
	width:20%;
	height:100px;
}

.direitos p{
	margin-left:20%;
	margin-top:12.5%;
}

.burocracia{
	position:relative;
	width:40%;
	margin-left:59%;
	margin-top:-4.9%;
}

.burocracia a {
	padding-left:5%;
}

div.links{
	width:100%;
	height:140px;
	position:relative;
}

aside{
	margin-left:1%;
}

.ld{
	margin-top:2%;
	padding-left:2%;
	width:20%;
}

aside.ld2{
	margin-left:22.5%;
	width:20%;
	position:relative;
	top:-42.5%;
    border-style: solid;
    border-width: 0px 0px 0px 2px;
	border-color:#E5E7E9;
	padding-left:1%;
}

aside.ld3{
	margin-left:42.5%;
	width:20%;
	position:relative;
	top:-86.5%;
    border-style: solid;
    border-width: 0px 0px 0px 2px;
	border-color:#E5E7E9;
	padding-left:1%;
}
	
	</style>