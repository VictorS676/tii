	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
			<title>Asriel - Login</title>
				<link rel="shorcut icon" href="imagens/a.png" />
<body>
<?php

include ('menu.php');

?>

	<form class="login-caixa" action="valida.php" method="post">
		<h1>Login</h1>
			<div class="textocaixa">
				<i class="far fa-user"></i>
				<input type="text" id="email_usu" name="email_usu" placeholder="E-Mail" class="" required="required">
			</div>
			<div class="textocaixa">
				<i class="fas fa-fingerprint"></i>
				<input type="password" id="senha_usu" name="senha_usu" placeholder="Senha" class="" required="required">
			</div>
				<button type="submit" name="login" class="btnw">Entrar</button>			
	</form>

</body>	

<?php

include ('footer.php');

?>

<style type="text/css">
@import "https://use.fontawesome.com/releases/v5.5.0/css/all.css";

body{
	background-image: radial-gradient(circle, #052336, #002f58, #003a7b, #00439e, #1347be);
}

.login-caixa{
	width:32%;
	position:relative;
	top:50%;
	left:50%;
	transform:translate(-50%,-50%);
	color:#f1f1f1;
}

.login-caixa h1{
	float:left;
	font-size:40px;
	border-bottom:6px solid #3090C7;
	margin-bottom:50px;
	padding:13px 0;
}
	
.textocaixa{
	width:100%;
	overflow:hidden;
	font-size:20px;
	padding:10px 0;
	margin:8px;
	border-bottom:1px solid #3090C7;
}

input::placeholder {
  color: #f1f1f1;
}

.textocaixa i{
	width:26px;
	float:left;
	text-align:center;
}

.textocaixa input{
	border:none;
	outline:none;
	background:none;
	color:#f1f1f1;
	font-size:18px;
	width:80%;
	float:left;
	margin: 0 10px;
}

.btnw{
	width:100%;
	background:none;
	border:2px solid #3090C7;
	color:#3090C7;
	padding:5px;
	font-size:18px;
	cursor:poniter;
	margin:12px 0;
	transition:0.8s;
	margin-top:15%;
	border-radius:2px;
	margin-bottom:25%;
}

.btnw:hover{
	background-color:#46C7C7;
	border:2px solid #46C7C7;	
	color:#fff;
}

input.cado{ 
	margin-top:5%;
	}

.desce{
	margin-top:5%;
	}
	
</style>