-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 30-Nov-2018 às 15:13
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE `cursos` (
  `id` int(11) NOT NULL,
  `nome` varchar(220) NOT NULL,
  `detalhes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cursos`
--

INSERT INTO `cursos` (`id`, `nome`, `detalhes`) VALUES
(1, 'Desenvolvimento de Games', 'Aprenda a programar jogos, conheÃ§a o curso de desenvolvimento de games um curso completo que chegou no mercado para que vocÃª domine a programaÃ§Ã£o de jogos.'),
(2, 'Web Design', 'Com o curso, vocÃª aprenderÃ¡ a utilizar as ferramentas para a criaÃ§Ã£o e o desenvolvimento de websites. VocÃª terÃ¡ o conhecimento necessÃ¡rio para elaborar projetos e executar as atividades exigidas no mercado de trabalho.'),
(3, 'InglÃªs', 'Em nossos cursos de inglÃªs, os alunos desenvolvem suas habilidades comunicativas no novo idioma, encontram desafios e alcanÃ§am grandes conquistas.'),
(4, 'HTML', 'Com esse curso de HTML, vocÃª vai aprender a criar uma pÃ¡gina web, utilizando e editando cÃ³digos em HTML.\r\nEsse curso de HTML foi desenvolvido para o ensino da criaÃ§Ã£o e ediÃ§Ã£o de pÃ¡ginas WEB.'),
(5, 'Javascript', 'Neste curso vocÃª irÃ¡ aprender desde os primeiros conceitos em JavaScript como variÃ¡veis, condicionais e funÃ§Ãµes, atÃ© os conceitos mais avanÃ§ados como JavaScript assÃ­ncrono.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `duvidas`
--

CREATE TABLE `duvidas` (
  `id_duv` int(11) NOT NULL,
  `email_visit` varchar(150) NOT NULL,
  `comentario` varchar(220) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `duvidas`
--

INSERT INTO `duvidas` (`id_duv`, `email_visit`, `comentario`) VALUES
(1, 'reinaldo.oprincedopagode@gmail.com', 'Adorei o site de vocÃªs, achei realmente incrÃ­vel.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_instrutor`
--

CREATE TABLE `tb_instrutor` (
  `id_inst` int(10) NOT NULL,
  `nome_inst` varchar(70) NOT NULL,
  `senha_inst` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_instrutor`
--

INSERT INTO `tb_instrutor` (`id_inst`, `nome_inst`, `senha_inst`) VALUES
(3, 'Carol', 'e2fc714c4727ee9395f324cd2e7f331f'),
(5, 'Lourenso', 'd4b2758da0205c1e0aa9512cd188002a'),
(6, 'Guga', '4000e3230d36dda670dc6a0d71ae2d95'),
(8, 'Tadeu', 'd9729feb74992cc3482b350163a1a010'),
(9, 'Maria', '3d8022861f6e74908c249306d39b6a93');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuario`
--

CREATE TABLE `tb_usuario` (
  `id_usu` int(11) NOT NULL,
  `nome_usu` varchar(100) NOT NULL,
  `email_usu` varchar(150) NOT NULL,
  `senha_usu` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_usuario`
--

INSERT INTO `tb_usuario` (`id_usu`, `nome_usu`, `email_usu`, `senha_usu`) VALUES
(3, 'Matias Feliz PimentÃ£o', 'matiasfeliz21@outlook.com.br', '81dc9bdb52d04dc20036dbd8313ed055'),
(7, 'HolegÃ¡rio BeÃ§a', 'bencola@outlook.com', '81b073de9370ea873f548e31b8adc081'),
(12, 'Ana Luisa', 'ana.luisa99@outlook.com', '63ab910cb3a7bc89faae5a46aa337aa22f5f4d30'),
(13, 'Jorge Miguel', 'jm@gmail.com', '81b073de9370ea873f548e31b8adc081'),
(16, 'Leonardo Lima', 'limalimalima333@bol.com.br', '4607e782c4d86fd5364d7e4508bb10d9'),
(17, 'Asriel Dreemurr', 'asriel.1@gmail.com', '608f0b988db4a96066af7dd8870de96c'),
(18, 'Flowey The Flower', 'ftf12@hotmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(19, 'Luisa Silveira', 'lulu.sisi@bol.com', '0e9dc723f98cc86f4e1424243f99170b'),
(20, 'Mariana Lira', 'marilira099@terra.com.br', '0d1d5461bc549b37a09bc2dc49b14faf'),
(23, 'Thin Lizzy', 'thin.lizzy@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(24, 'Maria Tereza', 'mary.tere@gmail.com', '202cb962ac59075b964b07152d234b70'),
(25, 'Emilly Raphaella', 'emyrapha@outlook.com', 'e10adc3949ba59abbe56e057f20f883e'),
(27, 'Papyrus', 'papyrus.mata@hotmail.com', '61c50b8274c54acb2288fab0b97a91cf'),
(28, 'ValÃ©ria Pereira', 'val.depe@terra.com.br', 'c33367701511b4f6020ec61ded352059'),
(29, 'Pedro Miguel', 'pmmp@gmail.com', 'd8578edf8458ce06fbc5bb76a58c5ca4'),
(30, 'Eusebio Banies', 'banedani@terra.uol.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(31, 'Ricardo Silva', 'silvinha.silvao@hotmail.com', '29dd1d84bda76c90c79abf9db5801c8a'),
(32, 'Marina Ruy Barbosa', 'marina1@gmail.com', '0c247b21f30f9ddf190eddea3d109e48'),
(33, 'Marina Silva', 'et@extraterrestre.com.br', '0de66efad5f6df14a5f160d7a6250d54'),
(34, 'Enzzo Vallentthiynno', 'enzzo.vallentthiynno@hotlook.com', 'e3f5c287696333b41bdff72dc48d4054'),
(35, 'Abigail Bresley', 'aa@gmail.com', 'fcc2eef4e6e41d1f93a9aff75a5456ab'),
(40, 'Adriane Ribeiro', 'dri.ribeiro422@hotmail.com', 'a8698009bce6d1b8c2128eddefc25aad'),
(47, 'Philipe Matta', 'PhilipOMatta@dayrep.com', '150920ccedc34d24031cdd3711e43310'),
(48, 'Carlos Fernandes', 'CarltonGFernandez@teleworm.us', '3f9595aff804894be2600b3de2856ea4'),
(51, 'Clara Costa Fernandes', 'ClaraCostaFernandes@jourrapide.com', 'bcb759b5b8ab63b06295c7434345d7a5'),
(52, 'Gabriel Silva Melo', 'GabrielSilvaMelo@armyspy.com', '6eea9b7ef19179a06954edd0f6c05ceb'),
(53, 'Mariana Pherrera', 'Marianernandes@dayrep.com', 'e807f1fcf82d132f9bb018ca6738a19f'),
(54, 'Breno Melo', 'BrenoMelo@armyspy.com', '6cf82ee1020caef069e753c67a97a70d'),
(55, 'Livia Oliveira Cardoso', 'LiviaOliveiraCardoso@rhyta.com', '1d4bbcfed31c6e01e90d8e4099e39eb7'),
(56, 'Roberto Alegretti', 'rr.soarez@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(58, 'Manuela Castro Gomes', 'ManuelaCastroGomes@armyspy.com', 'fb62579e990da4e2a8f15c3d1e123438'),
(59, 'Julian Barros Costa', 'JulianBarrosCosta@rhyta.com', '3f7c4756e067384eb19e87dcff2baec9'),
(60, 'Luana Oliveira Correia', 'LuanaOliveiraCorreia@armyspy.com', 'bc9d211c4c9f6cc0aed9486cedfa8b91'),
(62, 'Vitor Silva', 'vitor.silva899@hotlook.com', 'f1aa3cffcc247ad7f86dac8157b41230'),
(63, 'Divino Salvador Dos Anjos', 'Deus@xn--cu-bja.com', 'e9829608dd90ff6b8bf7cb50746eae78'),
(64, 'Vinicius Silva', 'vini.pds@hotmail.com', '18b811fcd47ef4cc98e366558b20e799'),
(65, 'Joao Cesar', 'joo.cesar@gmail.com', 'ea515ae83f8dcd82df7b72cb285ebcda'),
(66, 'Shantal De Morais', 'ss@gmail.com', '66f16a42e39c657e14e347c7171cc1e3'),
(67, 'Renato Mattos', 'renatinho@hotmail.com', 'adcaec3805aa912c0d0b14a81bedb6ff');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `duvidas`
--
ALTER TABLE `duvidas`
  ADD PRIMARY KEY (`id_duv`);

--
-- Indexes for table `tb_instrutor`
--
ALTER TABLE `tb_instrutor`
  ADD PRIMARY KEY (`id_inst`);

--
-- Indexes for table `tb_usuario`
--
ALTER TABLE `tb_usuario`
  ADD PRIMARY KEY (`id_usu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `duvidas`
--
ALTER TABLE `duvidas`
  MODIFY `id_duv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_instrutor`
--
ALTER TABLE `tb_instrutor`
  MODIFY `id_inst` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_usuario`
--
ALTER TABLE `tb_usuario`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
