<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
		<title>Cadastro de Instrutor</title>
			<link rel="shorcut icon" href="imagens/a.png"/>
			<script type="text/javascript">
function valida_campos(){
	if(document.cadastro.getElementById('nome_inst').value == ''){
		alert('Por favor, preencha os campos obrigatórios!');
		document.cadastro.getElementById('nome_inst').focus();
		return false;
	}	
		if(document.cadastro.getElementById('senha_usu').value == '' || document.cadastro.getElementById('senha_inst').value != document.cadastro.getElementById('senha2_inst').value){
		alert('As senhas não conferem, por favor, redigite!');
		document.cadastro.getElementById('senha_inst').focus();
		return false;
	}
}
</script>
<body>
<?php
include ('menu.php');
?>
<section class="banner">
			<img src="imagens/banner6.png" width="100%"/>
			<img src="imagens/banner6.png" width="100%"/>
				<div class="msg">
					<h1><i>Tudo é possível</i></h1>
					<p>Construa seu futuro com os melhores cursos e certificações on-line do Brasil.</p>			
				</div>
				<div class="info">
					<h4><i class="far fa-address-book"></i> 80.000 visitantes anuais</h4>
						<p>Conheça uma Variedade de temas atuais.</p>
					<h4><i class="fas fa-briefcase"></i> Especialistas do Setor</h4>
						<p>Encontre o professor ideal para seu ritmo.</p>	
					<h4><i class="fas fa-user-clock"></i> Acesso Vitalício</h4>
						<p>Estude no seu ritmo do seu jeito.</p>				
				</div>
</section>
<section class="potencial">
<h2>Descubra seu potencial</h2>
<section class="breve">
			<div class="servicos">
				<div class="cen">
					<div class="servico">
						<i class="fas fa-piggy-bank"></i>
							<h2>Ganhe dinheiro</h2>
							<p>Ganhe dinheiro sempre que um aluno comprar seu curso. Receba o pagamento via PayPal ou Payoneer. A escolha é sua.</p>
					</div>
					<div class="servico">
					    <i class="fas fa-play"></i>
							<h2>Inspire alunos</h2>
							<p>Compartilhe seu conhecimento e ajude as pessoas a aprender novas habilidades, melhorar suas carreiras e explorar seus hobbies.
							</p>				
					</div>
					<div class="servico">
						<i class="fas fa-thumbs-up"></i>
							<h2>Participe da nossa comunidade</h2>
							<p>Conte com a ajuda da nossa comunidade ativa de instrutores em todo o processo de criação do seu curso.
							</p>					
					</div>
				</div>
			</div>
		</section>
</section>
<form action="crud/inserir2.php" class="login-caixa" name="cadastro" method="post" autocomplete="off" onSubmit="return valida_campos();">
		<h1>Cadastro</h1>
			<div class="textocaixa">
				<i class="fas fa-user desce"></i>
					<input type="text" id="nome_inst" name="nome_inst" placeholder="Digite seu nome" class="cado" required="required">
			</div>
			<div class="textocaixa">
				<i class="fas fa-key desce"></i>
					<input type="password" id="senha_inst" name="senha_inst" placeholder="Escolha sua senha" class="cado" required="required">
			</div>
			<div class="textocaixa">
				<i class="fas fa-unlock desce"></i>
					<input type="password" id="senha2_inst" name="senha2_inst" placeholder="Redigite sua senha" class="cado" required="required">
			</div>
			<div class="ciente">
				<h5>Ao se cadastrar em nosso portal, você se declara ciente de nossos 
				<span href="termos.php">Termos de Uso</span> em conjunto com a nossa <span href="politicapriv.php">Política de Privacidade</span>.</h5>
			</div>
						<input type="submit" class="btnw" value="Cadastre-se"/>
	</form>
<section class="alunos">
			<h2>Amplie seu alcance</h2>
			<h4>Nós mudamos vidas conectando instrutores com alunos ao redor do mundo.</h4>
			<div class="box">
				<img src="imagens/perf6.jpg" alt="" class="box-img">
					<h1>Guto Neijain</h1>
					<h5>Desenvolvimento de Games</h5>
						<p>Toda essa história de passar conhecimento através de vídeo aulas surgiu por eu querer que pessoas com os mesmos interesses não tivessem que passar pelas mesmas dificuldades que eu passei.</p>
			</div>
			<div class="box">
				<img src="imagens/perf4.png" alt="" class="box-img">
					<h1>Akira Notiyama</h1>
					<h5>Programação</h5>
						<p>Mesmo sendo um dos maiores autores de videoaulas, comecei a usar a plataforma do Asriel para atingir principalmente meu público fora do Brasil e em menos de 3 meses consegui mais de 2000 alunos em mais de 20 países diferentes.</p>
			</div>
			<div class="box">
				<img src="imagens/perf5.jpg" alt="" class="box-img">
					<h1>Diego Davila</h1>
					<h5>Publicidade</h5>
						<p>Ensinando no Asriel os instrutores são donos do seu próprio tempo e você trabalha com o que realmente ama.</p>
			</div>
		</section>
		<section class="lgo">
	<form class="login-caixa" action="crud/valida2.php" method="post">
		<h1>Login</h1>
			<div class="textocaixa">
				<i class="far fa-user"></i>
				<input type="text" id="nome_inst" name="nome_inst" placeholder="Digite seu nome instrutor" class="" required="required">
			</div>
			<div class="textocaixa">
				<i class="fas fa-fingerprint"></i>
				<input type="password" id="senha_inst" name="senha_inst" placeholder="Senha" class="" required="required">
			</div>
				<button type="submit" name="login" class="btnw">Entrar</button>			
	</form>
	</section>
<style type="text/css">

section.potencial{
	width:100%;
}

div.servicos{
	background-color:#fff;
}

.potencial h2{
	text-align:center;
	font-size:28px;
	color:rgb(30,30,30);
}

.login-caixa{
	width:32%;
	position:relative;
	top:60%;
	left:50%;
	transform:translate(-50%,-50%);
	color:rgb(30,30,30);
	margin-top:10%;
}

.login-caixa h1{
	float:left;
	font-size:40px;
	border-bottom:6px solid #3090C7;
	margin-bottom:50px;
	padding:13px 0;
}
	
.textocaixa{
	width:100%;
	overflow:hidden;
	font-size:20px;
	padding:10px 0;
	margin:8px;
	border-bottom:1px solid #3090C7;
}

.textocaixa i{
	width:26px;
	float:left;
	text-align:center;
}

.textocaixa input{
	border:none;
	outline:none;
	background:none;
	color:rgb(30,30,30);
	font-size:18px;
	width:80%;
	float:left;
	margin: 0 10px;
}

input::placeholder {
  color: rgb(30,30,30);
}

.btnw{
	width:100%;
	background:none;
	border:2px solid #3090C7;
	color:#3090C7;
	padding:5px;
	font-size:18px;
	cursor:poniter;
	margin:12px 0;
	transition:0.8s;
	margin-top:15%;
	border-radius:2px;
	margin-bottom:10%;
}

.btnw:hover{
	background-color:#46C7C7;
	border:2px solid #46C7C7;	
	color:#fff;
}

input.cado{ 
	margin-top:5%;
	}

.desce{
	margin-top:5%;
	}
	
	h5{
		text-indent:5%;
	}
	
	span{
		color:#46C7C7;
	}
	
	.alunos h2{
padding-left:5%;
	}
	
	h4{
		margin-left:32.5%;
	}
	
	section.lgo{
		margin-top:25%;
	}
</style>
<?php
include('footer.php');
?>	