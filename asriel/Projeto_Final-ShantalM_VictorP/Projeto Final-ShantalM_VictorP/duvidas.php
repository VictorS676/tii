<!-- <!DOCTYPE html> -->
<html lang="pt-br">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Asriel: Protótipo de plataforma para cursos online, Versão: Alpha 1.0" />
		<meta name="keywords" content="Asriel, desenvolvimento de jogos, cursos, alunos, instrutores, tempo, aulas, online" />
		<meta name="author" content="Shantal de Morais Mantovani & Victor Pedro de Sousa" />
		<meta name="generator" content="Notepad++"/>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"/>
			<link rel="shorcut icon" href="imagens/a.png" />	
			<link rel="stylesheet" type="text/css" href="css/estilizando.css"/>
				<title>Asriel - Dúvidas</title>
				<script type=>
function mostrarResultado(box,num_max,campospan){
	var contagem_carac = box.length;
	if (contagem_carac != 0){
		document.getElementById(campospan).innerHTML = contagem_carac + " caracteres digitados";
		if (contagem_carac == 1){
			document.getElementById(campospan).innerHTML = contagem_carac + " caracter digitado";
		}
		if (contagem_carac >= num_max){
			document.getElementById(campospan).innerHTML = "Limite de caracteres excedido!";
		}
	}else{
		document.getElementById(campospan).innerHTML = "Ainda não temos nada digitado..";
	}
}

function valida_campos(){
	if(document.getElementById('email_visit').value == ''){
		alert('Por favor, preencha os campos obrigatórios!');
		document.getElementById('email_visit').focus();
		return false;
	}
	if(document.getElementById('comentario').value == ''){
		alert('Por favor, preencha os campos obrigatórios!');
		document.getElementById('comentario').focus();
		return false;
	}	
}

				</script>
	</head>		
<body>
<?php 
include('menu.php');
?>

<section class="ajuda">
		<form class="login-caixa" action="crud/pergunta.php" method="post" onSubmit="return valida_campos();">
		<h1>Dúvidas</h1>
			<div class="textocaixa">
				<i class="fas fa-question-circle"></i>
				<input type="email" id="email_visit" name="email_visit" placeholder="Insira seu e-mail para resposta" class="" required="required">
			</div>
			<div class="textocaixa">
				<i class="fas fa-comments"></i>
				<textarea id="comentario" name="comentario" placeholder="Escreva sua dúvida ou comentário aqui e prontamente responderemos suas questões." onkeyup="mostrarResultado(this.value,220,'spcontando');contarCaracteres(this.value,220,'sprestante')" rows="12" cols="50" maxlength="220"></textarea>
			<span id="spcontando" style="font-family:'Source Sans Pro', sans-serif;font-size:12px;">Ainda não temos nada digitado..</span><br />
<span id="sprestante" style="font-family:'Source Sans Pro', sans-serif;font-size:12px;"></span>
			</div>
				<button type="submit" name="manda" class="btnw">Enviar</button>			
	</form>
</section>	
	<div class="desc">
		<h1>FAQ DE PERGUNTAS FREQUENTES</h1>
<h3>Quanto eu posso ganhar se for professor?</h3>
<p>Oferecemos taxas de comissão muito competitivas! Quanto mais você ensinar e tiver um feedback positivo, mais você irá ganhar</p>

<h3>O que haverá no meu perfil?</h3>
<p>Isso depende de você! Você pode escolher qualquer um dos nossos cursos e personalizar o seu perfil!</p>

<h3>Eu tenho acesso a promoções e descontos especiais?</h3>
<p>Sim! Se você se registrar em nosso site, você terá acesso a cupons, descontos e conteúdo exclusivos para cadastrados.</p>

<h3>Quanto tempo leva para começar as aulas?</h3>
<p>Registre-se hoje e analisaremos sua candidatura em 3 a 4 dias úteis.</p>

<h3>Ainda tem dúvidas?</h3>
<p>Envie um e-mail para nós em <span class="ml">duvidas@asriel.com</span></p>
	</div>
	<div class="promova">
	    <h1>Promova o Asriel sempre que possível, desejando às pessoas para que nunca saia de moda: com os milhares de cursos online em centenas de categorias!</h1>
	</div>
<?php 
include('footer.php');
?>
<style type="text/css">
@import "https://use.fontawesome.com/releases/v5.5.0/css/all.css";

div.promova{
	position:relative;
	color:#f1f1f1;
	width:50%;
	text-align:center;
	margin-top:-23%;
	margin-left:25%;
	text-indent:5%;
}

span.ml{
	color:#3498DB;
}

section.ajuda{
	width:100%;
	background-image: radial-gradient(circle, #052336, #002f58, #003a7b, #00439e, #1347be);
}

div.desc{
	position:relative;
	width:100%;
	margin-top:-72.5%;
	background-color:#f1f1f1;
	border-bottom:20px solid #f1f1f1;
}

.desc h1{
	text-align:center;
	padding-bottom:5%;
}

.desc h3, p{
	padding-left:10%;
}

.login-caixa{
	width:40%;
	position:relative;
	padding-top:80%;
	left:50%;
	transform:translate(-50%,-50%);
	color:#f1f1f1;
}

.login-caixa h1{
	float:left;
	font-size:40px;
	border-bottom:6px solid #3090C7;
	margin-bottom:50px;
	padding:13px 0;
}
	
.textocaixa{
	width:100%;
	overflow:hidden;
	font-size:20px;
	padding:10px 0;
	margin:8px;
	border-bottom:1px solid #3090C7;
}

input::placeholder {
  color: #f1f1f1;
}

.textocaixa i{
	width:26px;
	float:left;
	text-align:center;
}

.textocaixa input{
	border:none;
	outline:none;
	background:none;
	color:#f1f1f1;
	font-size:18px;
	width:80%;
	float:left;
	margin: 0 10px;
}

.btnw{
	width:100%;
	background:none;
	border:2px solid #3090C7;
	color:#3090C7;
	padding:5px;
	font-size:18px;
	cursor:poniter;
	margin:12px 0;
	transition:0.8s;
	margin-top:15%;
	border-radius:2px;
	margin-bottom:25%;
}

.btnw:hover{
	background-color:#46C7C7;
	border:2px solid #46C7C7;	
	color:#fff;
}

input.cado{ 
	margin-top:5%;
	}

.desce{
	margin-top:5%;
	}
	
textarea{
	font-size:18px;
	padding-left:3%;
	text-indent:2%;
    background:none;
	border:none;
}	

textarea::placeholder {
    color: #f1f1f1;
	font-family: 'Source Sans Pro', sans-serif;	
}
</style>