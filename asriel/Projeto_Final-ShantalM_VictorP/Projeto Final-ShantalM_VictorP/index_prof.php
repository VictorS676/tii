<?php
	include_once("crud/conexao.php");
	$result_cursos = "SELECT * FROM cursos";
	$resultado_cursos = mysqli_query($con, $result_cursos);
?>
<!-- <!DOCTYPE html> -->
<html lang="pt-br">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Asriel: Protótipo de plataforma para cursos online, Versão: Alpha 1.0" />
		<meta name="keywords" content="Asriel, desenvolvimento de jogos, cursos, alunos, instrutores, tempo, aulas, online" />
		<meta name="author" content="Shantal de Morais Mantovani & Victor Pedro de Sousa" />
		<meta name="generator" content="Notepad++"/>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"/>
			<link rel="shorcut icon" href="imagens/a.png" />	
			<link rel="stylesheet" type="text/css" href="css/mn/estilizando.css"/>
				<title>Asriel - Painel de Professor</title>
	</head>		
<body>
<?php
session_start();
ob_start();
if(isset($_SESSION['nome_inst_sessao'])){
?>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Asriel: Protótipo de plataforma para cursos online, Versão: Alpha 1.0" />
		<meta name="keywords" content="Asriel, desenvolvimento de jogos, cursos, alunos, instrutores, tempo, aulas, online" />
		<meta name="author" content="Shantal de Morais Mantovani & Victor Pedro de Sousa" />
		<meta name="generator" content="Notepad++"/>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"/>
			<link rel="shorcut icon" href="imagens/a.png" />	
	</head>
<header>
		<nav>
			<div class="menu">
				<div class="logo">
					<a href="index_prof.php">
						<img src="imagens/fc.png" height="50px">
						<p class="text2">Asriel</p>
					</a>
				</div>
				<div class="cat_drop">
					<div class="dropdown">
						<button class="dropbtn"><i class="fas fa-th"></i> Categorias</button>		
						<div class="dropdown-conteudo">
							<a href="desengames.php">Desenvolvimento de Games</a>
							<a href="webdesign.php">Web Design</a>
							<a href="ingles.php">Inglês</a>
							<a href="html.php">HTML</a>
							<a href="javascript.php">Javascript</a>
						</div> 
					</div>	
				</div>
				<div class="busca">
					<form class="buscando" name="busque" action="crud/select.php" method="post">
						<input type="text" name="buscar" id="buscar" class="buscar" placeholder="Buscar cursos"></input>
							<button class="busca" type="submit"><i class="fas fa-search"></i></button>
					</form>
				</div>
				<div class="logout" id="y">
					<a href="logout.php" class="logout" id="y">Sair</a>
				</div>
			</div>
		</nav>
	</header>
	<?php
				    include('prof_user.php');
	?>	
	                        <!-- ============= JANELA MODAL - Utilizamos muito o auxilio do Canal do Youtube Celke =================== -->
<link href="css/bootstrap.min.css" rel="stylesheet">	
<div class="container theme-showcase" role="main">
			<div class="page-header">
				<h1>Painel de Controle de Professor</h1>
			</div><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
			<div class="pull-right">
				<button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#myModalcad">Cadastrar</button>
			</div>
			<!-- Inicio Modal -->
			<div class="modal fade" id="myModalcad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title text-center" id="myModalLabel">Cadastrar Curso</h4>
						</div>
						<div class="modal-body">
							<form method="POST" action="http://localhost/Projeto%20Final-ShantalM_VictorP/processa_cad.php" enctype="multipart/form-data"><!-- ÂNCORA -->
								<div class="form-group">
									<label for="recipient-name" class="control-label">Nome:</label>
									<input name="nome" type="text" class="form-control">
								</div>
								<div class="form-group">
									<label for="message-text" class="control-label">Detalhes:</label>
									<textarea name="detalhes" class="form-control"></textarea>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-success">Cadastrar</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- Fim Modal -->			
			<div class="row">
				<div class="col-md-12">
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nome do Curso</th>
								<th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<?php while($rows_cursos = mysqli_fetch_assoc($resultado_cursos)){ ?>
								<tr>
									<td><?php echo $rows_cursos['id']; ?></td>
									<td><?php echo $rows_cursos['nome']; ?></td>
									<td>
										<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal<?php echo $rows_cursos['id']; ?>">Visualizar</button>
										
										<button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#exampleModal" data-whatever="<?php echo $rows_cursos['id']; ?>" data-whatevernome="<?php echo $rows_cursos['nome']; ?>"  data-whateverdetalhes="<?php echo $rows_cursos['detalhes']; ?>">Editar</button>
										
										<a href="processa_apagar.php?id=<?php echo $rows_cursos['id']; ?>"><button type="button" class="btn btn-xs btn-danger">Apagar</button></a>
									</td>
								</tr>
								<!-- Inicio Modal -->
								<div class="modal fade" id="myModal<?php echo $rows_cursos['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title text-center" id="myModalLabel"><?php echo $rows_cursos['nome']; ?></h4>
											</div>
											<div class="modal-body">
												<p><?php echo $rows_cursos['id']; ?></p>
												<p><?php echo $rows_cursos['nome']; ?></p>
												<p><?php echo $rows_cursos['detalhes']; ?></p>
											</div>
										</div>
									</div>
								</div>
								<!-- Fim Modal -->
							<?php } ?>
						</tbody>
					 </table>
				</div>
			</div>		
		</div>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel">Cursos</h4>
					</div>
					<div class="modal-body">
						<form method="POST" action="http://localhost/Projeto%20Final-ShantalM_VictorP/processa.php" enctype="multipart/form-data"><!-- ÂNCORA -->
							<div class="form-group">
								<label for="recipient-name" class="control-label">Nome:</label>
								<input name="nome" type="text" class="form-control" id="recipient-name">
							</div>
							<div class="form-group">
								<label for="message-text" class="control-label">Detalhes:</label>
								<textarea name="detalhes" class="form-control" id="detalhes-text"></textarea>
							</div>
							<input name="id" type="hidden" id="id_curso">
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-danger">Alterar</button>
							</div>
						</form>
					</div>			  
				</div>
			</div>
		</div>
		<!-- jQuery (necessário para Bootstrap JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Inclui todos os plugins compilados, ou inclui arquivos individuais files necessários -->
		<script src="js/bootstrap.min.js"></script>
		<script type="text/javascript">
			$('#exampleModal').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) // Button que ativa o modal
				var recipient = button.data('whatever') // Extrai as informações dos atributos
				var recipientnome = button.data('whatevernome')
				var recipientdetalhes = button.data('whateverdetalhes')
				// Atualiza o conteúdo da modal. Usamos o JQuery aqui, mas posso usar outras livrarias
				var modal = $(this)
				modal.find('.modal-title').text('ID do Curso: ' + recipient)
				modal.find('#id_curso').val(recipient)
				modal.find('#recipient-name').val(recipientnome)
				modal.find('#detalhes-text').val(recipientdetalhes)
			})
		</script>
	<style type="text/css">
.page-header{
	margin-left:40%;
	width:45%;
	margin-right:-50%;
}

div.modal{
	margin-left:35%;
}

div.row{
	margin-left:30%;
	width:70%;
	padding-bottom:5%;
}

.btn{
	margin-left:5%;
}
    </style>	
				<?php 
				    include('footer_prof.php');					
				} else{ ?>
	<div class="sai">
	    <h2>Acesso Negado</h2>
		<img src="imagens/a.gif" width="100%"/>
	</div>
	
<style type="text/css">

@import url('https://fonts.googleapis.com/css?family=Press+Start+2P');

h2{
	font-size:40px;
	color:#fff;
	font-family: 'Press Start 2P', cursive;	
	text-align:center;
}

div.sai{
	width:100%;
	height:100%;
	background-color:#000;
}

</style>
<?php } ?>