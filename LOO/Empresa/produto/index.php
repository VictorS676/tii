<?php
// A inclusão do arquivo desse arquivo TEM que usar os pontos de nível de diretório.
include '../config/constants.php';

include APPPATH.'views/header.php';
include APPPATH.'views/navbar.php';

include 'views/exec1.php';
include 'views/exec2.php';
include 'views/exec3.php';

include APPPATH.'helpers/date_helper.php';
include APPPATH.'models/date_card_model.php';
include APPPATH.'views/date_card.php';

include APPPATH.'views/rodape.php';
include APPPATH.'views/footer.php';

?>