<?php

$id = $_GET['id'];

$idol['perfil'] = 'assets\img\go.jpg';
$idol['nome'] = 'George Orwell';
$idol['bio'] = 'Eric Arthur Blair, mais conhecido pelo pseudónimo George Orwell, foi um escritor, jornalista e ensaísta político inglês, nascido na Índia Britânica.';
$idol['link'] = 'https://www.ebiografia.com/george_orwell/';

$idol1['perfil'] = 'assets\img\eh.jpg';
$idol1['nome'] = 'Ernest Hemingway';
$idol1['bio'] = 'Ernest Miller Hemingway foi um escritor norte-americano. Trabalhou como correspondente de guerra em Madrid durante a Guerra Civil Espanhola.';
$idol1['link'] = 'https://www.portaldaliteratura.com/autores.php?autor=1901';

$idol2['perfil'] = 'assets\img\mt.jpg';
$idol2['nome'] = 'Mark Twain';
$idol2['bio'] = 'Samuel Langhorne Clemens, mais conhecido pelo pseudônimo Mark Twain, foi um escritor e humorista norte-americano.';
$idol2['link'] = 'https://www.infoescola.com/biografias/mark-twain/';

$idol3['perfil'] = 'assets\img\mc.png';
$idol3['nome'] = 'Marie Curie';
$idol3['bio'] = 'Foi a primeira mulher a ser laureada com um Prêmio Nobel e a primeira pessoa e única mulher a ganhar o prêmio duas vezes. A família Curie ganhou um total de cinco prêmios Nobel.';
$idol3['link'] = 'https://pt.wikipedia.org/wiki/Marie_Curie';

$idol4['perfil'] = 'assets\img\my.jpg';
$idol4['nome'] = 'Malala Yousafzai';
$idol4['bio'] = 'Malala Yousafzai é uma ativista paquistanesa. Foi a pessoa mais nova a ser laureada com um prémio Nobel.';
$idol4['link'] = 'https://guiadoestudante.abril.com.br/blog/atualidades-vestibular/conheca-a-historia-da-ativista-malala-yousafzai/';

$idol5['perfil'] = 'assets\img\mtc.jpg';
$idol5['nome'] = 'Madre Teresa';
$idol5['bio'] = 'Madre Teresa de Calcutá ou Santa Teresa de Calcutá, foi uma religiosa católica de etnia albanesa naturalizada indiana, fundadora da congregação das Missionárias da Caridade.';
$idol5['link'] = 'https://www.todamateria.com.br/madre-teresa-de-calcuta/';

$j = array($idol, $idol1, $idol2, $idol3, $idol4, $idol5);

?>