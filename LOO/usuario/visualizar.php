<?php

include '../config/constants.php';
include APPPATH.'views/header.php';
include APPPATH.'views/navbar.php';

include 'models/usuario_model.php';

$id = isset($_GET['id']) ? $_GET['id'] : 1; // Verifica a existência do ID
$usuario = dados_usuario($id);
include 'views/detalhe.php';

// include APPPATH.'views/rodape.php';
include APPPATH.'views/footer.php';

?>