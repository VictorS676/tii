<?php 

function lista_usuarios(){
	$link = new mysqli('localhost','root','','loo');
	$sql = "SELECT * FROM usuario";
	$result = $link->query($sql);

	$html = '<table class="table">
			  <thead>
				<tr>
				  <th scope="col">Nome</th>
				  <th scope="col">Sobrenome</th>
				  <th scope="col">E-Mail</th>
				  <th scope="col">Senha</th>
				  <th scope="col">Telefone</th>
				  <th scope="col"></th>
				</tr>
			  </thead>
			  <tbody>';

	while ($usuario = $result->fetch_assoc()){
		$html .= "<tr>";
		$html .= "<td>".$usuario['nome']."</td>";
		$html .= "<td>".$usuario['sobrenome']."</td>";
		$html .= "<td>".$usuario['email']."</td>";
		$html .= "<td>".$usuario['senha']."</td>";
		$html .= "<td>".$usuario['telefone']."</td>";
		$html .= '<td>'.get_action_buttons($usuario['id']).'</td>';
		$html .= "</tr>";
	}
	$html .= "</tbody></table>";
	echo $html;
}

function get_action_buttons($id){
	$html = '<a href="'.BASEURL.'usuario/editar.php?id='.$id.'"><i class="far fa-edit mr-3 text-success"></i></a>';
	$html .= '<a href="'.BASEURL.'usuario/deletar.php?id='.$id.'"><i class="fas fa-trash-alt mr-3 text-danger"></i></a>';
	$html .= '<a href="'.BASEURL.'usuario/visualizar.php?id='.$id.'"><i class="fas fa-eye text-muted"></i></a>';
	return $html;
}

function salva_usuario(){
	if(sizeof($_POST) > 0){
		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$email = $_POST['email'];
		$senha = $_POST['senha'];
		$telefone = $_POST['telefone'];

		// criar conexão com o banco de dados
		$link = new mysqli('localhost', 'root', '', 'loo');
		
		// Define a ação sobre o banco de dados
		$sql = "INSERT INTO usuario(nome, sobrenome, email, senha, telefone) VALUES ('$nome','$sobrenome','$email','$senha','$telefone')";
		
		// executa o código sql
		$link->query($sql);
	}
}

function dados_usuario($id){
	// TRABALHO_PROVA VETOR ASSOCIATIVO
	$sql = "SELECT * FROM usuario WHERE id = $id";
	$link = new mysqli('localhost', 'root', '', 'loo');
	$rs = $link->query($sql);
	return $rs->fetch_assoc();

	// $usuario['nome'] = 'Francisco';
	// $usuario['sobrenome'] = 'Filho';
	// $usuario['email'] = 'ff@gmail.com';
	// $usuario['senha'] = 'ivfmdeuip';
	// $usuario['telefone'] = '(15) 2348-8912';
}

function edita_usuario($id){
	if(sizeof($_POST) > 0){
		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$email = $_POST['email'];
		$senha = $_POST['senha'];
		$telefone = $_POST['telefone'];

		$link = new mysqli('localhost', 'root', '', 'loo');
		$sql = "UPDATE usuario SET nome='$nome', sobrenome='$sobrenome', email='$email', senha='$senha', telefone='$telefone' WHERE id = $id";
		$link->query($sql);

		// REDIRECIONA PARA A LISTA DE USUÁRIOS
		header('Location: '.BASEURL.'usuario/lista.php');

	}
}

function deleta_usuario($id){
	$sql = "DELETE FROM usuario WHERE id = $id";
	$link = new mysqli('localhost', 'root', '', 'loo');
	$link->query($sql);
	header('Location: '.BASEURL.'usuario/lista.php');
}

?>