	<div class="container mt-2 mb-1">	
		<section class="mt-4 mb-2">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<div class="list-group-flush">
						<div class="list-group-item">
							<p class="mb-0"> <i class="fab fa-slack-hash fa-2x mr-4 mr-4 red p-3 white-text rounded" aria-hidden="true"></i>Listgroup</p>
						</div>
						<div class="list-group-item">
							<p class="mb-0"><i class="fab fa-instagram fa-2x mr-4 blue p-3 white-text rounded " aria-hidden="true"></i>Fotos</p>
						</div>
						<div class="list-group-item">
							<p class="mb-0"><i class="fab fa-snapchat fa-2x mr-4 mr-4 purple p-3 white-text rounded" aria-hidden="true"></i>Férias</p>
						</div>
						<div class="list-group-item">
							<p class="mb-0"><i class="fab fa-facebook fa-2x mr-4 mr-4  blue darken-3 p-3 white-text rounded" aria-hidden="true"></i>Social</p>
						</div>
						<div class="list-group-item">
							<p class="mb-0"><i class="fab fa-google fa-2x mr-4 mr-4  lime lighten-1 p-3 white-text rounded" aria-hidden="true"></i>Ajuda</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-4">			
					<div class="card">
						<div class="view overlay">
							<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2821%29.jpg" alt="Card image cap">
								<a>
									<div class="mask rgba-white-slight"></div>
								</a>
						</div>
						<div class="card-body elegant-color white-text rounded-bottom">
							<a class="activator waves-effect mr-4"><i class="fas fa-share-alt white-text"></i></a>
							<h4 class="card-title">Card de 1/3</h4>
							<hr class="hr-light">
							<p class="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
							<a href="#!" class="white-text d-flex justify-content-end"><h5>Read more <i class="fas fa-angle-double-right"></i></h5></a>
						</div>
				</div>			
				</div>
				<div class="col-xs-12 col-md-2">
					<div class="card bg-light mb-3">
						<div class="card-header">Panel</div>
							<div class="card-body">
								<h5 class="card-title">Light Panel</h5>
								<p class="card-text">Crie uma seção da página que contenha um card, um listgroup e um panel. O card deve ocupar um terço da página, o listagroup deve ocupar a metade</p>
							</div>
						</div>
				</div>
			</div>
		</section>
	</div>