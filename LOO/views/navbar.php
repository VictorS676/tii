	<nav class="navbar navbar-expand-lg navbar-dark unique-color-dark">
	  <a class="navbar-brand" href="<?= BASEURL ?>index.php">LOO 2019</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
		aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="basicExampleNav">
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item active">
			<a class="nav-link" href="<?= BASEURL ?>index.php">Home
			  <span class="sr-only">(current)</span>
			</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="<?= BASEURL ?>produto/index.php?card=0">Produto</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="#">Contato</a>
		  </li>
		  <li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
			  aria-expanded="false">Empresa</a>
			<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
			  <a class="dropdown-item" href="#">A equipe</a>
			  <a class="dropdown-item" href="#">História</a>
			  <a class="dropdown-item" href="#">Objetivos</a>
			</div>
		  </li>

		  <li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
			  aria-expanded="false">Ídolos</a>
			<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
			  <a class="dropdown-item" href="<?= BASEURL ?>idolo.php?id=0">George Orwell</a>
			  <a class="dropdown-item" href="<?= BASEURL ?>idolo.php?id=1">Ernest Hemingway</a>
			  <a class="dropdown-item" href="<?= BASEURL ?>idolo.php?id=2">Mark Twain</a>
			  <a class="dropdown-item" href="<?= BASEURL ?>idolo.php?id=3">Marie Curie</a>
			  <a class="dropdown-item" href="<?= BASEURL ?>idolo.php?id=4">Malala Yousafzai</a>
			  <a class="dropdown-item" href="<?= BASEURL ?>idolo.php?id=5">Madre Teresa</a>
			</div>
		  </li>

		</ul>
		<form class="form-inline">
		  <div class="md-form my-0">
			<input class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Search">
		  </div>
		</form>
	  </div>
	</nav>	