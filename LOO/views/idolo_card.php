<div class="container">
	<section>
		<div class="row mt-5 mb-5">
			<div class="col col-md-6 mt2 pb-2 mx-auto">
				<div class="card">
				  <div class="view overlay">
					  <div class="view overlay zoom">
						<img class="card-img-top mx-auto" src="<?= $j[$id]['perfil'] ?>" alt="Card image cap">
							<a>
							  <div class="mask rgba-white-slight"></div>
							</a>
						</div>
				  </div>
				  <div class="card-body elegant-color white-text rounded-bottom">
					<a class="activator waves-effect mr-4"><i class="fas fa-share-alt white-text"></i></a>
					<h4 class="card-title"><?= $j[$id]['nome'] ?></h4>
					<hr class="hr-light">
					<p class="card-text white-text mb-4"><?= $j[$id]['bio'] ?></p>
					<a href="<?= $j[$id]['link'] ?>" class="white-text d-flex justify-content-end"><h5>Biografia <i class="fas fa-angle-double-right"></i></h5></a>
				  </div>
				</div>
			</div>
		</div>
	</section>
</div>